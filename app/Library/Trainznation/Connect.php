<?php


namespace App\Library\Trainznation;


class Connect
{
    protected $endpoint;

    public function __construct()
    {
        if(env('APP_ENV') == 'local'){
            $this->endpoint = 'https://trainznation.io/api/';
        }else{
            $this->endpoint = 'https://trainznation.eu/api/';
        }
    }

    private function call($method, array $params)
    {
        $curl = curl_init();
        if($method == 'POST')
        {
            curl_setopt_array($curl, [
                CURLOPT_URL => $this->endpoint.$params['uri'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => $params['postfield'],
            ]);
        }
        if($method == 'GET')
        {
            curl_setopt_array($curl, [
                CURLOPT_URL => $this->endpoint.$params['uri'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
            ]);
        }

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if($err)
        {
            return $err;
        }else{
            return $response;
        }
    }

    public function get($path)
    {
        $params = [
            "uri"   => $path
        ];

        return $this->call('GET', $params);
    }

    public function post($path, array $post)
    {
        $params = [
            "uri"   => $path,
            "postfield" => $post
        ];

        return $this->call("POST", $params);
    }
}