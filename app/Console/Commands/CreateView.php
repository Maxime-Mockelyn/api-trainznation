<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:view {type} {dossier} {view}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Création d'une vue";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $view = $this->argument('view');
        $dossierOutput = base_path().'/resources/views/'.$this->argument('dossier');
        $type = $this->argument('type');

        if($type == 'back'){
            ob_start();
            ?>
@extends("Layout.app")

@section("bread")
<div class="row page-titles">
    <div class="col-md-5 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">{{ env("APP_NAME") }}</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">{{ env("APP_NAME") }}</a></li>
            <li class="breadcrumb-item active">Tableau de Bord</li>
        </ol>
    </div>
</div>
@endsection

@section("content")
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                This is some text within a card block.
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")

@endsection
            <?php
            $content = ob_get_clean();
        }elseif($type == 'front'){
            ob_start();
            ?>
@extends('Layout.site')
@section("title")
BLOG
@stop
@section("content")
<section id="page-title">

    <div class="container clearfix">
        <h1>@yield("title")</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">{{ env('APP_NAME') }}</a></li>
            <li class="active">@yield("title")</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

        </div>

    </div>

</section><!-- #content end -->
@stop
            <?php
            $content = ob_get_clean();
        }else{
            $this->error('Le type de vue est inconnue !');
        }

        $this->isDir($dossierOutput);
        $rec = file_put_contents($dossierOutput."/".$view.".blade.php", $content);

        if($rec == false){
            $this->error('Impossible de créer le fichier de vue');
        } else {
            $this->info('Le fichier'.$view.' à été créée');
        }
    }

    private function isDir($dossier){
        if(!is_dir($dossier)){
            mkdir($dossier);
        }
    }
}
