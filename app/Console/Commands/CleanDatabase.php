<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CleanDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'srice:clean-database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nettoie la base de donnée des données inutile';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Nettoyage des informations inutile by SRICE');
    }
}
