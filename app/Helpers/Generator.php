<?php
if(!function_exists('createPassword')){
    function createPassword(){
        return str_random(8);
    }
}

if(!function_exists('createCodeCE')){
    function createCodeCE($nameComite){
        $comite = str_limit($nameComite, 3);
        return $comite.str_random(6);
    }
}

if(!function_exists('generateLorem')){
    function generateLorem(){
        return "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget aliquam elit. Etiam tincidunt accumsan nulla, non condimentum est scelerisque id. Suspendisse faucibus porta consequat. Phasellus in neque quis sem facilisis consequat. Proin vel nisl ut dui maximus laoreet vel a diam. Nulla facilisi. Donec mollis dui quis urna iaculis, ac interdum massa rutrum. Nunc eu tellus sit amet quam pellentesque rutrum.";
    }
}

if(!function_exists('generateNum')){
    function generateNum(int $lenght){
        return str_random($lenght);
    }
}

if(!function_exists('generateHeader')){
    function generateHeader(string $category){
        switch ($category)
        {
            case $category: return sourceImage('download/category/'.$category.'.jpg');
            default: return null;
        }
    }
}



