<?php
if(!function_exists('arrayPluck')){
    function arrayPluck(array $array, $value, $key){
        return array_pluck($array, $value, $key);
    }
}