<?php
if(!function_exists('notificationIcon')){
    function notificationIcon(string $identifier){
        switch ($identifier)
        {
            case 'AuthLogin': return 'fa-sign-in-alt';
            case 'AuthRegistered': return 'fa-user-plus';
            case 'badgeUnlock': return 'fa-certificate';
            case 'VideoTimer': return 'fa-play';
            case 'VideoPublish': return 'fa-check-circle';
            case 'AssetPublish': return 'fa-download';
            case 'BlogPublish': return 'fa-twitter-square';
            default: return 'fa-times-circle';
        }
    }
}

if(!function_exists('randColorWikiCategory')){
    function randColorWikiCategory(){
        $rand = rand(0, 8);

        switch ($rand)
        {
            case 0: return "";
            case 1: return "kt-svg-icon--brand";
            case 2: return "kt-svg-icon--light";
            case 3: return "kt-svg-icon--dark";
            case 4: return "kt-svg-icon--primary";
            case 5: return "kt-svg-icon--success";
            case 6: return "kt-svg-icon--info";
            case 7: return "kt-svg-icon--warning";
            case 8: return "kt-svg-icon--danger";
            default: return "";
        }
    }
}
