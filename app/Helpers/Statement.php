<?php
if(!function_exists('stateTutoriel')){
    function stateTutoriel($state){
        if($state == 0) {
            return 'border-danger';
        }elseif($state == 1){
            return 'border-success';
        }else{
            return 'border-warning';
        }

    }
}

if(!function_exists('stateAnomalie')){
    function stateAnomalie($state){
        if($state == 0) {
            return 'bg--error';
        }elseif($state == 1){
            return 'bg--warning';
        }else{
            return 'bg--success';
        }

    }
}

if(!function_exists('stateAnomalieText')){
    function stateAnomalieText($state){
        if($state == 0) {
            return 'color--error';
        }elseif($state == 1){
            return 'color--warning';
        }else{
            return 'color--success';
        }

    }
}

if(!function_exists('stateAnomalieBack')){
    function stateAnomalieBack($state){
        if($state == 0) {
            return 'kt-bg-danger';
        }elseif($state == 1){
            return 'kt-bg-warning';
        }else{
            return 'kt-bg-success';
        }

    }
}

if(!function_exists('stateAnomalieRadial')){
    function stateAnomalieRadial($percent){
        if($percent == 0 & $percent <= 33.33) {return "#D30101";}
        elseif($percent >= 33.34 && $percent <= 66.68){return "#D36A01";}
        else{return "#67D301";}

    }
}

if(!function_exists('stateAnomalieProgress')){
    function stateAnomalieProgress($percent){
        if($percent == 0 & $percent <= 33.33) {return "kt-bg-danger";}
        elseif($percent >= 33.34 && $percent <= 66.68){return "kt-bg-warning";}
        else{return "kt-bg-success";}

    }
}

if(!function_exists('stateCompatibilityBadge')){
    function stateCompatibilityBadge($state){
        switch ($state)
        {
            case 0:
                return 'badge-danger';
            case 1:
                return 'badge-warning';
            case 2:
                return 'badge-success';
            case 3:
                return 'badge-default';
            default:
                return "badge-default";
        }

    }
}

if(!function_exists('stateCompatibilityBadgeEdit')){
    function stateCompatibilityBadgeEdit($state){
        switch ($state)
        {
            case 0:
                return "<span class='kt-badge kt-badge--danger kt-badge--inline kt-badge--rounded'>Non Compatible</span>";
            case 1:
                return "<span class='kt-badge kt-badge--warning kt-badge--inline kt-badge--rounded'>Problème de compatibilité</span>";
            case 2:
                return "<span class='kt-badge kt-badge--success kt-badge--inline kt-badge--rounded'>Compatible</span>";
            case 3:
                return "<span class='kt-badge kt-badge--info kt-badge--inline kt-badge--rounded'>Test à effectuer</span>";
            default:
                return "<span class='kt-badge kt-badge--dark kt-badge--inline kt-badge--rounded'>Inconnue</span>";
        }

    }
}

if(!function_exists('stateCompatibilityTooltip')){
    function stateCompatibilityTooltip($state){
        switch ($state)
        {
            case 0:
                return 'Non Compatible';
            case 1:
                return 'Problème de compatibilité';
            case 2:
                return "Compatible";
            case 3:
                return "Test à effectuer";

            default:
                return "Inconnue";
        }

    }
}

if(!function_exists('stateComment')){
    function stateComment($state){
        if($state == 0) {
            return '<span class="kt-badge kt-badge--danger kt-badge--inline"><i class="la la-times-circle"></i> Non Approuvée</span>';
        }else{
            return '<span class="kt-badge kt-badge--success kt-badge--inline"><i class="la la-check-circle"></i>Approuvée</span>';
        }

    }
}

if(!function_exists('stateDownload')){
    function stateDownload($state, $date = null){
        if($state == 0) {
            return '<i class="flaticon2-close-cross kt-font-danger" data-toggle="kt-tooltip" title="Non Publier"></i>';
        }elseif($state == 1){
            return '<i class="flaticon2-correct kt-font-success" data-toggle="kt-tooltip" title="Publier"></i>';
        }else{
            if($date != null)
            {
                return '<i class="la la-clock-o kt-font-warning" data-toggle="kt-tooltip" title="Prévue '.formatDateHuman($date).'"></i>';
            }else{
                return '<i class="la la-clock-o kt-font-warning" data-toggle="kt-tooltip" title="Prévue"></i>';
            }
        }

    }
}

if(!function_exists('stateAssetPercent')){
    function stateAssetPercent($state){
        if($state <= 33.5) {
            return 'kt-bg-danger';
        }elseif($state > 33.6 && $state <= 66.5){
            return 'kt-bg-warning';
        }else{
            return 'kt-bg-success';
        }

    }
}

if(!function_exists('stateBackTutoriel')){
    function stateBackTutoriel($state, $date = null){
        if($state == 0) {
            return '<i class="flaticon2-close-cross kt-font-danger" data-toggle="kt-tooltip" title="Non Publier"></i>';
        }elseif($state == 1){
            return '<i class="flaticon2-correct kt-font-success" data-toggle="kt-tooltip" title="Publier"></i>';
        }else{
            if($date != null)
            {
                return '<i class="la la-clock-o kt-font-warning" data-toggle="kt-tooltip" title="Prévue '.formatDateHuman($date).'"></i>';
            }else{
                return '<i class="la la-clock-o kt-font-warning" data-toggle="kt-tooltip" title="Prévue"></i>';
            }
        }

    }
}

if(!function_exists('stateUserActivity')){
    function stateUserActivity($state){
        switch ($state)
        {
            case 0:
                return 'kt-font-danger';
            case 1:
                return 'kt-font-warning';
            case 2:
                return "kt-font-success";

            default:
                return "Inconnue";
        }

    }
}
