<?php
if(!function_exists('formatCurrency')){
    function formatCurrency($value){
        return number_format($value, 2, ',', ' ')." €";
    }
}

if(!function_exists('formatDate')){
    function formatDate($date, $separated = '/'){
        if($separated == '/'){
            return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('d/m/Y');
        }else{
            return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('d-m-Y');
        }
    }
}

if(!function_exists('formatDateTime')){
    function formatDateTime($date){
        return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('d/m/Y à H:i');
    }
}

if(!function_exists('formatTimestamp')){
    function formatTimestamp($date){
        return strtotime($date);
    }
}

if(!function_exists('formatDelai')){
    function formatDelai($heure_init, $diff){
        $difference = \Carbon\Carbon::createFromTimestamp(strtotime($heure_init))->subSeconds($diff);
        return $difference->format('H:i');
    }
}

if(!function_exists('formatTime')){
    function formatTime($date){
        return \Carbon\Carbon::createFromTimestamp(strtotime($date))->format('H:i');
    }
}

if(!function_exists('formatDateHuman')){
    function formatDateHuman($date){
        \Carbon\Carbon::setLocale('fr');
        return \Carbon\Carbon::createFromTimestamp(strtotime($date))->diffForHumans();
    }
}

if(!function_exists('formatJSON')){
    function formatJSON(array $value){
        return response()->json($value);
    }
}

if(!function_exists('formatNumCb')){
    function formatNumCb($numcb){
        return preg_replace("/[0-9]/", "X", $numcb, 12);
    }
}

if(!function_exists('formatPlural')){
    function formatPlural(string $string, int $count){
        if($count > 1){
            return $string.'s';
        }else{
            return $string;
        }
    }
}

if(!function_exists('pregReplaceKuid')){
    function pregReplaceKuid($kuid){
        $first = str_replace('<', '', $kuid);
        $last = str_replace('>', '', $first);
        $point = str_replace(':', '_', $last);
        return $point;
    }
}

if(!function_exists('firstString')){
    function firstString(string $string){
        return str_limit($string, 1);
    }
}
