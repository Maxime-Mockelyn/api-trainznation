<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function subcategories()
    {
        return $this->hasMany(Subcategory::class);
    }
}
