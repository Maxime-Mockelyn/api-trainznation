<?php

namespace App\Http\Controllers\Referenciel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReferencielController extends Controller
{
    public function message()
    {
        return view("Referenciel.message");
    }

    public function miniBrowser()
    {
        return view("Referenciel.miniBrowser");
    }
}
