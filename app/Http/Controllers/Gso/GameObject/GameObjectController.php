<?php

namespace App\Http\Controllers\Gso\GameObject;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GameObjectController extends Controller
{
    public function index()
    {
        return view("Gso.Gameobject.index");
    }

    public function browser()
    {
        return view("Gso.Gameobject.browser");
    }

    public function constructor()
    {
        return view("Gso.Gameobject.constructor");
    }

    public function interface()
    {
        return view("Gso.Gameobject.interface");
    }

    public function interlocking()
    {
        return view("Gso.Gameobject.interlocking");
    }

    public function train()
    {
        return view("Gso.Gameobject.train");
    }
}
