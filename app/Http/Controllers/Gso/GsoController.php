<?php

namespace App\Http\Controllers\Gso;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GsoController extends Controller
{
    public function asset()
    {
        return view("Gso.asset");
    }

    public function gameobjectid()
    {
        return view("Gso.gameobjectid");
    }

}
