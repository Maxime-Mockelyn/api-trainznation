<?php

namespace App\Http\Controllers;

use App\Library\Trainznation\Connect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Validator;

class HomeController extends Controller
{
    public function index()
    {
        return view("home");
    }

    public function search(Request $request)
    {
        switch ($request->search){
            case 'commentaire': return redirect()->route('Element.comment');break;
            case 'delimiteur': return redirect()->route('Element.comment');break;
            case ';': return redirect()->route('Element.comment');break;
            case ':': return redirect()->route('Element.comment');break;
            case '//': return redirect()->route('Element.comment');break;
            case '{}': return redirect()->route('Element.comment');break;
            case '{};': return redirect()->route('Element.comment');break;
            case 'break': return redirect()->route('Element.keyword');break;
            case 'case': return redirect()->route('Element.keyword');break;
            case 'class': return redirect()->route('Element.keyword');break;
            case 'continue': return redirect()->route('Element.keyword');break;
            case 'default': return redirect()->route('Element.keyword');break;
            case 'define': return redirect()->route('Element.keyword');break;
            case 'final': return redirect()->route('Element.keyword');break;
            case 'for': return redirect()->route('Element.keyword');break;
            case 'goto': return redirect()->route('Element.keyword');break;
            case 'if': return redirect()->route('Element.keyword');break;
            case 'include': return redirect()->route('Element.keyword');break;
            case 'inherited': return redirect()->route('Element.keyword');break;
            case 'mandatory': return redirect()->route('Element.keyword');break;
            case 'me': return redirect()->route('Element.keyword');break;
            case 'native': return redirect()->route('Element.keyword');break;
            case 'null': return redirect()->route('Element.keyword');break;
            case 'on': return redirect()->route('Element.keyword');break;
            case 'public': return redirect()->route('Element.keyword');break;
            case 'return': return redirect()->route('Element.keyword');break;
            case 'static': return redirect()->route('Element.keyword');break;
            case 'switch': return redirect()->route('Element.keyword');break;
            case 'thread': return redirect()->route('Element.keyword');break;
            case 'void': return redirect()->route('Element.keyword');break;
            case 'wait': return redirect()->route('Element.keyword');break;
            case 'while': return redirect()->route('Element.keyword');break;
            case 'logique': return redirect()->route('Element.operator');break;
            case 'tableau': return redirect()->route('Element.operator');break;
            case 'cast': return redirect()->route('Element.operator');break;
            case 'addition': return redirect()->route('Element.operator');break;
            case 'soustraction': return redirect()->route('Element.operator');break;
            case 'multiplication': return redirect()->route('Element.operator');break;
            case 'division': return redirect()->route('Element.operator');break;
            case 'modulateur': return redirect()->route('Element.operator');break;
            case 'incremental': return redirect()->route('Element.operator');break;
            case 'bool': return redirect()->route('Element.type');break;
            case 'int': return redirect()->route('Element.type');break;
            case 'string': return redirect()->route('Element.type');break;
            case 'object': return redirect()->route('Element.type');break;
            case 'float': return redirect()->route('Element.type');break;
            case 'FindAsset': return redirect()->route('Gso.asset');break;
            case 'GetConfigSoup': return redirect()->route('Gso.asset');break;
            case 'GetKuid': return redirect()->route('Gso.asset');break;
            case 'GetLocalisedName': return redirect()->route('Gso.asset');break;
            case 'GetStringTable': return redirect()->route('Gso.asset');break;
            case 'LookupKUIDTable': return redirect()->route('Gso.asset');break;
            case 'GetDebugString': return redirect()->route('Gso.gameobjectid');break;
            case 'SerializeToString': return redirect()->route('Gso.gameobjectid');break;
            case 'AddHandler': return redirect()->route('Gso.gameobject');break;
            case 'ClearMessages': return redirect()->route('Gso.gameobject');break;
            case 'GetId': return redirect()->route('Gso.gameobject');break;
            case 'GetName': return redirect()->route('Gso.gameobject');break;
            case 'PostMessage': return redirect()->route('Gso.gameobject');break;
            case 'SendMessage': return redirect()->route('Gso.gameobject');break;
            case 'Sleep': return redirect()->route('Gso.gameobject');break;
            case 'Sniff': return redirect()->route('Gso.gameobject');break;
            case 'Browser': return redirect()->route('Gso.Gameobject.browser');break;
            case 'BringToFront': return redirect()->route('Gso.Gameobject.browser');break;
            case 'GetAsset': return redirect()->route('Gso.Gameobject.browser');break;
            case 'GetWindowVisible': return redirect()->route('Gso.Gameobject.browser');break;
            case 'LoadHTMLFile': return redirect()->route('Gso.Gameobject.browser');break;
            case 'LoadHTMLString': return redirect()->route('Gso.Gameobject.browser');break;
            case 'ResetScrollBar': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetCloseEnabled': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetParam': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetScrollEnabled': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetWindowStyle': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetWindowVisible': return redirect()->route('Gso.Gameobject.browser');break;
            case 'GetWindowWidth': return redirect()->route('Gso.Gameobject.browser');break;
            case 'GetWindowHeight': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetWindowRect': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetWindowSize': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetWindowPosition': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetWindowGrow': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetElementObjectProperty': return redirect()->route('Gso.Gameobject.browser');break;
            case 'SetTrainzText': return redirect()->route('Gso.Gameobject.browser');break;
            case 'GetTrainzStrings': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewBrowser': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewDriverCommand': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewDriverCommands': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewKUIDList': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewMenu': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewProductFilter': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'NewSoup': return redirect()->route('Gso.Gameobject.constructor');break;
            case 'RVBColor': return redirect()->route('Gso.Gameobject.interface');break;
            case 'AdjustScore': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetDecoupleMode': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetDisplayHeight': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetDisplayWidth': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetInterfaceVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetMapView': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetMessageWindowVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetMetricMode': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetObjectiveBarVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetScore': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetTimeStamp': return redirect()->route('Gso.Gameobject.interface');break;
            case 'GetWaybillWindowVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'HasUnseenMessages': return redirect()->route('Gso.Gameobject.interface');break;
            case 'HighlightButtonBarElement': return redirect()->route('Gso.Gameobject.interface');break;
            case 'Log': return redirect()->route('Gso.Gameobject.interface');break;
            case 'LogCallStack': return redirect()->route('Gso.Gameobject.interface');break;
            case 'LogResult': return redirect()->route('Gso.Gameobject.interface');break;
            case 'Print': return redirect()->route('Gso.Gameobject.interface');break;
            case 'ResetResults': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetAlert': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetDecoupleMode': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetElementProperty': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetHelperIconScale': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetMapView': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetMessageWindowVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetMetricMode': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetObjective': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetObjectiveBarVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetObjectiveIcon': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetResults': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetScore': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetTooltip': return redirect()->route('Gso.Gameobject.interface');break;
            case 'SetWaybillWindowVisible': return redirect()->route('Gso.Gameobject.interface');break;
            case 'ShowDriverButtonMenu': return redirect()->route('Gso.Gameobject.interface');break;
            case 'ShowObjective': return redirect()->route('Gso.Gameobject.interface');break;
            case 'ShowOnScreenHelp': return redirect()->route('Gso.Gameobject.interface');break;
            case 'WarnObsolete': return redirect()->route('Gso.Gameobject.interface');break;
            case 'Init': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'SetPathName': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'GetPathName': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'GetLocalisedPathName': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'IsObjectInPathDefinition': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'IsObjectOnPath': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'IsObjectOnOrBeyondPath': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'GetPathClearMethod': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'SetPathClearMethod': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'CanTransitionPathToState': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'ActivatePath': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'CancelPath': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'SetPanicState': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'AddTrain': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'RemoveTrain': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'IsOccupiedByTrain': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'EnablePathVisualisation': return redirect()->route('Gso.Gameobject.interlocking');break;
            case 'Exception':
                $results = [
                    0   => [
                        "title" => "[GSO/GameObject] - Classe <strong>GameObject</strong>",
                        "link"  => "/gso/game_object#Exception",
                        "text"  => "Permet de déclencher une exception sur le thread ou la fonction actuel"
                    ],
                    1   => [
                        "title" => "[GSO/GameObject/Interface] - Classe <strong>Interface</strong>",
                        "link"  => "/gso/game_object/interface#Exception",
                        "text"  => "Autorise explicitement le déclenchement d'une exception par un script en cas d'erreur majeure."
                    ]
                ];
                return $this->result(2, $request->search, $results);
            case 'DoesMatch':
                $results = [
                    0   => [
                        "title" => "[GameObjectId] - DoesMatch par GameObjectID",
                        "link"  => "/gso/game_object_id#doesMatch-gameobjectid",
                        "text"  => "Indique si ce GameObjectID correspond à l'ID transmis. Les GameObjectID DOIVENT être soumis à un test d’égalité à l’aide de cette fonction. Tester avec des opérateurs standard ('==' et '! =') Ne fonctionnera pas."
                    ],
                    1   => [
                        "title" => "[GameObjectId] - DoesMatch par GSObject",
                        "link"  => "/gso/game_object_id#doesMatch-gsobject",
                        "text"  => "Indique si ce GameObjectID correspond à l'objet transmis."
                    ]
                ];
                return $this->result(2, $request->search, $results);
            case 'array':
                $results = [
                    0   => [
                        "title" => "[Mot-clé] - Tableau",
                        "link"  => "/element/operator#1",
                        "text"  => "Indexation de tableaux et de chaînes."
                    ],
                    1   => [
                        "title" => "[Les Tableau] - Généralité",
                        "link"  => "/element/array",
                        "text"  => "Les tableaux doivent être déclarés et initialisés avant de pouvoir être accédés, et il sera généralement nécessaire de renseigner le tableau avec des données significatives."
                    ],
                    2   => [
                        "title" => "[Type] - Tableau",
                        "link"  => "/element/type#array",
                        "text"  => "Les tableaux sont des listes indexées de tout type de base ou de tout type de classe, y compris GameObjects."
                    ],
                ];

                return $this->result(3, $request->search, $results);

            case 'message':
                $results = [
                    0   => [
                        "title" => "[GSO/GameObject] - Classe <strong>GameObject</strong>",
                        "link"  => "/gso/game_object#exemple",
                        "text"  => "Exemple d'utilisation de PostMessage()"
                    ],
                    1   => [
                        "title" => "[Referenciel/Message] - Que sont les messages et comment peuvent-ils être utilisés ?",
                        "link"  => "/referenciel/message#what",
                        "text"  => "Les messages sont gérés par le routeur , une sorte de bureau de poste interne, et peuvent être échangés entre tous les objets issus de GameObject . Cela inclut les commandes de pilote, les règles et les bibliothèques, ainsi que presque tous les objets scriptables basés sur des maillages."
                    ],
                    2   => [
                        "title" => "[Referenciel/Message] - Comment écouter les messages ?",
                        "link"  => "/referenciel/message#how",
                        "text"  => ""
                    ],
                    3   => [
                        "title" => "[Referenciel/Message] - Quand et comment envoyer des messages ?",
                        "link"  => "/referenciel/message#when",
                        "text"  => "Vous pourriez envisager d'envoyer vos propres messages dans certains cas"
                    ],
                    4   => [
                        "title" => "[Referenciel/Message] - Exemple",
                        "link"  => "/referenciel/message#exemple",
                        "text"  => "Écoute de messages et utilisation de méthodes de gestionnaire de messages"
                    ],
                ];
                return $this->result(5, $request->search, $results);

            case 'GetElementProperty':
                $results = [
                    0   => [
                        "title" => "[GSO/GameObject/Browser] - Classe <strong>Browser</strong>",
                        "link"  => "/gso/game_object/browser#GetElementproperty",
                        "text"  => "Permet d'avoir des informations sur un élément HTML"
                    ],
                    1   => [
                        "title" => "[GSO/GameObjet/Interface] - Classe <strong>Interface</strong>",
                        "link"  => "/gso/game_object/interface#GetElementProperty",
                        "text"  => "Permet d'avoir des informations sur un élément d'interface"
                    ]
                ];
                return $this->result(2, $request->search, $results);
            default: return view('search', [
                "result"    => 0,
                "text"      => "Aucun Résultat pour cette recherche",
                "search"    => $request->search
            ]);
        }
    }

    public function result(int $result,string $search,array $results, string $text = null)
    {
        if($result == 0)
        {
            return view("search", [
                "result"    => 0,
                "text"      => $text,
                "search"    => $search
            ]);
        }else{
            return view("search", [
                "result"    => $result,
                "search"    => $search,
                "results"   => $results
            ]);
        }
    }

    public function loadNews()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://trainznation.eu/api/blog/first",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return "cURL Error #:" . $err;
        } else {
            $json = json_decode($response);
            dd($json);
            /*var_dump($json->data);
            die();*/
            ob_start();
            ?>
            <div class="card">
                <div class="card-image waves-effect waves-block waves-light">
                    <a href="https://trainznation.eu/blog/<?= $json->data->slug ?>">
                        <img src="https://trainznation.eu/storage/blog/<?= $json->data->id ?>.png" alt="item-img">
                    </a>
                    <h4 class="card-title text-shadow m-0"><?= $json->data->title ?></h4>
                </div>
                <ul class="card-action-buttons">
                    <li>
                        <a href="https://trainznation.eu/blog/<?= $json->data->slug ?>" class="btn-floating waves-effect waves-light teal accent-4">
                            <i class="material-icons">remove_red_eye</i>
                        </a>
                    </li>
                </ul>
                <div class="card-content">
                    <a href="#!"></a>
                    <p class="row mb-1">
                        <small class="right">
                            <a href="#!">
                                <span class="new badge red accent-2" data-badge-caption="<?= $json->data->categorie->name; ?>"></span>
                            </a>
                        </small>
                        <small class="left"><?= Carbon::createFromTimestamp(strtotime($json->data->published_at))->format("d/m/Y à H:i") ?></small>
                    </p>
                    <p class="item-post-content"><?= Str::limit($json->data->content, 100); ?></p>
                </div>
                <div class="card-reveal">
                      <span class="card-title grey-text text-darken-4">
                        <i class="material-icons right">close</i> <?= $json->data->title ?></span>
                    <p><?= $json->data->content ?></p>
                </div>
            </div>
            <?php
            $content = ob_get_clean();

            return response()->json(["data" => $content]);
        }
    }

    public function postSuggest(Request $request)
    {
        $connect = new Connect();
        $result = $connect->post('suggestion', [
                "name"  => $request->name,
            "suggestion"=> $request->suggestion
        ]);

        $json = json_decode($result);

        if($json->success == true)
        {
            toastr()->success($json->message, "Poste d'une suggestion", ["positionClass" => "toast-top-full-width"]);
            return redirect()->back();
        }else{
            toastr()->error($json->message, "Erreur", ["positionClass" => "toast-top-full-width"]);
            return redirect()->back();
        }
    }
}
