<?php

namespace App\Http\Controllers\Element;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ElementController extends Controller
{
    public function comment()
    {
        return view('Element.comment');
    }

    public function keyword()
    {
        return view('Element.keyword');
    }

    public function operator()
    {
        return view("Element.operator");
    }

    public function type()
    {
        return view("Element.type");
    }

    public function array()
    {
        return view("Element.array");
    }
}
