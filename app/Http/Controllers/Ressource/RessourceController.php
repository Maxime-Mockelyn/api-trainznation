<?php

namespace App\Http\Controllers\Ressource;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RessourceController extends Controller
{
    public function index()
    {
        return view("Ressource.index");
    }

    public function show($id)
    {
        return view("Ressource.show", [
            "id"    => $id
        ]);
    }
}
