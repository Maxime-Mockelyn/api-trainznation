<?php
namespace App\Repository\Api;

use App\Model\Subcategory;

class SubcategoryRepository
{
    /**
     * @var Subcategory
     */
    private $subcategory;

    /**
     * SubcategoryRepository constructor.
     * @param Subcategory $subcategory
     */

    public function __construct(Subcategory $subcategory)
    {
        $this->subcategory = $subcategory;
    }

}

        