<?php
namespace App\Repository\Api;

use App\Model\Category;

class CategoryRepository
{
    /**
     * @var Category
     */
    private $category;

    /**
     * CategoryRepository constructor.
     * @param Category $category
     */

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

}

        