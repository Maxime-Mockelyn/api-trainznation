<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);
Route::get('search', ["as" => "search", "uses" => "HomeController@search"]);
Route::get('result', ["as" => "result", "uses" => "HomeController@result"]);

Route::get('/loadnews', 'HomeController@loadNews');

Route::group(["prefix" => "element", "namespace" => "Element"], function (){
    Route::get('comment', ["as" => "Element.comment", "uses" => "ElementController@comment"]);
    Route::get('keyword', ["as" => "Element.keyword", "uses" => "ElementController@keyword"]);
    Route::get('operator', ["as" => "Element.operator", "uses" => "ElementController@operator"]);
    Route::get('type', ["as" => "Element.type", "uses" => "ElementController@type"]);
    Route::get('array', ["as" => "Element.array", "uses" => "ElementController@array"]);
});

Route::group(["prefix" => "gso", "namespace" => "Gso"], function (){
    Route::get('asset', ["as" => 'Gso.asset', "uses" => 'GsoController@asset']);
    Route::get('game_object_id', ["as" => 'Gso.gameobjectid', "uses" => 'GsoController@gameobjectid']);
    Route::group(["prefix"  => "game_object", "namespace" => "GameObject"], function (){
        Route::get('/', ["as" => 'Gso.gameobject', "uses" => 'GameObjectController@index']);
        Route::get('browser', ["as" => 'Gso.gameobject.browser', "uses" => 'GameObjectController@browser']);
        Route::get('constructor', ["as" => "Gso.gameobject.constructor", "uses" => "GameObjectController@constructor"]);
        Route::get('interface', ["as" => "Gso.gameobject.interface", "uses" => "GameObjectController@interface"]);
        Route::get('interlocking', ["as" => "Gso.gameobject.interlocking", "uses" => "GameObjectController@interlocking"]);
        Route::get('train', ["as" => "Gso.gameobject.train", "uses" => "GameObjectController@train"]);
    });
});

Route::group(["prefix" => "referenciel", "namespace" => "Referenciel"], function (){
    Route::get('message', ["as" => "Referenciel.message", "uses" => "ReferencielController@message"]);
    Route::get('miniBrowser', ["as" => "Referenciel.miniBrowser", "uses" => "ReferencielController@miniBrowser"]);
    Route::get('browser_message', ["as" => "Referenciel.browser_message", "uses" => "ReferencielController@browser_message"]);
});

Route::group(["prefix" => "ressource", "namespace" => "Ressource"], function (){
    Route::get('/', ["as" => "Ressource.index", "uses" => "RessourceController@index"]);
    Route::get('/{id}', ["as" => "Ressource.show", "uses" => "RessourceController@show"]);
});

Route::get('code', 'TestController@code');

Route::post('/postSuggest', ["as" => "Post.suggest", "uses" => "HomeController@postSuggest"]);