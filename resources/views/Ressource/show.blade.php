@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Liste des Ressources</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li class="active">Ressource</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
@if($id == 1)
    <div class="section">
        <div class="row">
            <div class="col s3">
                <div class="card">
                    <div class="card-image">
                        <img src="https://nsm09.casimages.com/img/2019/06/10//19061006003124396216268699.png" alt="sample">
                    </div>
                    <div class="card-content">
                        <p>Le but de cette objet est d'ouvrir et de fermer une porte de à une certaine heure</p>
                    </div>
                    <div class="card-action">
                        <a href="https://mega.nz/#!6nggXSxC!BEmXZwDKoc5Nr1M87N7gDE4M35PrSDtrFm-MC9ce3Hc">Télécharger la ressource</a>
                        <a href="https://trainznation.eu/tutoriel/6/ouverture-et-fermeture-dune-porte-suivant-horaire">Voir le tutoriel</a>
                    </div>
                </div>
            </div>
            <div class="col s9">
                <div class="card">
                    <div class="card-content">
                        <h1 class="header">Ouverture & Fermeture d'une porte</h1>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Procédure</span>
                        <ul>
                            <li>1. Création de l'asset principal</li>
                            <li>2. Création de la porte & de sont animation</li>
                            <li>3. Création du fichier de config</li>
                            <li>4. Création du script</li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <span class="card-title">Script & Explication</span>
                        <pre>
                            <code class="language-javascript" data-language="javascript">
include "MapObject.gs"

class kiosque isclass MapObject
{
  thread void animation (void);

  public void Init(void)
  {
    inherited();
    animation();
  }

  thread void animation (void){

  int i=1;

  while(i==1){

	float hour = World.GetGameTime();
	if((hour>=0.791676) or (hour<=0.333368)) SetMeshAnimationState("portes",true);
	else SetMeshAnimationState("portes",false);
	Sleep(1);}
                              }
};
                                </code>
                            </pre>
                        <p>
                            En premiers lieu nous utilisons ici un thead qui nous permettera d'écouter les minutes qui passe dans le jeu de façon non visible (Asynchrone).<br>
                            Dans ce Thread, qui déclare la fonction <i>animation</i> nous créons une fariable <i>hour</i> de type float qui nous permettera de sauvegarder le temps du jeu.<br>
                            Ensuite, nous verifions que l'heure soit plus grand de une heure déterminer (ici: 0.791676 / environ 19h) et que l'heure soit plus petite ou égale à une autre heure déterminer
                            (ici: 0.333368 / Environ 8h). Si cette fonction est vrai alors nous déclenchons l'ouverture de la porte, sinon, nous la fermons.<br>
                            Le tout dans un <i>while</i> qui à pour valeur de référence 1 minute. (int i=1)
                        </p>
                        <br>
                        <div id="card-alert" class="card cyan">
                            <div class="card-content white-text">
                                <img src="/assets/images/avatar/avatar-3.png" alt="avatar" class="alert-circle responsive-img valign profile-image">
                                <span class="card-title white-text darken-1">Information</span>
                                <p>Le fichier de resource comprend le cdp ainsi qu'un tableur permettant de convertir l'heure réel en valeur pour trainz.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection

@section("script")

@endsection