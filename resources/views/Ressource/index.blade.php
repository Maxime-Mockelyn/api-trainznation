@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Liste des Ressources</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li class="active">Ressource</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <div class="row">
        <div class="col s4">
            <div class="card blue-grey darken-4" style="background-image: url(https://nsm09.casimages.com/img/2019/06/10//19061006003124396216268699.png); background-size: cover;">
                <div class="card-content">
                    <span class="card-title font-weight-400 mb-10">Ouverture & Fermeture</span>
                    <p>Cette objet animé s'ouvre et ce ferme à une certaine heure de la journée</p>
                    <div class="border-non mt-5">
                        <a class="waves-effect waves-light btn red border-round box-shadow">Voir la ressource</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")

@endsection