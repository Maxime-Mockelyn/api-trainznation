<!-- START FOOTER -->
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
          <span>Copyright ©
            <script type="text/javascript">
              document.write(new Date().getFullYear());
            </script> <a class="grey-text text-lighten-2" href="https://trainznation.eu" target="_blank">Trainznation</a> All rights reserved.</span>
            <span class="right hide-on-small-only"> Design and Developed by <a class="grey-text text-lighten-2" href="http://be.net/maximemockelyn">Mockelyn Maxime</a></span>
        </div>
    </div>
</footer>
<!-- END FOOTER -->