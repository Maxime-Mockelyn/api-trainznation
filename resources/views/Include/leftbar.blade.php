<!-- START LEFT SIDEBAR NAV-->
<aside id="left-sidebar-nav">
    <ul id="slide-out" class="side-nav fixed leftside-navigation">
        <li class="no-padding">
            <ul class="collapsible" data-collapsible="accordion">
                <li class="bold {{ currentRoute('home') }}">
                    <a href="{{ route('home') }}">
                        <i class="material-icons">dashboard</i>
                        <span class="nav-text">Accueil</span>
                    </a>
                </li>
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan {{
                    currentRoute('Element.comment') ||
                    currentRoute('Element.keyword') ||
                    currentRoute('Element.operator') ||
                    currentRoute('Element.type') ||
                    currentRoute('Element.array')
                    }}">
                        <i class="material-icons">gavel</i>
                        <span class="nav-text">Element de programmation</span>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li class="{!! currentRoute("Element.comment") !!}">
                                <a href="{{ route('Element.comment') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Commentaire & Délimitation</span>
                                </a>
                            </li>
                            <li class="{{ currentRoute("Element.keyword") }}">
                                <a href="{{ route('Element.keyword') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Mots-Clé</span>
                                </a>
                            </li>
                            <li class="{{ currentRoute("Element.operator") }}">
                                <a href="{{ route('Element.operator') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Opérateurs</span>
                                </a>
                            </li>
                            <li class="{{ currentRoute("Element.type") }}">
                                <a href="{{ route('Element.type') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Type</span>
                                </a>
                            </li>
                            <li class="{{ currentRoute("Element.array") }}">
                                <a href="{{ route('Element.array') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Les tableaux</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">gavel</i>
                        <span class="nav-text">GSO Object</span>
                    </a>
                    <div class="collapsible-body">
                        <ul class="collapsible" data-collapsible="accordion">
                            <li>
                                <a href="{{ route('Gso.asset') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Asset</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('Gso.gameobjectid') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>GameObjectId</span>
                                </a>
                            </li>
                            <li class="bold">
                                <a class="collapsible-header waves-effect waves-cyan">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span class="nav-text">GameObject</span>
                                </a>
                                <div class="collapsible-body">
                                    <ul class="collapsible" data-collapsible="accordion">
                                        <li>
                                            <a href="{{ route('Gso.gameobject') }}">
                                                <i class="material-icons">keyboard_arrow_right</i>
                                                <span>Classe</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Gso.gameobject.browser') }}">
                                                <i class="material-icons">keyboard_arrow_right</i>
                                                <span>Browser</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Gso.gameobject.constructor') }}">
                                                <i class="material-icons">keyboard_arrow_right</i>
                                                <span>Constructors</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Gso.gameobject.interface') }}">
                                                <i class="material-icons">keyboard_arrow_right</i>
                                                <span>Interface</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Gso.gameobject.interlocking') }}">
                                                <i class="material-icons">keyboard_arrow_right</i>
                                                <span>InterlockingTowerPath</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan">
                        <i class="material-icons">book</i>
                        <span class="nav-text">Référenciel</span>
                    </a>
                    <div class="collapsible-body">
                        <ul>
                            <li>
                                <a href="{{ route('Referenciel.message') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Message</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('Referenciel.miniBrowser') }}">
                                    <i class="material-icons">keyboard_arrow_right</i>
                                    <span>Mini Browser</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="bold {{ currentRoute('home') }}">
                    <a href="{{ route('Ressource.index') }}">
                        <i class="material-icons">file_download</i>
                        <span class="nav-text">Ressources</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only">
        <i class="material-icons">menu</i>
    </a>
</aside>
<!-- END LEFT SIDEBAR NAV-->