@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Bienvenue</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li class="active">Accueil</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <div class="row">
        <div class="col s12 m12 l6">
            <ul class="collection with-header">
                <li class="collection-header teal accent-4">
                    <h4 class="task-card-title">Derniers Ajouts</h4>
                    <p class="task-card-date">30 Aout 2019 à 18:05</p>
                </li>
                <li class="collection-item">
                    <i class="material-icons small green-text">add_circle_outline</i>
                    <span>Ajout de la classe <a href="/gso/game_object/train">Train</a></span>
                    <span class="secondary-content"> 30/08/2019 à 18:05</span>
                </li>
                <li class="collection-item">
                    <i class="material-icons small green-text">add_circle_outline</i>
                    <span>Ajout de la classe <a href="/gso/game_object/interface">Interface</a></span>
                    <span class="secondary-content"> 16/06/2019 à 22:19</span>
                </li>
                <li class="collection-item">
                    <i class="material-icons small green-text">add_circle_outline</i>
                    <span>
                        Ajout d'un bouton sur le coté droit en bas permettant:
                        <ul>
                            <li>D'acceder au site: trainznation</li>
                            <li>De soumettre une suggestion</li>
                        </ul>
                    </span>
                    <span class="secondary-content"> 16/06/2019 à 21:20</span>
                </li>
                <li class="collection-item">
                    <i class="material-icons small green-text">add_circle_outline</i>
                    <span>Ajout du système de news en page d'accueil</span>
                    <span class="secondary-content"> 15/06/2019 à 19:20</span>
                </li>
                <li class="collection-item">
                    <i class="material-icons small green-text">add_circle_outline</i>
                    <span>Ajout de cette liste de tache</span>
                    <span class="secondary-content"> 11/06/2019 à 21:08</span>
                </li>
                <li class="collection-item">
                    <i class="material-icons small green-text">add_circle_outline</i>
                    <span>Ajout de la classe <a href="/gso/game_object/constructor">Constructors</a></span>
                    <span class="secondary-content"> 11/06/2019 à 21:04</span>
                </li>
            </ul>
            <div class="card horizontal">
                <div class="card-image">
                    <img src="https://syltheron.gallerycdn.vsassets.io/extensions/syltheron/configtrainzsnippet/1.0.35/1559914084198/Microsoft.VisualStudio.Services.Icons.Default">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h4>ConfigTrainzSnippet</h4>
                        <i>Version: 1.0.35</i>
                        <p>Générateur de configuration pour fichier 'config.txt'</p><br>
                        <i>Vous devez posseder <a href="https://code.visualstudio.com/">Visual Studio Code</a></i>
                    </div>
                    <div class="card-action">
                        <a href="https://marketplace.visualstudio.com/items?itemName=syltheron.configtrainzsnippet" class="waves-effect waves-light btn gradient-45deg-red-pink">Télécharger</a>
                    </div>
                </div>
            </div>
            <div class="card horizontal">
                <div class="card-image">
                    <img src="https://syltheron.gallerycdn.vsassets.io/extensions/syltheron/scripttrainzsnippet/0.0.3/1559913408464/Microsoft.VisualStudio.Services.Icons.Default">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <h4>ScriptTrainzSnippet</h4>
                        <i>Version: 0.0.61</i>
                        <p>Permet d'autocompleter les informations de la bibliothèque API des scripts de Trainz.</p><br>
                        <i>Vous devez posseder <a href="https://code.visualstudio.com/">Visual Studio Code</a></i>
                    </div>
                    <div class="card-action">
                        <a href="https://marketplace.visualstudio.com/items?itemName=syltheron.scripttrainzsnippet" class="waves-effect waves-light btn gradient-45deg-red-pink">Télécharger</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m12 l6" id="news">

        </div>
    </div>
</div>
@endsection

@section("script")
<script type="text/javascript">
    function loadNews(){
        let div = $("#news");

        $.ajax({
            url: '/loadnews',
            success: function (data) {
                div.html(data.data)
            }
        })
    }

    (function ($) {
        loadNews()
    })(jQuery)
</script>
@endsection