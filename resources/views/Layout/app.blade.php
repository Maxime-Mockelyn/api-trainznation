@include("Include.header")
@include("Include.topbar")
<!-- //////////////////////////////////////////////////////////////////////////// -->
<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">
        @include("Include.leftbar")
        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START CONTENT -->
        <section id="content">
            <!--breadcrumbs start-->
            @yield("bread")
            <!--breadcrumbs end-->
            <!--start container-->
            <div class="container">
                @yield("content")
                <!-- Floating Action Button -->
                <div id="floating" class="fixed-action-btn " style="bottom: 50px; right: 19px;">
                    <a class="btn-floating btn-large">
                        <i class="material-icons">help_outline</i>
                    </a>
                    <ul>
                        <li>
                            <a href="https://trainznation.eu" class="btn-floating blue tooltipped" data-tooltip="Site Trainznation" target="_blank">
                                <i class="material-icons">http</i>
                            </a>
                        </li>
                        <li>
                            <a href="#modal1" class="btn-floating green tooltipped modal-trigger" data-tooltip="Vous avez une suggestion ?">
                                <i class="material-icons">question_answer</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- Floating Action Button -->
            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->
    </div>
    <!-- END WRAPPER -->
</div>
<div id="modal1" class="modal modal-fixed-footer">
    <form action="{{ route('Post.suggest') }}" method="POST">
        @csrf
        <div class="modal-content">
            <h4>Vous avez une suggestion ?</h4>
            <div class="row">
                <div class="input-field col s12">
                    <input type="text" id="name" name="name">
                    <label for="name">Votre nom ou Pseudo</label>
                </div>
                <div class="input-field col s12">
                    <textarea class="materialize-textarea" id="suggestion" name="suggestion"></textarea>
                    <label for="suggestion">Votre suggestion</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="modal-action btn cyan waves-effect waves-light">Soumettre <i class="material-icons right">send</i></button>
            <button type="button" class="modal-action modal-close waves-effect waves-green btn-flat ">Annuler</button>
        </div>
    </form>
</div>
<!-- END MAIN -->
<!-- //////////////////////////////////////////////////////////////////////////// -->
@include("Include.footer")
<!-- ================================================
    Scripts
================================================ -->
<!-- jQuery Library -->
<script type="text/javascript" src="/assets/vendors/jquery-3.2.1.min.js"></script>
<!--materialize js-->
<script type="text/javascript" src="/assets/js/materialize.min.js"></script>

<script type="text/javascript" src="/assets/vendors/prism/prism.js"></script>
<!--scrollbar-->
<script type="text/javascript" src="/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="/assets/js/plugins.js"></script>
<script type="text/javascript" src="/assets/js/vendors/uxtour/uxtour.js"></script>
<!--custom-script.js - Add your own theme custom JS-->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="/assets/js/custom-script.js"></script>
{!! toastr()->render() !!}
@yield("script")

</body>
</html>