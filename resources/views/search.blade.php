@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">{{ $result }} Résultat pour la recherche: {{ $search }}</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li class="active">Résultat de la recherche</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    @if($result == 0)
    <div class="section">
        {{ $text }}
    </div>
    @else
        @if($search == 'array')
            <div class="row">
                <div class="col s12">
                    @foreach($results as $array)
                        <div class="card horizontal">
                            <div class="card-image width-65">
                                <img src="/assets/images/custom/search/array.png">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <h5>{{ $array['title'] }}</h5>
                                    <p>{{ $array['text'] }}</p>
                                </div>
                                <div class="card-action border-none">
                                    <a href="{{ $array['link'] }}" class="waves-effect waves-light btn box-shadow">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        @if($search == 'DoesMatch')
            <div class="row">
                <div class="col s12">
                    @foreach($results as $array)
                        <div class="card horizontal">
                            <div class="card-image width-65">
                                <img src="/assets/images/custom/search/doesMatch.png">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <h5>{{ $array['title'] }}</h5>
                                    <p>{{ $array['text'] }}</p>
                                </div>
                                <div class="card-action border-none">
                                    <a href="{{ $array['link'] }}" class="waves-effect waves-light btn box-shadow">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        @if($search == 'message')
            <div class="row">
                <div class="col s12">
                    @foreach($results as $array)
                        <div class="card horizontal">
                            <div class="card-image width-65">
                                <img src="/assets/images/custom/search/message.png">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <h5>{!! $array['title'] !!}</h5>
                                    <p>{{ $array['text'] }}</p>
                                </div>
                                <div class="card-action border-none">
                                    <a href="{{ $array['link'] }}" class="waves-effect waves-light btn box-shadow">En savoir plus</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    @endif
@endsection

@section("script")

@endsection