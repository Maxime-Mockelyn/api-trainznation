@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>GameObjectId</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li class="active">GameObjectId</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <ul>
        <li><i class="material-icons">navigate_next</i> GameObjectID est une classe permettant d'implémenter un identifiant unique pour un objet de script de jeu spécifique.</li>
        <li><i class="material-icons">navigate_next</i> Cet identifiant unique est persistant dans toutes les instances de jeu et convient pour être enregistré / chargé dans GetProperties / SetProperties.</li>
        <li><i class="material-icons">navigate_next</i> GameObjectID est destiné à remplacer l'ID unique hérité de GameObjects, obsolète depuis Tane SP2, qui n'était pas persistant entre les instances de session de jeu successives.</li>
        <li><i class="material-icons">navigate_next</i> Cette classe est disponible depuis Tane SP2 (4.5)</li>
        <li><i class="material-icons">navigate_next</i> final game class GameObjectID isclass GSObject</li>
    </ul>

    <div class="row">
        <div class="col s12 m9 l10">
            <div id="doesMatch-gameobjectid" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>DoesMatch</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white"><strong>public native bool DoesMatch(GameObjectID otherID);</strong></p><br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>otherID = </strong> autre instance GameObjectID passée à vérifier si elle correspond à l'instance initiale GameObjectID.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Booléen (true/false)</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Indique si ce GameObjectID correspond à l'ID transmis. Les GameObjectID DOIVENT être soumis à un test d’égalité à l’aide de cette fonction. Tester avec des opérateurs standard ('==' et '! =') Ne fonctionnera pas.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
bool isSameObject = gameObjectID1.DoesMatch(gameObjectID2);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="doesMatch-gsobject" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>DoesMatch</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native bool DoesMatch(GSObject otherObject);</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>otherObject = </strong> l'objet à tester. Ce doit être un GameObject.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Booléen (true/false)</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Indique si ce GameObjectID correspond à l'objet transmis.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
bool isSameObject = gameObjectID1.DoesMatch(otherObject);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="getDebugString" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetDebugString</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native string GetDebugString(void);</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> string (Chaine de caractère)</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Renvoie une chaîne décrivant le contenu de cet ID, à utiliser dans les journaux de débogage. Le format de cette chaîne n'est pas garanti, n'essayez pas de l'analyser par une machine.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
string debugString = gameObjectID.GetDebugString();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="serializeToString" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SerializeToString</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native string SerialiseToString(void);</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> string (Chaine de caractère)</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Retourne une chaîne lisible par machine représentant ce GameObjectID et analysable par <i>Router.SerialiseGameObjectIDFromString()</i>. Cela permet à un GameObjectID d'être temporairement stocké dans un lien HTML ou similaire.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
string serialString = gameObjectID.SerialiseToString();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#doesMatch-gameobjectid">DoesMatch par GameObjectID</a></li>
                    <li><a href="#doesMatch-gsobject">DoesMatch par GSObject</a></li>
                    <li><a href="#getDebugString">GetDebugString</a></li>
                    <li><a href="#serializeToString">SerializeToString</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection