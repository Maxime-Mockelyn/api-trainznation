@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2"
                       placeholder="Tapez une fonction"/>
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>Interface</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li><a href="">Gameobject</a></li>
                        <li class="active">Interface</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="section">
        <h2 class="header">Classe Interface</h2>
        <ul>
            <li><i class="material-icons">navigate_next</i> La classe Interface fournit aux scripts des méthodes
                permettant de gérer l'interface utilisateur.
            </li>
        </ul>
        <ul class="tabs">
            <li class="tab col s3"><a href="#constant">Constantes & Messages</a></li>
            <li class="tab col s3"><a href="#method">Methodes</a></li>
        </ul>
        <div id="constant">
            <div class="row">
                <div class="col s12 m9 l10">
                    <h4 class="header">Constantes & Messages</h4>
                    <div id="RVBColor" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>Constante de couleur RVB</h4>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Valeur de couleur prédéfinie 24bits</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>public define int Colour_Red = 0xFF0000</td>
                                        <td>Rouge</td>
                                    </tr>
                                    <tr>
                                        <td>public define int Colour_Green = 0x00FF00</td>
                                        <td>Vert</td>
                                    </tr>
                                    <tr>
                                        <td>public define int Colour_Blue = 0x0000FF</td>
                                        <td>Bleu</td>
                                    </tr>
                                    <tr>
                                        <td>public define int Colour_Yellow = 0xFFFF20</td>
                                        <td>Jaune</td>
                                    </tr>
                                    <tr>
                                        <td>public define int Colour_Cyan = 0xB0C0FF</td>
                                        <td>Cyan</td>
                                    </tr>
                                    <tr>
                                        <td>public define int Colour_White = 0xFFFFFF</td>
                                        <td>Blanc</td>
                                    </tr>
                                    <tr>
                                        <td>public define int Colour_Black = 0x000000</td>
                                        <td>Noir</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="button" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>Indice d'élément de bouton</h4>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Constantes faisant référence aux boutons du menu Pilote</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>public define int ButtonBar_Element_Close = 0</td>
                                        <td>Bouton de sortie</td>
                                    </tr>
                                    <tr>
                                        <td>public define int ButtonBar_Element_Pause = 1</td>
                                        <td>Bouton de pause</td>
                                    </tr>
                                    <tr>
                                        <td>public define int ButtonBar_Element_Save = 2</td>
                                        <td>Bouton de sauvegarde</td>
                                    </tr>
                                    <tr>
                                        <td>public define int ButtonBar_Element_Performance = 3</td>
                                        <td>Bouton de paramètre de performance</td>
                                    </tr>
                                    <tr>
                                        <td>public define int ButtonBar_Element_MetricImp = 4</td>
                                        <td>Bouton Impérial/Métrique</td>
                                    </tr>
                                    <tr>
                                        <td>public define int ButtonBar_Element_Find = 5</td>
                                        <td>Bouton de recherche</td>
                                    </tr>
                                    <tr>
                                        <td>public define int ButtonBar_Element_Help = 6</td>
                                        <td>Bouton d'aide</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="message" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>Messages associés</h4>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Les messages envoyés vers et depuis
                                        des objets de l'industrie sont répertoriés ci-dessous:
                                    </li>
                                </ul>
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Majeur</th>
                                        <th>Mineur</th>
                                        <th>Source</th>
                                        <th>Destination</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><a class="modal-trigger" href="#interface-event">Interface-Event</a></td>
                                        <td>MapViewOff</td>
                                        <td>?</td>
                                        <td>Diffusé (Broadcast)</td>
                                    </tr>
                                    <tr>
                                        <td><a class="modal-trigger" href="#interface-event">Interface-Event</a></td>
                                        <td>MapViewOn</td>
                                        <td>?</td>
                                        <td>Diffusé (Broadcast)</td>
                                    </tr>
                                    <tr>
                                        <td><a class="modal-trigger" href="#interface-event">Interface-Event</a></td>
                                        <td>Mouse-Enter</td>
                                        <td>MapObject</td>
                                        <td>Diffusé (Broadcast)</td>
                                    </tr>
                                    <tr>
                                        <td><a class="modal-trigger" href="#interface-event">Interface-Event</a></td>
                                        <td>Mouse-Leave</td>
                                        <td>?</td>
                                        <td>Diffusé (Broadcast)</td>
                                    </tr>
                                    <tr>
                                        <td><a class="modal-trigger" href="#interface-event">Interface-Event</a></td>
                                        <td>Set-Metric-Mode</td>
                                        <td>?</td>
                                        <td>Diffusé (Broadcast)</td>
                                    </tr>
                                    <tr>
                                        <td><a class="modal-trigger" href="#interface-event">Interface-Event</a></td>
                                        <td>Toggle-Interface</td>
                                        <td>?</td>
                                        <td>Diffusé (Broadcast)</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col hide-on-small-only m3 l2">
                    <div class="toc-wrapper">
                        <br>
                        <br>
                        <br>
                        <ul class="section table-of-contents">
                            <li><strong>Constantes & Messages</strong></li>
                            <li><a href="#RVBColor">Constante de couleur RVB</a></li>
                            <li><a href="#button">Indice d'élément de bouton</a></li>
                            <li><a href="#message">Messages associés</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="method">
            <div class="row">
                <div class="col s12 m9 l10">
                    <h4 class="header">Methodes</h4>
                    <div id="AdjustScore" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>AdjustScore</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public void AdjustScore(int delta)</strong></p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>delta = </strong>
                                                incrémentation positive ou négative à appliquer au score.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="Exception" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>Exception</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void Exception(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message = </strong>
                                                Message à afficher et consigné lorsque l'exception se produit.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Autorise explicitement le
                                                déclenchement d'une exception par un script en cas d'erreur majeure.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.Exception("Fatal Error");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetDecoupleMode" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetDecoupleMode</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool GetDecoupleMode(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucun</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Renvoie le mode de
                                                découplage actuel à partir de l'interface du pilote.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le mode de découpler peut
                                                être activé ou désactivé lorsque l'utilisateur clique sur l'icône de
                                                découpler situé sur la barre de boutons du pilote ou appuie sur les
                                                touches <i>Ctrl + D</i> .
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Lorsque l'interface est en
                                                mode découplage, l'icône Découpler est mise en surbrillance.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool dcMode = Interface.GetDecoupleMode();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetDisplayHeight" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetDisplayHeight</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native int GetDisplayHeight(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucun</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La hauteur de l'affichage de
                                                l'utilisateur en pixels.
                                            </li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le mode de découpler peut être activé ou désactivé lorsque l'utilisateur clique sur l'icône de découpler situé sur la barre de boutons du pilote ou appuie sur les touches <i>Ctrl + D</i> .</li>
                                            <li><i class="material-icons">navigate_next</i> Lorsque l'interface est en mode découplage, l'icône Découpler est mise en surbrillance.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
int height = Interface.GetDisplayHeight();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetDisplayWidth" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetDisplayWidth</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native int GetDisplayWidth(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucun</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La largeur de l'écran de
                                                l'utilisateur en pixels.
                                            </li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le mode de découpler peut être activé ou désactivé lorsque l'utilisateur clique sur l'icône de découpler situé sur la barre de boutons du pilote ou appuie sur les touches <i>Ctrl + D</i> .</li>
                                            <li><i class="material-icons">navigate_next</i> Lorsque l'interface est en mode découplage, l'icône Découpler est mise en surbrillance.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
int width = Interface.GetDisplayWidth();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetElementProperty" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetElementProperty</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native string GetElementProperty(string elementId, string
                                        propertyId)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>elementId</strong> =
                                                élément à interroger.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>propertyId</strong>
                                                = Propriété à interroger.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La valeur de la propriété
                                                spécifiée si elle existe, null sinon.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le mode de découpler peut
                                                être activé ou désactivé lorsque l'utilisateur clique sur l'icône de
                                                découpler situé sur la barre de boutons du pilote ou appuie sur les
                                                touches <i>Ctrl + D</i> .
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Lorsque l'interface est en
                                                mode découplage, l'icône Découpler est mise en surbrillance.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
int width = Interface.GetDisplayWidth();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetInterfaceVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetInterfaceVisible</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool GetInterfaceVisible(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> True si l'interface du
                                                pilote est visible, false sinon.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Toute l’interface du mode
                                                Pilote peut être masquée avec la touche F5 .
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool visible = Interface.GetInterfaceVisible();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetMapView" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetMapView</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool GetMapView(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> True si la fenêtre
                                                d’affichage de la carte est visible, false sinon.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La mini-carte peut être
                                                affichée ou masquée en appuyant sur M ou Ctrl + M
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool visible = Interface.GetMapView();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetMessageWindowVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetMessageWindowVisible</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool GetMessageWindowVisible(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> True si la fenêtre de
                                                message en haut de l'écran est visible, false sinon.
                                            </li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La mini-carte peut être affichée ou masquée en appuyant sur M ou Ctrl + M</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool visible = Interface.GetMessageWindowVisible();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetMetricMode" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetMetricMode</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool GetMetricMode(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Vrai si l'interface affiche
                                                des unités métriques, faux pour impérial.
                                            </li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La mini-carte peut être affichée ou masquée en appuyant sur M ou Ctrl + M</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool imperial = !Interface.GetMetricMode();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetObjectiveBarVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetMetricMode</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool GetObjectiveBarVisible()</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> True si le panneau de
                                                l'objectif Pilote est visible, false sinon.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La visibilité de la barre
                                                d’objectif peut être basculée avec la touche <i>F6</i></li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool objectivesShown = Interface.GetObjectiveBarVisible();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetScore" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetScore</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public int GetScore(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le score actuel.</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La visibilité de la barre d’objectif peut être basculée avec la touche <i>F6</i></li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
int score = GetScore();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetTimeStamp" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetTimeStamp</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native string GetTimeStamp(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Un horodatage en temps réel
                                                en secondes.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Retourne l'heure actuelle
                                                exprimée en secondes écoulées depuis minuit le 1er janvier 1970 UTC.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> La valeur de retour est une
                                                chaîne, Trainzscript ne prenant en charge aucun type entier étendu.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
string timestamp = Interface.GetTimeStamp();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="GetWaybillWindowVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>GetWaybillWindowVisible</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public bool GetWaybillWindowVisible()</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Vrai si la fenêtre Feuille
                                                de route est affichée, Faux dans le cas contraire.
                                            </li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Retourne l'heure actuelle exprimée en secondes écoulées depuis minuit le 1er janvier 1970 UTC.</li>
                                            <li><i class="material-icons">navigate_next</i> La valeur de retour est une chaîne, Trainzscript ne prenant en charge aucun type entier étendu.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
bool waybillVisible = GetWaybillWindowVisible();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HasUnseenMessages" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>HasUnseenMessages</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native bool HasUnseenMessages(void)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Vrai s'il y a des messages
                                                destinés à la fenêtre de message du pilote qui ne sont pas affichés,
                                                faux sinon.
                                            </li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Il y a des messages non vus
                                                si la fenêtre de message est invisible alors que des messages ont été
                                                reçus.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Ceci est indiqué à
                                                l'utilisateur par l'icône de microphone en surbrillance sur la barre de
                                                boutons du pilote.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
if (Interface.HasUnseenMessages()) Interface.SetMessageWindowVisible(true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="HighlightButtonBarElement" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>HighlightButtonBarElement</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void HighlightButtonBarElement(int buttonIndex, bool
                                        state)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>buttonIndex</strong>
                                                = Une des valeurs d' <a href="#constant">index d'élément de bouton</a>
                                                définissant le bouton à surligner.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>state</strong> =
                                                True pour activer la surbrillance, false pour le désactiver.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Met en surbrillance un
                                                bouton du menu Pilote activé ou désactivé.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode permet de
                                                mettre en surbrillance un bouton du menu Pilote situé dans le coin
                                                supérieur gauche de l’écran, comme si le curseur de la souris était
                                                au-dessus de ce bouton.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Notez que le menu Pilote
                                                n’est pas toujours visible, mais vous pouvez l’afficher explicitement
                                                avec <a href="#ShowDriverButtonMenu">ShowDriverButtonMenu()</a>.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.HighlightButtonBarElement(ButtonBar_Element_Pause,true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="Log" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>Log</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void Log(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                Chaîne de texte à imprimer dans le fichier journal.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Imprime une chaîne de texte
                                                dans le fichier journal <i>jetlog.txt</i> .
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Voir aussi <a href="#Print">Print()</a>
                                                .
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Désactivé par défaut dans
                                                TS12.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.Log("Session ended");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="LogCallStack" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>LogCallStack</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void LogCallStack(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                chaîne de texte à inclure dans la pile d'appels et à imprimer dans le
                                                fichier journal.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Imprime une chaîne de texte dans le fichier journal <i>jetlog.txt</i> .</li>
                                            <li><i class="material-icons">navigate_next</i> Voir aussi <a href="#Print">Print()</a> .</li>
                                            <li><i class="material-icons">navigate_next</i> Désactivé par défaut dans TS12.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.LogCallStack("Call Stack Contents");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="LogResult" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>LogResult</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public void LogResult(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                Chaîne de texte à inclure avec le résultat et à imprimer dans le journal
                                                des résultats.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Imprime une chaîne de texte dans le fichier journal <i>jetlog.txt</i> .</li>
                                            <li><i class="material-icons">navigate_next</i> Voir aussi <a href="#Print">Print()</a> .</li>
                                            <li><i class="material-icons">navigate_next</i> Désactivé par défaut dans TS12.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.LogResult("Result Appended");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="Print" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>Print</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public void LogResult(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                Chaîne de texte à imprimer dans le fichier journal et dans la fenêtre de
                                                message.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Imprime une chaîne de texte
                                                dans le fichier journal <i>jetlog.txt</i> et l'affiche dans la fenêtre
                                                de messagerie.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Voir aussi <a href="#Log">Log()</a>
                                                .
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.Print("Session ended");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ResetResults" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>ResetResults</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public void ResetResults()</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucun</li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Efface le journal des
                                                résultats.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.ResetResults();
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetAlert" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetAlert</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetAlert(string message, string iconTexture, float
                                        duration, int colour)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                Texte du message à afficher.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>iconTexture</strong>
                                                = Icône d'alerte, utilisez une chaîne vide sans icône.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>duration</strong> =
                                                durée en secondes pour que l'alerte reste affichée.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>colour</strong> =
                                                Couleur du texte de l'alerte sous forme de RVB 24 bits ou de l'une des
                                                <a href="#constant">constantes de couleur RVB</a>.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Efface le journal des résultats.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetAlert("Train Halted due to leaves on line","",10.0,Interface.Colour_Red);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetDecoupleMode" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetDecoupleMode</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetDecoupleMode(bool enabled)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>enabled</strong> =
                                                utiliser true pour activer le découplage, false pour désactiver.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le mode de découplage de l'
                                                interface du pilote peut être activé ou désactivé à l' aide de l'icône
                                                sur la barre de boutons du pilote ou par pression de touche
                                                <i>Ctrl+D</i> .
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Lorsque l'interface est en
                                                mode découplage, l'icône Découpler est mise en surbrillance.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetDecoupleMode(false);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetElementProperty" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetElementProperty</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetElementProperty(string elementId, string propertyId,
                                        string value)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>elementId</strong> =
                                                élément à mettre à jour.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>propertyId</strong>
                                                = Propriété à mettre à jour.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>value</strong> =
                                                Nouvelle valeur à définir.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode permet de
                                                modifier les propriétés des éléments des fenêtres du navigateur dans
                                                l’interface de Trainz, telles que la mini-carte.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Voir aussi <a
                                                        href="#GetElementProperty">GetElementProperty()</a></li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetElementProperty(elementId,propertyId,value);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetHelperIconScale" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetHelperIconScale</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetHelperIconScale(float scale)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>scale</strong> =
                                                facteur d'échelle à appliquer aux icônes de jonction à l'écran, etc.,
                                                plage 0.5..1.0
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode permet de modifier les propriétés des éléments des fenêtres du navigateur dans l’interface de Trainz, telles que la mini-carte.</li>
                                            <li><i class="material-icons">navigate_next</i> Voir aussi <a href="#GetElementProperty">GetElementProperty()</a></li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetHelperIconScale(0.5); // set to 50% of default size
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetMapView" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetMapView</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetHelperIconScale(float scale)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> L'utilisateur peut afficher
                                                ou masquer la fenêtre d'affichage de la carte en appuyant sur <i>M</i>
                                                ou <i>Ctrl + M</i> sur le clavier.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetMapView(true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetMessageWindowVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetMessageWindowVisible</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetMessageWindowVisible(bool visible)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> L'utilisateur peut afficher ou masquer la fenêtre d'affichage de la carte en appuyant sur <i>M</i> ou <i>Ctrl + M</i> sur le clavier.</li>
                                        </ul>-->
                                        <strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a
                                                        href="#GetMessageWindowVisible">GetMessageWindowVisible()</a>
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <a
                                                        href="#HasUnseenMessages">HasUnseenMessages()</a></li>
                                        </ul>
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
if (Interface.HasUnseenMessages()) Interface.SetMessageWindowVisible(true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetMetricMode" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetMetricMode</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native mode SetMetricMode(bool useMetric)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>useMetric</strong> =
                                                Utilisez true pour sélectionner les unités métriques, false pour
                                                sélectionner impériale.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> L'utilisateur peut afficher ou masquer la fenêtre d'affichage de la carte en appuyant sur <i>M</i> ou <i>Ctrl + M</i> sur le clavier.</li>
                                        </ul>-->
                                        <strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetMetricMode(false);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetObjective" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetObjective</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetObjective(string message, string iconTexture)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                Texte du message objectif.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> <strong>iconTexture</strong>
                                                = Icône à afficher, utilisez une chaîne vide pour omettre l'icône.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode rendra visible
                                                le panneau des objectifs du conducteur.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> L'utilisateur peut masquer
                                                ou afficher le panneau à sa discrétion.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetObjective("Line closed ahead","");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetObjectiveBarVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetObjectiveBarVisible</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetObjectiveBarVisible(bool visible)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> La barre d’objectif Pilote
                                                peut être basculée entre invisible et visible en appuyant sur la touche
                                                <i>F6</i> .
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetObjectiveBarVisible(false);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetObjectiveIcon" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetObjectiveIcon</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetObjectiveIcon(string iconTexture)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>iconTexture</strong>
                                                = Icône à afficher, utilisez une chaîne vide pour supprimer une icône
                                                précédente.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Le fichier image à utiliser
                                                comme icône doit être une image de taille 32x32 et de format TGA.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetObjectiveIcon("");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetResults" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetResults</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetResults(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                chaîne à afficher dans la boîte de résultats.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Affiche les résultats d'un
                                                scénario.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode changera le
                                                menu des activités en écran de résultats et remplira la boîte de
                                                résultats avec la chaîne donnée.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Les résultats seront
                                                visibles lorsque l'utilisateur fermera le module d'activité.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetResults("Result text to display");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetScore" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetScore</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetScore(string message)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>message</strong> =
                                                Message à afficher dans la barre d'activité.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Affiche les résultats d'un scénario.</li>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode changera le menu des activités en écran de résultats et remplira la boîte de résultats avec la chaîne donnée.</li>
                                            <li><i class="material-icons">navigate_next</i> Les résultats seront visibles lorsque l'utilisateur fermera le module d'activité.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetScore("Score");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetTooltip" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetTooltip</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void SetTooltip(Browser tooltip)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>tooltip</strong> =
                                                Navigateur à utiliser comme fenêtre d'info-bulle, si null l'info-bulle
                                                actuelle sera supprimée.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Définit l'info-bulle qui
                                                devrait suivre la souris.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode spécifie que
                                                l'info-bulle doit suivre la souris.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Toute info-bulle existante
                                                sera remplacée.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Si ce navigateur est libéré,
                                                il sera automatiquement supprimé.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetTooltip(tooltip);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="SetWaybillWindowVisible" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>SetWaybillWindowVisible</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public void SetWaybillWindowVisible(bool visible)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Définit l'info-bulle qui devrait suivre la souris.</li>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode spécifie que l'info-bulle doit suivre la souris.</li>
                                            <li><i class="material-icons">navigate_next</i> Toute info-bulle existante sera remplacée.</li>
                                            <li><i class="material-icons">navigate_next</i> Si ce navigateur est libéré, il sera automatiquement supprimé.</li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.SetWaybillWindowVisible(false);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ShowDriverButtonMenu" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>ShowDriverButtonMenu</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void ShowDriverButtonMenu(bool visible)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> </li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.ShowDriverButtonMenu(true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ShowObjective" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>ShowObjective</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void ShowObjective(bool visible)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <!--<strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> </li>
                                        </ul>-->
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.ShowObjective(true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="ShowOnScreenHelp" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>ShowOnScreenHelp</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void ShowOnScreenHelp(bool visible)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>visible</strong> =
                                                Utilisez true pour afficher et false pour masquer.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Cette méthode remplit la
                                                même fonction que l' élément de menu <i>Afficher l'aide à l'écran</i> du
                                                pilote, qui permet d'afficher ou de masquer des noms sur des objets de
                                                carte, tels que des jonctions.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.ShowOnScreenHelp(true);
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="WarnObsolete" class="section scrollspy">
                        <div class="card">
                            <div class="card-content">
                                <h4>WarnObsolete</h4>
                                <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                    <strong>public native void WarnObsolete(string warning)</strong>
                                </p><br>
                                <div class="row">
                                    <div class="col s7">
                                        <strong>Parametres:</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <strong>warning</strong> =
                                                Message pour se connecter à jetlog.txt lors du premier appel de cette
                                                méthode.
                                            </li>
                                        </ul>
                                        <strong>Valeur retournée</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Aucune</li>
                                        </ul>
                                        <strong>Remarque</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> Génère un runtime
                                                avertissant qu'une fonction ou une utilisation donnée est obsolète.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> L'avertissement va au
                                                fichier JetLog.txt et n'est pas visible par l'utilisateur occasionnel
                                                dans Trainz.
                                            </li>
                                            <li><i class="material-icons">navigate_next</i> Un seul avertissement est
                                                généré pour une chaîne d'avertissement donnée.
                                            </li>
                                        </ul>
                                        <!--<strong>Voir également</strong>
                                        <ul>
                                            <li><i class="material-icons">navigate_next</i> <a href="#GetMetricMode">GetMetricMode()</a></li>
                                        </ul>-->
                                    </div>
                                    <div class="col s5">
                                        <strong>Syntax</strong>
                                        <pre><code class="language-javascript" data-language="javascript">
Interface.WarnObsolete("Method is obsolete");
                                </code></pre>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col hide-on-small-only m3 l2">
                    <div class="toc-wrapper">
                        <br>
                        <br>
                        <br>
                        <ul class="section table-of-contents">
                            <li><strong>Les Méthodes</strong></li>
                            <li><a href="#AdjustScore">AdjustScore</a></li>
                            <li><a href="#Exception">Exception</a></li>
                            <li><a href="#GetDecoupleMode">GetDecoupleMode</a></li>
                            <li><a href="#GetDisplayHeight">GetDisplayHeight</a></li>
                            <li><a href="#GetDisplayWidth">GetDisplayWidth</a></li>
                            <li><a href="#GetElementProperty">GetElementProperty</a></li>
                            <li><a href="#GetInterfaceVisible">GetInterfaceVisible</a></li>
                            <li><a href="#GetMapView">GetMapView</a></li>
                            <li><a href="#GetMessageWindowVisible">GetMessageWindowVisible</a></li>
                            <li><a href="#GetMetricMode">GetMetricMode</a></li>
                            <li><a href="#GetObjectiveBarVisible">GetObjectiveBarVisible</a></li>
                            <li><a href="#GetScore">GetScore</a></li>
                            <li><a href="#GetTimeStamp">GetTimeStamp</a></li>
                            <li><a href="#GetWaybillWindowVisible">GetWaybillWindowVisible</a></li>
                            <li><a href="#HasUnseenMessages">HasUnseenMessages</a></li>
                            <li><a href="#HighlightButtonBarElement">HighlightButtonBarElement</a></li>
                            <li><a href="#Log">Log</a></li>
                            <li><a href="#LogCallStack">LogCallStack</a></li>
                            <li><a href="#LogResult">LogResult</a></li>
                            <li><a href="#Print">Print</a></li>
                            <li><a href="#ResetResults">ResetResults</a></li>
                            <li><a href="#SetAlert">SetAlert</a></li>
                            <li><a href="#SetDecoupleMode">SetDecoupleMode</a></li>
                            <li><a href="#SetElementProperty">SetElementProperty</a></li>
                            <li><a href="#SetHelperIconScale">SetHelperIconScale</a></li>
                            <li><a href="#SetMapView">SetMapView</a></li>
                            <li><a href="#SetMessageWindowVisible">SetMessageWindowVisible</a></li>
                            <li><a href="#SetMetricMode">SetMetricMode</a></li>
                            <li><a href="#SetObjective">SetObjective</a></li>
                            <li><a href="#SetObjectiveBarVisible">SetObjectiveBarVisible</a></li>
                            <li><a href="#SetObjectiveIcon">SetObjectiveIcon</a></li>
                            <li><a href="#SetResults">SetResults</a></li>
                            <li><a href="#SetScore">SetScore</a></li>
                            <li><a href="#SetTooltip">SetTooltip</a></li>
                            <li><a href="#SetWaybillWindowVisible">SetWaybillWindowVisible</a></li>
                            <li><a href="#ShowDriverButtonMenu">ShowDriverButtonMenu</a></li>
                            <li><a href="#ShowObjective">ShowObjective</a></li>
                            <li><a href="#ShowOnScreenHelp">ShowOnScreenHelp</a></li>
                            <li><a href="#WarnObsolete">WarnObsolete</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div id="interface-event" class="modal bottom-sheet">
        <div class="modal-content">
            <h4>Message d'interface</h4>
            <p>Ces messages fournissent des informations sur l'état des différentes parties de l'interface. Vous pouvez
                les utiliser pour déterminer quelles parties de l'écran sont actuellement visibles.</p>
            <br>
            <p>Il existe un ou deux cas particuliers:</p>
            <ul>
                <li><i class="material-icons">navigate_next</i> Interface-Event, Mouse-Enter: Ceci peut être utilisé
                    pour détecter un survol d'un objet basé sur un maillage et est diffusé à partir de l'objet concerné.
                </li>
                <li><i class="material-icons">navigate_next</i> Interface-Event, Mouse-Leave: Ceci est envoyé lorsque la
                    souris quitte un objet, le message est également diffusé mais doit être utilisé avec précaution car
                    l'objet concerné n'est pas spécifié par le message.
                </li>
                <li><i class="material-icons">navigate_next</i> Interface-Event, Set-Metric-Mode: Ce message peut être
                    utilisé pour déterminer si l'affichage utilise des unités impériales ou métriques.
                </li>
            </ul>
        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">OK</a>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        $(".modal").modal()
    </script>
@endsection