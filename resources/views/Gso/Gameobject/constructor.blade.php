@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>Constructors</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li><a href="">Gameobject</a></li>
                        <li class="active">Constructors</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <ul>
        <li><i class="material-icons">navigate_next</i> Une classe statique offrant la possibilité de créer des instances de certaines autres classes à partir d'un script.</li>
        <li><i class="material-icons">navigate_next</i> Trainzscript n'autorisant généralement pas la création de classes de jeu dans un script, ces méthodes autorisent des exceptions à cette règle.</li>
        <li><i class="material-icons">navigate_next</i> La mise en œuvre est susceptible de changer.</li>
    </ul>

    <div class="row">
        <div class="col s12 m9 l10">
            <div id="GetTrainzAsset" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetTrainzAsset</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native Asset GetTrainzAsset(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s12">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> L'atout global de Trainz.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode est principalement utilisée par les classes intégrées.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="GetTrainzStrings" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetTrainzStrings</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native StringTable GetTrainzStrings(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s12">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> La StringTable dans sont intégralité</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode est principalement utilisée par les classes intégrées.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewBrowser" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewBrowser</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native Browser NewBrowser(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Un nouvel objet de navigateur vide.</li>
                                </ul>
                                <!--<strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode est principalement utilisée par les classes intégrées.</li>
                                </ul>-->
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Browser browser = Constructors.NewBrowser();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewDriverCommand" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewDriverCommand</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native DriverCommand NewDriverCommand(Asset command)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s12">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>command</strong> = référence à une ressource <i>DriverCommand</i> à partir de laquelle la nouvelle instance de commande sera construite.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Une nouvelle instance DriverCommand à utiliser par le script.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode n’ajoute pas de nouvelle commande à la liste des commandes de pilote disponibles renvoyées par <a href="/world#GetDriverCommandList">World.GetDriverCommandList()</a>, mais crée simplement une instance de script à usage privé.</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewDriverCommands" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewDriverCommands</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native DriverCommands NewDriverCommands(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s12">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Une nouvelle liste DriverCommands vide.</li>
                                </ul>
                                <!--<strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode n’ajoute pas de nouvelle commande à la liste des commandes de pilote disponibles renvoyées par <a href="/world#GetDriverCommandList">World.GetDriverCommandList()</a>, mais crée simplement une instance de script à usage privé.</li>
                                </ul>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewKUIDList" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewKUIDList</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native KUIDList NewKUIDList(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Un nouvel objet KUIDList, vide et déverrouillé.</li>
                                </ul>
                                <!--<strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode est principalement utilisée par les classes intégrées.</li>
                                </ul>-->
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
KUIDList consist = Constructors.NewKUIDList();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewMenu" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewMenu</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native Menu NewMenu(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Un nouvel objet Menu vide.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le menu renvoyé est destiné uniquement à la configuration. Cette méthode ne crée aucune représentation visuelle.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Menu menu = Constructors.NewMenu();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewProductFilter" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewProductFilter</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native ProdutFilter NewProducFilter(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Un nouvel objet ProdutFilter, vide et déverrouillé.</li>
                                </ul>
                                <!--<strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le menu renvoyé est destiné uniquement à la configuration. Cette méthode ne crée aucune représentation visuelle.</li>
                                </ul>-->
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
ProductFilter filter = Constructors.NewProductFilter();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="NewSoup" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>NewSoup</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native Soup NewSoup(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Un nouvel objet Soup, vide et déverrouillé.</li>
                                </ul>
                                <!--<strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le menu renvoyé est destiné uniquement à la configuration. Cette méthode ne crée aucune représentation visuelle.</li>
                                </ul>-->
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Soup soup = Constructors.NewSoup();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#GetTrainzAsset">GetTrainzAsset</a></li>
                    <li><a href="#GetTrainzStrings">GetTrainzStrings</a></li>
                    <li><a href="#NewBrowser">NewBrowser</a></li>
                    <li><a href="#NewDriverCommand">NewDriverCommand</a></li>
                    <li><a href="#NewDriverCommands">NewDriverCommands</a></li>
                    <li><a href="#NewKUIDList">NewKUIDList</a></li>
                    <li><a href="#NewMenu">NewMenu</a></li>
                    <li><a href="#NewProductFilter">NewProductFilter</a></li>
                    <li><a href="#NewSoup">NewSoup</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection