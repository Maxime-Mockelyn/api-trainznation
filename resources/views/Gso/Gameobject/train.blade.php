@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>Constructors</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li><a href="">Gameobject</a></li>
                        <li class="active">Train</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <ul>
        <li><i class="material-icons">navigate_next</i> Un train est une collection de véhicules dans TRS.</li>
        <li><i class="material-icons">navigate_next</i> Ils agissent comme une seule entité et peuvent être contrôlés à l'aide de cette classe.</li>
        <li><i class="material-icons">navigate_next</i> Un train est placé sur une piste dans un script en utilisant <i>World.CreateTrain()</i> ou <i>TrainUtil.CreateTrainFromSoup()</i> .</li>
        <li><i class="material-icons">navigate_next</i> Trainz créera automatiquement un objet Train pour chaque composition apparaissant dans le jeu.</li>
        <li><i class="material-icons">navigate_next</i> Lorsqu'un train existant est découplé et divisé en deux ensembles distincts, un nouvel objet de train est créé.</li>
        <li><i class="material-icons">navigate_next</i> De même, lorsque deux trains couplent l'un des objets du train, celui-ci est détruit.</li>
    </ul>

    <ul class="tabs">
        <li class="tab col s3"><a href="#message">Messages</a></li>
        <li class="tab col s3"><a href="#constante">Constantes</a></li>
        <li class="tab col s3"><a href="#method">Methodes</a></li>
        <li class="tab col s3"><a href="#relative">Methodes Relatives</a></li>
    </ul>
    <div id="message">
        <div class="row">
            <div class="col s12 m9 l12">
                <h4 class="header">Messages</h4>
                <ul>
                    <li><i class="material-icons">navigate_next</i> Les messages envoyés aux objets Train sont listés ci-dessous:</li>
                </ul>

            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection