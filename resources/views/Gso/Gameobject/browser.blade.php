@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>Browser</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li><a href="">Gameobject</a></li>
                        <li class="active">Browser</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <ul>
        <li><i class="material-icons">navigate_next</i> Cette classe fournit une interface avec Trainz <a href="/referenciel/miniBrowser">MiniBrowser</a> permettant au programmeur de fournir une interface HTML simple.</li>
        <li><i class="material-icons">navigate_next</i> Le navigateur prend en charge un petit sous-ensemble de code HTML.</li>
        <li><i class="material-icons">navigate_next</i> Les navigateurs multiples sont pris en charge.</li>
        <li><i class="material-icons">navigate_next</i> Pour créer un nouveau navigateur, utilisez <a href="/gso/game_object/constructor#NewBrowser">Constructors.NewBrowser()</a> .</li>
        <li><i class="material-icons">navigate_next</i> Pour remplir avec du code HTML, utilisez <a href="#LoadHTMLString">LoadHTMLString</a> ou <a href="#LoadHTMLFile">LoadHTMLFile</a>.</li>
        <li><i class="material-icons">navigate_next</i> Reportez-vous à la page <a href="/referenciel/miniBrowser">MiniBrowser</a> pour obtenir des détails sur les balises prises en charge.</li>
    </ul>

    <div class="row">
        <div class="col s12 m9 l10">
            <div id="message" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Message</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Les messages envoyés depuis et vers les objets du navigateur sont énumérés ci-dessous:</li>
                        </ul>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Majeur</th>
                                    <th>Mineur</th>
                                    <th>Source</th>
                                    <th>Destination</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Browser-URL</td>
                                <td>URL</td>
                                <td>Browser</td>
                                <td>Broadcast</td>
                            </tr>
                            <tr>
                                <td>Browser-Closed</td>
                                <td>null</td>
                                <td>Browser</td>
                                <td>Broadcast</td>
                            </tr>
                            </tbody>
                        </table>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> L' URL dans un message <i>Browser-URL</i> utilise une balise de lien hypertexte spéciale sous la forme <i>live://link</i> .</li>
                            <li><i class="material-icons">navigate_next</i> Si votre code HTML contient le lien <i><'a href=live://anthem> Jouer l'hymne national <'/a></i> et que l'utilisateur clique sur le lien, le message Browser-URL prendra alors la forme Browser-URL, en <i>live://anthem</i> et votre script peuvent intercepter le message et y répondre en conséquence.</li>
                            <li><i class="material-icons">navigate_next</i> Étant donné que les messages des objets du navigateur sont toujours diffusés, il est essentiel de vous assurer que le message provient de la fenêtre attendue. Vous pouvez le faire en inspectant la source du message.</li>
                            <li><i class="material-icons">navigate_next</i> Lorsque l'utilisateur ferme la fenêtre du navigateur en cliquant sur le bouton de fermeture ou en appuyant sur la touche Échap, un message contenant un message <i>Browser-Closed</i> est diffusé. L'objet Navigateur devrait normalement être réinitialisé à null en réponse, de sorte que la fenêtre du navigateur disparaisse.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="constants" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Constantes</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Les constantes définies dans cette classe sont:</li>
                        </ul>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Styles de la fenêtre du navigateur</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>public define int STYLE_NO_FRAME = 0</td>
                                <td>Navigateur sans cadre (pas dans une fenêtre bordée).</td>
                            </tr>
                            <tr>
                                <td>public define int STYLE_DEFAULT = 1</td>
                                <td>Style de navigateur par défaut. Ceci est défini par Trainz lui-même et signifie généralement que la fenêtre du navigateur aura une bordure encadrée.</td>
                            </tr>
                            <tr>
                                <td>public define int STYLE_TOOLTIP = 2</td>
                                <td>Navigateur sans cadre ni facilité d’interaction avec l’utilisateur.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div id="BringToFront" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>BringToFront</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void BringToFront(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Amène la fenêtre devant tous les autres navigateurs.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.BringToFront();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="GetAsset" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetAsset</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native Asset GetAsset(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Actif à partir duquel ce navigateur charge des données et des ressources HTML.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> L'actif renvoyé sera l'actif utilisé lors du dernier appel à <a href="#LoadHTMLFile">LoadHTMLFile</a> ou <a href="#LoadHTMLString">LoadHTMLString</a> .</li>
                                    <li><i class="material-icons">navigate_next</i> S'il n'y a pas eu d'appel de ce type, la valeur renvoyée sera null.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Asset asset = browser.GetAsset();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="GetWindowVisible" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowVisible</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native bool GetWindowVisible(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>Booléen</strong>: True pour indiquer que le navigateur est actuellement visible à l'écran, false si le navigateur est masqué.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
hidden = !browser.GetWindowVisible();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="LoadHTMLFile" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>LoadHTMLFile</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void LoadHTMLFile(Asset asset, string filename)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>asset</strong> = référence à l'actif où se trouvent le fichier HTML et d'autres ressources.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>filename</strong> = fichier HTML à charger, peut inclure un spécificateur de chemin relatif.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> L'actif peut être de n'importe quel type, à condition que les fichiers appropriés soient inclus dans la structure de dossiers.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.LoadHTMLFile(resourceAsset,"/html/File-01.html");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="LoadHTMLString" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>LoadHTMLString</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void LoadHTMLString(Asset asset, string htmlData)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>asset</strong> = référence à l'actif où se trouvent les ressources requises.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>html_data</strong> = chaîne contenant le code HTML à charger dans le navigateur.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> L'actif peut être de n'importe quel type, à condition que les fichiers appropriés soient inclus dans la structure de dossiers.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.LoadHTMLString(resourceAsset,htmlData);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ResetScrollBar" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>ResetScrollBar</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void ResetScrollBar(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Déplace la poignée de la barre de défilement et le contenu HTML vers le haut.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.ResetScrollBar();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetCloseEnabled" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetCloseEnabled</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetCloseEnabled(bool enabled)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>enabled</strong> = définir sur false pour empêcher l'utilisateur de fermer le navigateur, définir sur true pour activer la fermeture.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetCloseEnabled(false);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetParam" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetParam</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetParam(int index, string value)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>index</strong> = index du paramètre à définir.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>value</strong> = chaîne valeur à affecter au paramètre spécifié.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le code HTML peut contenir des paramètres modifiables désignés par les caractères réservés $# , où # est un nombre.</li>
                                    <li><i class="material-icons">navigate_next</i> SetParam() demande à l'analyseur de remplacer le texte du paramètre function par le paramètre fictif lorsque le navigateur est chargé.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetParam(2,"Birmingham New Street");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetScrollEnabled" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetScrollEnabled</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetScrollEnabled(bool enabled)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>enabled</strong> = définir sur false pour empêcher l'utilisateur de faire défiler le navigateur, définissez sur true pour l'activer.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetScrollEnabled(false);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetWindowStyle" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowStyle</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowStyle(int style)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>style</strong> = Une des constantes de <a href="#constants">style de la fenêtre du navigateur</a> définissant le type de navigateur à fournir.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetWindowStyle(Browser.STYLE_DEFAULT);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetWindowVisible" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowVisible</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowVisible(bool visible)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>visible</strong> = true pour afficher la fenêtre du navigateur, false pour la masquer.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
SetWindowVisible(!GetWindowVisible());
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <h4 class="header"><u>Taille & Position du navigateur</u></h4>
            <div id="GetWindowLeft" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowLeft</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetWindowLeft(void)</strong>
                        </p>
                        <br>
                        <p>Récupère la coordonnée en pixels du bord gauche.</p>
                    </div>
                </div>
            </div>
            <div id="GetWindowTop" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowTop</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetWindowTop(void)</strong>
                        </p>
                        <br>
                        <p>Récupère la coordonnée en pixels du bord supérieur.</p>
                    </div>
                </div>
            </div>
            <div id="GetWindowRight" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowRight</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetWindowRight(void)</strong>
                        </p>
                        <br>
                        <p>Récupère la coordonnée en pixels du bord droit.</p>
                    </div>
                </div>
            </div>
            <div id="GetWindowBottom" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowBottom</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetWindowBottom (void)</strong>
                        </p>
                        <br>
                        <p>Récupère la coordonnée en pixels du bord inférieur.</p>
                    </div>
                </div>
            </div>
            <div id="GetWindowWidth" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowWidth</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetWindowWidth(void)</strong>
                        </p>
                        <br>
                        <p>Récupère la largeur de la fenêtre en pixels.</p>
                    </div>
                </div>
            </div>
            <div id="GetWindowHeight" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetWindowHeight</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetWindowHeight(void)</strong>
                        </p>
                        <br>
                        <p>Récupère la hauteur de la fenêtre en pixels.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowLeft" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowLeft</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowLeft(int left)</strong>
                        </p>
                        <br>
                        <p>Définit la coordonnée en pixels du bord gauche.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowTop" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowTop</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowTop(int top)</strong>
                        </p>
                        <br>
                        <p>Définit la coordonnée en pixels du bord supérieur.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowRight" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowRight</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowRight(int right)</strong>
                        </p>
                        <br>
                        <p>Définit la coordonnée en pixels du bord droit.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowBottom" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowBottom</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowBottom(int bottom)</strong>
                        </p>
                        <br>
                        <p>Définit la coordonnée en pixels du bord inférieur.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowRect" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowRect</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowRect(int left, int, top, int right, int bottom)</strong>
                        </p>
                        <br>
                        <p>Définit les coordonnées en pixels des quatre bords en un seul appel.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowWidth" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowWidth</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowWidth(int width)</strong>
                        </p>
                        <br>
                        <p>Définit la largeur de la fenêtre en pixels.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowHeight" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowHeight</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowHeight(int height)</strong>
                        </p>
                        <br>
                        <p>Définit la hauteur de la fenêtre en pixels.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowSize" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowSize</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowSize(int width, int height)</strong>
                        </p>
                        <br>
                        <p>Définit la hauteur et la largeur de la fenêtre en pixels.</p>
                    </div>
                </div>
            </div>
            <div id="SetWindowPosition" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowPosition</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowPosition(int left, int top)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>left</strong> = coordonnée en pixels du bord gauche.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>top</strong> = coordonnée en pixels du bord supérieur.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>right</strong> = coordonnée en pixels du bord droit.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>bottom</strong> = coordonnée en pixels du bord inférieur.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>width</strong> = largeur de la fenêtre en pixels.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>height</strong> = hauteur de la fenêtre en pixels</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Déplace la fenêtre aux coordonnées spécifiées sans affecter sa taille.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetWindowPosition(0,0,200,300);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetWindowGrow" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetWindowGrow</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetWindowGrow(int minWidth, int minHeight, int maxWidth, int maxHeight)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>minWidth,minHeight</strong> = largeur et hauteur minimales en pixels.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>maxWidth,maxHeight</strong> = Largeur et hauteur maximales en pixels.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Définit une hauteur et une largeur maximales et minimales auxquelles la fenêtre du navigateur sera contrainte.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetWindowGrow(100,100,300,300);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <h4 class="header"><u>Element HTML Trainz Object</u></h4>
            <ul>
                <li><i class="material-icons">navigate_next</i> Les balises <i>trainz-object</i> définissent des objets de navigateur spéciaux avec lesquels l'utilisateur peut interagir.</li>
                <li><i class="material-icons">navigate_next</i> Celles-ci incluent un cadran graphique, une zone de saisie de texte ou un cadre de navigateur et une gamme de paramètres.</li>
            </ul>
            <table class="table bordered">
                <thead>
                <tr>
                    <th>Propriété</th>
                    <th>Valeur</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>style</td>
                    <td>string</td>
                    <td>
                        Le Type de controle:
                        <ul>
                            <li>- dial</li>
                            <li>- text-line</li>
                            <li>- browser</li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <td>id</td>
                    <td>string</td>
                    <td>Nom de cet objet dans le navigateur.</td>
                </tr>
                <tr>
                    <td>width</td>
                    <td>integer</td>
                    <td>Largeur en pixels de la zone dans laquelle afficher l'objet.</td>
                </tr>
                <tr>
                    <td>height</td>
                    <td>integer</td>
                    <td>Hauteur de pixel de la zone d'affichage de l'objet.</td>
                </tr>
                <tr>
                    <td>texture</td>
                    <td>filename</td>
                    <td>Nom du fichier de texture pour l'objet.</td>
                </tr>
                <tr>
                    <td>min</td>
                    <td>float</td>
                    <td>Valeur maximale du contrôle.</td>
                </tr>
                <tr>
                    <td>max</td>
                    <td>float</td>
                    <td>Valeur maximale du contrôle.</td>
                </tr>
                <tr>
                    <td>valmin</td>
                    <td>float</td>
                    <td>Valeur minimale possible de l'objet.</td>
                </tr>
                <tr>
                    <td>valmax</td>
                    <td>float</td>
                    <td>Valeur maximale possible de l'objet.</td>
                </tr>
                <tr>
                    <td>step</td>
                    <td>integer</td>
                    <td>Les étapes de ce contrôle.</td>
                </tr>
                <tr>
                    <td>clickstep</td>
                    <td>integer</td>
                    <td>Nombre d'étapes par clic</td>
                </tr>
                <tr>
                    <td>value</td>
                    <td>float</td>
                    <td>valeur par défaut initiale de l'objet.</td>
                </tr>
                </tbody>
            </table>
            <p>La Définition HTML pour un cadran serait:</p>
            <pre><code class="language-html" data-language="html">
&lt;a href="live://dial/dcc" tooltip="Speed control">
   &lt;trainz-object style=dial
                  width=100
                  height=100
                  id="dcc"
                  texture="newdriver/dcc/dcc_controller.tga"
                  min=0.1
                  max=0.9
                  valmin=-1.0
                  valmax=1.0
                  step=0
                  clickstep=1
                  value=0.5>
   &lt;/trainz-object>
&lt;/a>
                                </code></pre>
            <ul>
                <li><i class="material-icons">navigate_next</i> Ce code HTML implémente l'émission d'un message Browser-URL chaque fois que la numérotation est ajustée par l'utilisateur.</li>
                <li><i class="material-icons">navigate_next</i> Lorsque le script d'actif reçoit le message, il peut interroger le contrôle pour déterminer ce qu'il doit faire.</li>
                <li><i class="material-icons">navigate_next</i> De même, si le script met à jour les propriétés du cadran, les graphiques HTML refléteront le changement.</li>
            </ul>
            <div id="GetElementProperty" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetElementProperty</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native string GetElementProperty(string elementId, string propertyId)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>elementId</strong> = Nom de l'élément HTML à interroger.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>propertyId</strong> = Nom de la propriété trainz-object à récupérer.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Valeur actuelle de la propriété spécifiée.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Toutes les valeurs sont renvoyées au format chaîne et doivent être converties ou converties selon le cas.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
float speed = (float)browser.GetElementProperty("dcc","value"); //Retourne la valeur de la position du 'dial'
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SetElementObjectProperty" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetElementObjectProperty</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetElementProperty(string elementId, string propertyId, object value)</strong>
                        </p>
                        <p>Définit la propriété de l'élément d'objet nommé dans ce navigateur sur l'objet donné.</p><br>
                        <p>Cette méthode est utilisée pour affecter une valeur d'objet à la propriété contenue dans un élément qui se trouve dans la page HTML avec laquelle ce navigateur est actuellement chargé.</p><br>
                        <p>Un élément dans une page HTML est généralement quelque chose comme une zone de saisie de texte, un cadran ou un bouton défini par une balise &lt;trainz-object>. Chaque élément possède plusieurs propriétés et cette méthode permet de définir ces éléments de propriété par nom.</p><br>
                        <strong>Parametres:</strong>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <strong>elementId</strong> = Nom de l'élément pour définir la propriété</li>
                            <li><i class="material-icons">navigate_next</i> <strong>propertyId</strong> = Propriété dans l'élément à définir. Les propriétés supportées par la balise &lt;trainz-object> peuvent être utilisées.</li>
                            <li><i class="material-icons">navigate_next</i> <strong>value</strong> = est la valeur à laquelle la propriété sera attribuée.</li>
                        </ul>
                        <p>Certaines propriétés d'élément font référence à des objets, telles que la propriété "driver-orders" des éléments "driver-order-bar". Avec cette méthode, vous pouvez par exemple transmettre une référence d'objet DriverCommands à l'élément driver-order-bar.
                            De plus, un navigateur intégré peut recevoir une référence d'actif pour sa propriété "actif" via cette méthode. Cela modifiera l'actif d'où les images sont chargées.</p>
                    </div>
                </div>
            </div>
            <div id="SetElementProperty" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetElementProperty</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetElementProperty(string elementId, string propertyId, string value)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>elementId</strong> = Nom de l'élément HTML à mettre à jour.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>propertyId</strong> = Nom de la propriété trainz-object à mettre à jour.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>value</strong> = Nouvelle valeur à définir.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Valeur actuelle de la propriété spécifiée.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Toutes les valeurs doivent être converties ou converties au format chaîne pour pouvoir être utilisées dans cette méthode.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetElementProperty("dcc","value",(string)speed); // Met à jour la valeur de la position de 'dial'
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <h4 class="header">Element HTML Trainz Text</h4>
            <ul>
                <li><i class="material-icons">navigate_next</i> Les balises &lt;trainz-text> définissent des chaînes de texte pouvant être mises à jour sans qu'il soit nécessaire de recharger la page.</li>
                <li><i class="material-icons">navigate_next</i> Ceux-ci se présentent sous la forme &lt;trainz-text id="speed" text="0mph"> &lt;/trainz-text></li>
                <li><i class="material-icons">navigate_next</i> La balise textuelle spécifie le texte par défaut affiché pour l'élément lors de la création initiale de la page.</li>
                <li><i class="material-icons">navigate_next</i> Une fois la page chargée, un script peut utiliser <i>SetTrainzText()</i> pour fournir une mise à jour dynamique.</li>
            </ul>
            <div id="SetTrainzText" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SetTrainzText</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SetTrainzText(string id, string text)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>id</strong> = L'id de l' élément <i>trainz-text</i> à mettre à jour.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>text</strong> = La valeur de chaîne à fournir.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
browser.SetTrainzText("speed","90mph");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="exemple" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Exemple</h4>
                        <p>Ce code illustre les bases de la gestion d'un navigateur.</p>
                        <pre><code class="language-javascript" data-language="javascript">
class TestBrowser isclass MapObject {

// Une variable globale destinée à contenir l'objet 'navigateur (browser)'
   Browser browser;

   void OpenBrowser(void) {
   // si le navigateur n'existe pas, il doit être créé
      if (!browser) browser = Constructors.NewBrowser();
   // définit la taille et le style et charge le code HTML, dans ce cas, à partir d'un fichier de la ressource appelante
      browser.SetWindowPosition(0,0);
      browser.SetWindowStyle(browser.STYLE_DEFAULT);
      browser.SetWindowSize(200,500);
      browser.LoadHTMLFile(GetAsset(),"HTMLSource.html");
   }

   void CloseBrowser(Message msg) {
   // si le navigateur a été fermé, nous devons détruire la référence
      if (msg.src == browser) browser = null;
   }

   void BrowserLink(Message msg) {
   // assurez-vous que nous répondons au bon objet de navigateur
      if (msg.src != browser) return;
   // effectue la réponse appropriée à la chaîne de caractère 'link'
      string link = msg.minor;
      if (link == "live://anthem") {
         World.Play2DSound(GetAsset(),"God Save the Queen.wav");
      }
      else if (link == "live://travesty") {
         World.Play2DSound(GetAsset(),"Advance Australia Fair.wav");
      }
   }

   public void Init(void) {
      inherited();
   // crée des gestionnaires pour les messages du navigateur
      AddHandler(me,"Browser-URL","","BrowserLink");
      AddHandler(me,"Browser-Closed","","CloseBrowser");
   // ouvre la fenêtre du navigateur
      OpenBrowser();
   }

};
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="annexe" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Annexe</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object/constructors#NewBrowser">Constructors.NewBrowser()</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#message">Message</a></li>
                    <li><a href="#constants">Constantes</a></li>
                    <br>
                    <li><strong>Méthodes Générales</strong></li>
                    <li><a href="#BringToFront">BringToFront</a></li>
                    <li><a href="#GetAsset">GetAsset</a></li>
                    <li><a href="#GetWindowVisible">GetWindowVisible</a></li>
                    <li><a href="#LoadHTMLFile">LoadHTMLFile</a></li>
                    <li><a href="#LoadHTMLString">LoadHTMLString</a></li>
                    <li><a href="#ResetScrollBar">ResetScrollBar</a></li>
                    <li><a href="#SetCloseEnabled">SetCloseEnabled</a></li>
                    <li><a href="#SetParam">SetParam</a></li>
                    <li><a href="#SetScrollEnabled">SetScrollEnabled</a></li>
                    <li><a href="#SetWindowStyle">SetWindowStyle</a></li>
                    <li><a href="#SetWindowVisible">SetWindowVisible</a></li>
                    <br>
                    <li><strong>Taille & Position du navigateur</strong></li>
                    <li><a href="#GetWindowWidth">GetWindowWidth</a></li>
                    <li><a href="#GetWindowHeight">GetWindowHeight</a></li>
                    <li><a href="#SetWindowRect">SetWindowRect</a></li>
                    <li><a href="#SetWindowSize">SetWindowSize</a></li>
                    <li><a href="#SetWindowPosition">SetWindowPosition</a></li>
                    <li><a href="#SetWindowGrow">SetWindowGrow</a></li>
                    <br>
                    <li><strong>Element HTML Trainz Object</strong></li>
                    <li><a href="#GetElementProperty">GetElementProperty</a></li>
                    <li><a href="#SetElementObjectProperty">SetElementObjectProperty</a></li>
                    <li><a href="#SetElementProperty">SetElementProperty</a></li>
                    <br>
                    <li><strong>Element HTML Trainz Text</strong></li>
                    <li><a href="#SetTrainzText">SetTrainzText</a></li>
                    <br>
                    <li><a href="#exemple">Exemple</a></li>
                    <li><a href="#annexe">Annexe</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection