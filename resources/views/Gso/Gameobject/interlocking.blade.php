@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>InterlockingTowerPath</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li><a href="">Gameobject</a></li>
                        <li class="active">InterlockingTowerPath</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <p>
        La classe InterlockingTowerPath est la représentation de script d'un "chemin" de tour de verrouillage individuel.
        Dans ce contexte, un chemin représente la voie qu'un train peut emprunter à travers une zone contrôlée par une <i>"Tour de controle"</i>.
        Il est défini par un certain nombre d'objets en bordure de voie (signaux, jonctions et croisements) et par l'état dans lequel ces objets doivent se trouver pour permettre le passage du train dans cette voie.
        Chaque chemin a un signal d’entrée et de sortie. Lorsqu'un train s'approche d'un signal d'entrée, la tour de verrouillage (et les bibliothèques associées) tentera de lui attribuer un chemin spécifique, afin qu'il puisse transmettre le signal.
    </p>
    <ul class="tabs">
        <li class="tab col s3"><a href="#function">Fonctions Communes</a></li>
        <li class="tab col s3"><a href="#annexes">Classes Annexes</a></li>
    </ul>

    <div id="function">
        <div class="row">
            <div class="col s12 m9 l10">
                <h4 class="header">Fonctions Communes</h4>
                <p>Vous trouverez ci-dessous une liste de certaines des fonctions communes de base de la classe de script InterlockingTower. Cette liste n’est pas exhaustive, elle se contente de donner un aperçu.</p>
                <div id="Init" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Init</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public mandatory void Init(InterlockingTower tower, SecurityToken token)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Initialise le chemin avec les paramètres définis, si possible. Lève une exception et renvoie false si la demande est invalide.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>tower = </strong>La tour actuel.</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>toker = </strong>Un jeton de sécurité de la propriété InterlockingTower, disposant des droits suffisants pour activer le chemin</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Aucune</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SetPathName" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>SetPathName</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void SetPathName(string pathName)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Définit le nom du chemin d'identification. Cela devrait être unique dans la tour actuel.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>pathName = </strong>Le nouveau nom d'identification du chemin de la tour.</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Aucune</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GetPathName" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>GetPathName</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public string GetPathName()</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Renvoie le nom du chemin d'identification pour ce chemin de tour.</p>
                                    <!--<strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>pathName = </strong>Le nouveau nom d'identification du chemin de la tour.</li>
                                    </ul>-->
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Aucune</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GetLocalisedPathName" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>GetLocalisedPathName</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public string GetLocalisedPathName()</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Renvoie le nom du chemin localisé pour ce chemin tour. Les noms localisés sont stockés dans la table des chaînes pour la route ou la session. Il n'y a pas d'interface pour définir ceci et cela doit être fait par le créateur de la session.</p>
                                    <!--<strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>pathName = </strong>Le nouveau nom d'identification du chemin de la tour.</li>
                                    </ul>-->
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Aucune</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="IsObjectInPathDefinition" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>IsObjectInPathDefinition</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public bool IsObjectInPathDefinition(MapObject obj, int childIndex, bool bIncludeExternalRequirements)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Retourne si un objet donné fait partie de la définition de ce chemin.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>obj = </strong>L'objet de chemin (jonction, signal, etc.) à rechercher.</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>childIndex = </strong>Index d'un objet enfant, ou -1 pour l'objet lui-même.</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>bIncludeExternalRequirements = </strong>Indique s'il faut également vérifier la liste des exigences externes.</li>
                                    </ul>
                                    <strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si l'objet a été trouvé, false sinon</li>
                                    </ul>
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="IsObjectOnPath" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>IsObjectOnPath</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public bool IsObjectOnPath(MapObject obj)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Effectue une recherche de piste et indique si l'objet transmis est quelque part sur le chemin. Évitez de l'appeler aussi souvent que possible car cela peut être assez intensif pour le processus.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>obj = </strong>L'objet de carte pour rechercher le chemin.</li>
                                    </ul>
                                    <strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si l'objet a été trouvé sur le chemin, false sinon</li>
                                    </ul>
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="IsObjectOnOrBeyondPath" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>IsObjectOnOrBeyondPath</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public bool IsObjectOnOrBeyondPath(MapObject obj)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Renvoie si un objet donné se trouve sur ce chemin ou se trouve sur la piste au-delà du signal de sortie. Notez que la recherche au-delà du chemin suivra les directions de jonction actuellement définies.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>obj = </strong>L'objet de carte pour rechercher le chemin.</li>
                                    </ul>
                                    <strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - Si l'objet a été trouvé sur le chemin ou dans la plage de recherche au-delà du chemin.</li>
                                    </ul>
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="GetPathClearMethod" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>GetPathClearMethod</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public int GetPathClearMethod()</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Renvoie la méthode d'effacement de chemin actuellement définie, qui est l'une des définitions ITP_CLEAR_ *.</p>
                                    <!--<strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>obj = </strong>L'objet de carte pour rechercher le chemin.</li>
                                    </ul>-->
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - Si l'objet a été trouvé sur le chemin ou dans la plage de recherche au-delà du chemin.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SetPathClearMethod" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>SetPathClearMethod</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void SetPathClearMethod(int clearMethod)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Modifie la méthode d'effacement de chemin pour ce chemin.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>clearMethod = </strong>La nouvelle méthode d'effacement à définir, l'une des définitions ITP_CLEAR_ *.</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - Si l'objet a été trouvé sur le chemin ou dans la plage de recherche au-delà du chemin.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="CanTransitionPathToState" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>CanTransitionPathToState</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>bool CanTransitionPathToState(int state)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Retourne si ce chemin est dans un état où il est possible de commencer à passer à l'état passé. Lors de la création d'un script de tour personnalisé, il s'agit d'un endroit idéal pour effectuer des vérifications supplémentaires des changements d'état du chemin.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>state = </strong>Etat dans lequel nous souhaitons faire la transition.</li>
                                    </ul>
                                    <strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si le chemin est capable de passer à l'état demandé, false sinon.</li>
                                    </ul>
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="ActivatePath" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>ActivatePath</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void ActivatePath(SecurityToken token)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Commence à activer le chemin, si possible. Publiera un message sur la tour propriétaire de type "InterlockingTowerPath", "Actif" lorsque le chemin est défini.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>token = </strong>Un jeton de sécurité de la tour propriétaire avec les droits "control-path".</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si le chemin est capable de passer à l'état demandé, false sinon.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="CancelPath" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>CancelPath</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void CancelPath(SecurityToken token)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Commence à annuler le chemin, si possible. Publiera un message au propriétaire de type "InterlockingTowerPath", "Annulé" lorsque le chemin est annulé. Cela ramènera tous les objets contrôlés à leur état d'origine, le cas échéant, en fonction de m_clearState.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>token = </strong>Un jeton de sécurité de la tour propriétaire avec les droits "control-path".</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si le chemin est capable de passer à l'état demandé, false sinon.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="SetPanicState" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>SetPanicState</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void SetPanicState(SecurityToken token)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Supprime instantanément tous les signaux contrôlés le long de ce chemin. Ne change pas d'état de jonction ou de croisement. Utilisé pour simuler une situation d'urgence quelconque. Pour sortir d'un état de panique, appelez ActivatePath ou CancelPath.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>token = </strong>Un jeton de sécurité de la tour propriétaire avec les droits "control-path".</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si le chemin est capable de passer à l'état demandé, false sinon.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="AddTrain" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>AddTrain</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void AddTrain(SecurityToken token, Train train)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Ajoute le train passé sur ce chemin. N'effectue aucune validation sur cette opération et garantira toutes les demandes provenant d'une source valide.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>token = </strong>Un jeton de sécurité de la tour propriétaire avec les droits "control-path".</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>train = </strong>Le train à passé sur le chemin.</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si le chemin est capable de passer à l'état demandé, false sinon.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="RemoveTrain" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>RemoveTrain</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void RemoveTrain(SecurityToken token, Train train)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Supprimé le train passé de ce chemin. N'effectue aucune validation sur l'opération et garantira toutes les demandes provenant d'une source valide.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>token = </strong>Un jeton de sécurité de la tour propriétaire avec les droits "control-path".</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>train = </strong>Le train à supprimé sur le chemin.</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - true si le chemin est capable de passer à l'état demandé, false sinon.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="IsOccupiedByTrain" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>IsOccupiedByTrain</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public bool IsOccupiedByTrain(Train train, bool bRequireHasClearedEntrySignal)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Retourne si le chemin est connu pour être occupé par un train spécifique. N'effectue aucune recherche de piste et dépend du fait que la tour propriétaire appelle correctement AddTrain / RemoveTrain.</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>train = </strong>Le train à vérifier.</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>bRequireHasClearedEntrySignal = </strong>Si la valeur est true, n'indiquez cette valeur que si le train a effacé le signal d'entrée et occupe le passage; sinon, il suffit que la tour ait simplement ajouté le train.</li>
                                    </ul>
                                    <strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - Si le train a été ajouté au chemin et, éventuellement, a passé le signal d'entrée.</li>
                                    </ul>
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div id="EnablePathVisualisation" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>EnablePathVisualisation</h4>
                            <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                                <strong>public void EnablePathVisualisation(bool enable, bool bShouldIncludeAllTCBs)</strong></p><br>
                            <div class="row">
                                <div class="col s12">
                                    <strong>Description</strong>
                                    <p>Active / désactive la visualisation de ce chemin dans le monde du jeu, à l’aide de TrackPathDisplay .</p>
                                    <strong>Parametres:</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <strong>enable = </strong>true pour activer la visualisation du chemin, false pour le désactiver</li>
                                        <li><i class="material-icons">navigate_next</i> <strong>bShouldIncludeAllTCBs = </strong>Indique s'il faut également ajouter des visualisations de chemin pour chaque TrackCircuitBlock occupé .</li>
                                    </ul>
                                    <!--<strong>Valeur retournée</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> bool - Si le train a été ajouté au chemin et, éventuellement, a passé le signal d'entrée.</li>
                                    </ul>-->
                                    <!--<strong>Remarque</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                        <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                    </ul>-->
                                    <!--<strong>Voir également</strong>
                                    <ul>
                                        <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                    </ul>-->
                                </div>
                                <!--<div class="col s5">
                                    <strong>Syntax</strong>
                                    <pre><code class="language-javascript" data-language="javascript">
AdjustScore(-5);
                                </code></pre>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col hide-on-small-only m3 l2">
                <div class="toc-wrapper">
                    <br>
                    <br>
                    <br>
                    <ul class="section table-of-contents">
                        <li><strong>Fonctions Communes</strong></li>
                        <li><a href="#Init">Init</a></li>
                        <li><a href="#SetPathName">SetPathName</a></li>
                        <li><a href="#GetPathName">GetPathName</a></li>
                        <li><a href="#GetLocalisedPathName">GetLocalisedPathName</a></li>
                        <li><a href="#IsObjectInPathDefinition">IsObjectInPathDefinition</a></li>
                        <li><a href="#IsObjectOnPath">IsObjectOnPath</a></li>
                        <li><a href="#IsObjectOnOrBeyondPath">IsObjectOnOrBeyondPath</a></li>
                        <li><a href="#GetPathClearMethod">GetPathClearMethod</a></li>
                        <li><a href="#SetPathClearMethod">SetPathClearMethod</a></li>
                        <li><a href="#CanTransitionPathToState">CanTransitionPathToState</a></li>
                        <li><a href="#ActivatePath">ActivatePath</a></li>
                        <li><a href="#CancelPath">CancelPath</a></li>
                        <li><a href="#SetPanicState">SetPanicState</a></li>
                        <li><a href="#AddTrain">AddTrain</a></li>
                        <li><a href="#RemoveTrain">RemoveTrain</a></li>
                        <li><a href="#IsOccupiedByTrain">IsOccupiedByTrain</a></li>
                        <li><a href="#EnablePathVisualisation">EnablePathVisualisation</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="annexes">
        <div class="card">
            <div class="card-content">
                <h4 class="header">Classes Annexes</h4>
                <ul>
                    <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object/trainz_game_object/mesh_object/map_object/interlocking_tower">InterlockingTower</a></li>
                    <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object/trainz_game_object/mesh_object/map_object/trackside/signal">Signal</a></li>
                    <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object/trainz_game_object/mesh_object/map_object/trackside/junction">Junction</a></li>
                    <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object/trainz_game_object/mesh_object/map_object/scenery_with_track/crossing">Crossing</a></li>
                </ul>
            </div>
        </div>
    </div>

</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection