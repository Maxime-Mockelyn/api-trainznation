@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>GameObject</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li class="active">GameObject</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <ul>
        <li><i class="material-icons">navigate_next</i> GameObject est la classe de base de tout objet nécessitant la prise en charge de la messagerie et des threads.</li>
        <li><i class="material-icons">navigate_next</i> Les GameObjects sont placés dans la table d'objets du jeu, appelée routeur.</li>
        <li><i class="material-icons">navigate_next</i> Vous ne pouvez pas créer un GameObject en utilisant <i>new()</i> à partir d'un script, tous les objets du jeu sont créés par le jeu.</li>
        <li><i class="material-icons">navigate_next</i> Chaque objet de jeu a un identifiant unique (voir <a href="#GetId">GetId()</a>) et beaucoup ont également un nom.</li>
        <li><i class="material-icons">navigate_next</i> Les instances GameObject sont souvent désignées sous le nom de <i>nœud</i> car elles représentent un nœud de message dans le routeur.</li>
    </ul>

    <div class="row">
        <div class="col s12 m9 l10">
            <div id="AddHandler" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>AddHandler</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white"><strong>public native void AddHandler(GameObject target, string major, string minor, string Handler)</strong></p><br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>target = </strong> Objet auquel ajouter le gestionnaire de messages. Ceci est généralement <i>me</i>, mais peut être n'importe quel <i>GameObject</i> .</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>major = </strong> message majeur à écouter.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>minor = </strong> Message mineur à écouter. Si ce paramètre est null ou une chaîne vide, tous les messages correspondant au paramètre majeur seront envoyés au gestionnaire.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>Handler = </strong> Nom de la méthode du gestionnaire de messages à appeler. Cela doit avoir un prototype sous la forme <i>void MessageHandler(Message msg)</i></li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Ajoute un gestionnaire de messages à la classe actuelle, de sorte qu'elle soit appelée à chaque fois que l'objet cible GameObject reçoit un message correspondant.</li>
                                    <li><i class="material-icons">navigate_next</i> Les gestionnaires de messages appelés par ce moyen ne sont pas threadés et seront exécutés immédiatement.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
AddHandler(me,"Object","Enter","ObjectHandler");
AddHandler(me,"Object","","ObjectHandler");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="ClearMessages" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>ClearMessages</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white"><strong>public native void ClearMessages(string major, string minor)</strong></p><br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>major = </strong> Objet auquel ajouter le gestionnaire de messages. Ceci est généralement <i>me</i>, mais peut être n'importe quel <i>GameObject</i> .</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>minor = </strong> message majeur à écouter.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Efface tous les messages en attente des types majeur et mineur spécifiés qui ont été envoyés via des appels à PostMessage () et n’ont pas encore été traités.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
ClearMessages("Object","Enter");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Exception" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Exception</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void Exception(string reason)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>reason = </strong> Texte explicatif à ajouter à la boîte de message.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Déclenche une exception sur le thread actuel et affiche le contenu du paramètre raison suivi d'un vidage de pile.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Exception("Fatal Error");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="GetId" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetId</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native int GetId(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> L'ID GameObject, une valeur entière utilisée par le routeur pour identifier l'objet actuel.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> L'ID est unique à la session en cours.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
int ID = GetId();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="GetName" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetName</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native string GetName(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le nom de l'objet actuel s'il en a un, une chaîne vide dans le cas contraire.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le nom renvoyé est la chaîne de texte affichée en haut du navigateur d'objet de propriété pour l'objet concerné.</li>
                                    <li><i class="material-icons">navigate_next</i> Si le nom a été attribué par le jeu, il s'agira du nom d'utilisateur de l'actif suivi d'un numéro de série.</li>
                                    <li><i class="material-icons">navigate_next</i> La chaîne renvoyée par cette méthode n'est pas garantie d'être unique, bien que TRS envoie des invites demandant une confirmation dans les cas où une duplication de noms est sur le point de se produire.</li>
                                    <li><i class="material-icons">navigate_next</i> Le nom persiste de session en session.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
string MyName = GetName();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="PostMessage" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>PostMessage</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void PostMessage(GameObject dest, string major, string minor, float seconds)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>dest =</strong> objet de destination pour le message. Si c'est nul, le message sera diffusé.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>major =</strong> message majeur à envoyer.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>minor =</strong> Message mineur à envoyer.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>seconds =</strong> Délai en secondes avant la publication du message. Si la valeur est zéro, le message sera immédiatement publié.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le nom de l'objet actuel s'il en a un, une chaîne vide dans le cas contraire.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Les messages envoyés via cette méthode sont traités par le routeur dans la prochaine mise à jour du jeu après leur échéance.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
PostMessage(me,"Timer","Tick",10.0);
PostMessage(null,"Browser-URL","live://next",0.0);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="SendMessage" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>SendMessage</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void SendMessage(GameObject dest, string major, string minor)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>dest =</strong> objet de destination pour le message. Si c'est nul, le message sera diffusé.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>major =</strong> message majeur à envoyer.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>minor =</strong> Message mineur à envoyer.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Envoie un message à l'objet de destination pour traitement immédiat.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
SendMessage(train,"Object","Enter");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Sleep" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Sleep</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void Sleep(float seconds)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>seconds =</strong> Temps d'attente en secondes.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Fait en sorte que le thread en cours marque une pause pendant la période spécifiée.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Sleep(10.0);
                                </code></pre>
                            </div>
                        </div>
                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                            <div class="card-content white-text">
                                <p>
                                    <i class="material-icons">warning</i> ATTENTION : <i>Sleep(World.PlaySound(...))</i> ne doit plus être utilisé car PlaySound ne renvoie plus la durée du fichier!</p>
                            </div>
                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Sniff" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Sniff</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native void Sniff(GameObject target, string major, string minor, bool state)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>target =</strong> Game Object auquel le gestionnaire sera attaché.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>major =</strong> message majeur à écouter.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>minor =</strong> Message mineur à écouter. Si ce paramètre est null ou une chaîne vide, tous les messages correspondant au paramètre <i>majeur</i> seront traités.</li>
                                    <li><i class="material-icons">navigate_next</i> <strong>state =</strong> indicateur booléen pour définir si le gestionnaire est actuellement actif ou non.</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucune</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Attache un gestionnaire de messages à un autre objet permettant au script en cours d'écouter les messages spécifiés.</li>
                                    <li><i class="material-icons">navigate_next</i> Les messages du type spécifié reçus par l'objet cible seront copiés dans le script actuel.</li>
                                    <li><i class="material-icons">navigate_next</i> Le script actuel aura généralement besoin d'un gestionnaire distinct pour traiter ces messages et devra normalement vérifier l'identité de la source du message.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Sniff(train,"Object","Enter",true);
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="exemple" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Exemple</h4>
                        <p>
                            Les appels de script effectués au cours de la méthode <i>Init()</i> d'un objet ne peuvent jamais être certains que d'autres objets de la carte ont déjà été initialisés. Dans les cas où l'initialisation dépend de l'existence d'autres objets,
                            <a href="#PostMessage">PostMessage()</a> peut être utilisé pour retarder tout ou partie du démarrage. Les messages envoyés de cette manière ne sont jamais traités avant la prochaine mise à jour du jeu et ceci ne se produit pas tant que tous les objets n'ont pas été initialisés.
                        </p>
                        <pre><code class="language-javascript" data-language="javascript">
public void Init(void)
{
   inherited();
   SetupTasks();
   AddHandler(me,"Setup","","SetupHandler");
   PostMessage(me,"Setup","",0.0);
}

void SetupHandler(Message msg)
{
   if (msg.src == me)
   {
      MoreSetupTasks();
   }
}
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="annexe" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Annexe</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <a href="#">Router.GetGameObject()</a></li>
                            <li><i class="material-icons">navigate_next</i> <a href="#">Router.PostMessage()</a></li>
                            <li><i class="material-icons">navigate_next</i> <a href="/referenciel/message">Référenciel <i>Message</i> TrainzScript</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#AddHandler">AddHandler</a></li>
                    <li><a href="#ClearMessages">ClearMessages</a></li>
                    <li><a href="#Exception">Exception</a></li>
                    <li><a href="#GetId">GetId</a></li>
                    <li><a href="#GetName">GetName</a></li>
                    <li><a href="#PostMessage">PostMessage</a></li>
                    <li><a href="#SendMessage">SendMessage</a></li>
                    <li><a href="#Sleep">Sleep</a></li>
                    <li><a href="#Sniff">Sniff</a></li>
                    <li><a href="#exemple">Exemple</a></li>
                    <li><a href="#annexe">Annexe</a></li>

                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection