@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Classe <strong>Asset</strong></h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="">GSO Object</a></li>
                        <li class="active">Asset</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <p>Le terme <i>Actif ou Asset</i> fait référence à un élément <i>objet</i> de Trainz. Tous les actifs ont un KUID qui fournit une identification unique, un fichier <i>config.txt</i> qui fournit des données de
    de configuration et un <i>StringTable</i> facultatif qui est un référenciel pour les valeurs de chaîne indexées et un moyen de fournir une traduction en langue étrangère.
    </p>
    <p>Un actif fournit les informations dont TRS a besoin pour placer un nouvel objet , également appelé instance de l'actif, dans l'environnement de jeu.</p>
    <p>Certaines des méthodes de cette classe font référence à la table kuid de l'actif <i>(kuid-table)</i>, un conteneur dans config.txt qui répertorie les kuids d'autres actifs nécessaires au bon fonctionnement. Ces actifs sont souvent répertoriés à l'aide d'un index entier:</p>
    <pre><code class="language-javascript" data-language="javascript">
kuid-table {
   0    <'kuid:44179:60013'>
   1    <'kuid:-3:10110'>
}
                                </code></pre>
    <p>Le kuid-table est toutefois au format Soup , ce qui permet d'utiliser des étiquettes descriptives. Il est ainsi plus facile pour un script de faire référence au KUID ou à la ressource concernée:</p>
    <pre><code class="language-javascript" data-language="javascript">
kuid-table {
   coal           <'kuid:44179:60013>
   greencorona    <'kuid:-3:10110>
}
                                </code></pre>

    <div class="row">
        <div class="col s12 m9 l10">
            <div id="FindAsset" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>FindAsset</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white"><strong>public native Asset FindAsset(string kuidTableAssetName)</strong></p><br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>kuidTableAssetName = </strong> valeur d'étiquette référençant une dépendance portée au <i>kuid table</i> de l'actif de la <i>config.txt</i> .</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Une référence à l'actif représenté par l' entrée <i>kuid-table</i>.</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                    <li><i class="material-icons">navigate_next</i> Cette méthode est l'équivalent de <i>World.FindAsset(GetAsset().LookupKUIDTable(kuidTableName))</i></li>
                                </ul>
                                <strong>Voir également</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <a href="#lookupKUIDTable">LookupKUIDTable()</a></li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
kuid-table {
   green  <'kuid:-3:10110>
}
                                </code></pre>
                                <pre><code class="language-javascript" data-language="javascript">
Asset asset = GetAsset().FindAsset("green");  // // renvoie une référence aux corona vert
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="getConfigSoup" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetConfigSoup</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native Soup GetConfigSoup(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le contenu du fichier config.txt appartenant à l'actif scripté au format Soup .</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> La soupe renvoyée par cette méthode sera en lecture seule.</li>
                                    <li><i class="material-icons">navigate_next</i> Les sous-conteneurs du fichier config.txt sont accessibles en tant que sous-soupes:</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
Soup soup = GetAsset().GetConfigSoup();
                                </code></pre>
                                <pre><code class="language-javascript" data-language="javascript">
// Accessible en tant que sous-soup

Soup meshTable = GetAsset().GetConfigSoup().GetNamedSoup("mesh-table");
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="getKUID" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetKUID</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native KUID GetKUID(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> KUID de l'actif actuel</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Le format de retour est <'kuid:1234:5678> ou <'kuid2:1234:5678:9> selon le cas.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
KUID kuid = GetAsset().GetKUID();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="getLocalisedName" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetLocalisedName</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native string GetLocalisedName(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Chaîne représentant le nom localisé de l'actif.</li>
                                    <li><i class="material-icons">navigate_next</i> Pour les versions anglaise du jeu , ce sera le contenu du fichier <i>config.txt</i> dans la balise <i>username</i></li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Pour les autres versions linguistiques, le résultat sera le nom localisé équivalent, s'il existe.</li>
                                    <li><i class="material-icons">navigate_next</i> Pour l'allemand, <i>username-de</i></li>
                                    <li><i class="material-icons">navigate_next</i> Pour l'italien, <i>username-it</i></li>
                                    <li><i class="material-icons">navigate_next</i> S'il n'y a pas de balise localisée, le contenu de la balise <i>username</i> sera retourné.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
string username = GetAsset().GetLocalisedName();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="getStringTable" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>GetStringTable</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native StringTable GetStringTable(void)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Aucun</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Une référence à la classe <i>StringTable</i> de l'actif .</li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Tout actif peut avoir un StringTable dans sa configuration qui consiste en une collection d’éléments de chaîne nommés au format Soup .</li>
                                    <li><i class="material-icons">navigate_next</i> Les versions en langue étrangère renverront une StringTable localisée, en utilisant par exemple le conteneur <i>config.txt string-table-es</i> . Cela permet la traduction de l'interface utilisateur d'un actif.</li>
                                    <li><i class="material-icons">navigate_next</i> L' objet <i>StringTable</i> renvoyé fournit des méthodes pour accéder facilement à ces chaînes.</li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
StringTable strings = GetAsset().GetStringTable();
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="lookupKUIDTable" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>LookupKUIDTable</h4>
                        <p style="border: solid 1px #000; padding: 5px; background-color: rgba(27,15,6,0.5); border-radius: 15px; color: white">
                            <strong>public native KUID LookupKUIDTable(string kuidTableAssetName)</strong>
                        </p>
                        <br>
                        <div class="row">
                            <div class="col s7">
                                <strong>Parametres:</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <strong>kuidTableAssetName = </strong> valeur d'étiquette référençant une dépendance portée au <i>kuid table</i> de l'actif de la <i>config.txt</i> .</li>
                                </ul>
                                <strong>Valeur retournée</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Une référence au KUID représenté par l' entrée <i>kuid-table.</i></li>
                                </ul>
                                <strong>Remarque</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> Retourne null si l'entrée kuid-table est invalide ou si elle n'existe pas.</li>
                                </ul>
                                <strong>Voir également</strong>
                                <ul>
                                    <li><i class="material-icons">navigate_next</i> <a href="#FindAsset">FindAsset()</a></li>
                                </ul>
                            </div>
                            <div class="col s5">
                                <strong>Syntax</strong>
                                <pre><code class="language-javascript" data-language="javascript">
kuid-table {
   green  <'kuid:-3:10110>
}
                                </code></pre>
                                <pre><code class="language-javascript" data-language="javascript">
Asset asset = GetAsset().LookupKUIDTable("green");  // // renvoie une référence aux corona vert
                                </code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="annexe" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Annexe</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <a href="#">TrainzGameObject.GetAsset()</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#FindAsset">FindAsset</a></li>
                    <li><a href="#getConfigSoup">GetConfigSoup</a></li>
                    <li><a href="#getKUID">GetKuid</a></li>
                    <li><a href="#getLocalisedName">GetLocalisedName</a></li>
                    <li><a href="#getStringTable">GetStringTable</a></li>
                    <li><a href="#lookupKUIDTable">LookupKUIDTable</a></li>
                    <li><a href="#annexe">Methode Annexe</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection