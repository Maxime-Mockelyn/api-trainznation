@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Commentaires & Délimitations</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Element de programmation</a></li>
                        <li class="active">Commentaires & Délimitations</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div class="section">
        <h4 class="header">Commentaires</h4>
        <div class="card">
            <div class="card-content">
                <code class="language-css">//</code>
                <br>
                <p>Commentaire sur une seule ligne, le texte avant le prochain retour à la ligne sera ignoré par le compilateur.</p>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <code class="language-css">/* */</code>
                <br>
                <ul>
                    <li>Bloquer le commentaire</li>
                    <li>Le texte entre les paires de caractères '/ *' et '* /' sera ignoré.</li>
                    <li>Cette convention peut être utilisée pour désactiver temporairement l'exécution de sections de code.</li>
                    <li>Les étiquettes de commentaire bloquées ne peuvent pas être imbriquées, mais elles peuvent contenir des commentaires d’une seule ligne.</li>
                </ul>
            </div>
        </div>
        <div class="divider"></div>
        <h4 class="header">Délimiteurs</h4>
        <div class="card">
            <div class="card-content">
                <code class="language-css">:</code>
                <br>
                <ul>
                    <li>Marque la fin d'une déclaration d'étiquette</li>
                    <li>Voir <i>on,switch,case,default,label</i></li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <code class="language-css">;</code>
                <br>
                <ul>
                    <li>Marque la fin d'une déclaration</li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <code class="language-css">{}</code>
                <br>
                <ul>
                    <li>Délimite l'étendue d'une fonction ou d'une méthode</li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-content">
                <code class="language-css">{};</code>
                <br>
                <ul>
                    <li>Délimite l'étendue d'une classe (class)</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection