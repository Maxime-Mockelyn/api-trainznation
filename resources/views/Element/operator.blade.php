@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Opérateurs</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Element de programmation</a></li>
                        <li class="active">Opérateur</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <h4 class="header">Les Opérateurs</h4>
    <div class="row">
        <div class="col s12 m9 l10">
            <div id="1" class="section scrollspy">
                <h5 class="header">Regroupement et indexation</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>()</h4>
                        <ul>
                            <li>Groupement d'expression</li>
                            <li>Comme la plupart des langages de programmation, Trainzscript exécute des opérations arithmétiques dans l'ordre défini par les règles de priorité des opérateurs.</li>
                            <li><i>3 + 2 * 12 - 7</i> renverrait 20, car la multiplication a une priorité plus élevée que l'addition et la soustraction.</li>
                            <li>Vous pouvez forcer le compilateur à modifier cette séquence et à préciser vos intentions en le déclarant sous la forme <i>((3 + 2) * 12) - 7</i> .</li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>[i]</h4>
                        <ul>
                            <li>Indexation de tableaux et de chaînes.</li>
                            <li>Les tableaux sont basés sur zéro et <i>arrayName [0]</i> représentera le premier élément.</li>
                            <li>De même, <i>arrayName [arrayName.size () - 1]</i> représentera le dernier élément.</li>
                            <li>Un appel à une valeur en dehors de la plage [0… size () - 1] générera une exception.</li>
                            <li>Cette notation peut également être utilisée pour renvoyer un seul caractère d'une chaîne.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
intArray[intArray.size()] = 5 //ajoutera la nouvelle valeur de 5 à la fin du tableau.
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>[i,j]</h4>
                        <ul>
                            <li>Tableau et index de plage.</li>
                            <li>L'expression <i>string [0, string.size ()]</i> renverra la chaîne entière.</li>
                            <li>Il est préférable de penser que les entiers i et j sont des positions de «curseur», où i est le premier élément et j est supérieur au dernier élément.</li>
                            <li>Si i est omis, la plage commencera au début du tableau (index 0).</li>
                            <li>Si j est omis, la plage se terminera après la fin du tableau (.size ()).</li>
                            <li>Pour renvoyer un seul élément autre que le premier ou le dernier, i et j sont nécessaires.</li>
                            <li></li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
s[,3]  = "abc"
s[2,3] = "c"
s[2,]  = "cde"
                                </code></pre>
                        <pre><code class="language-javascript" data-language="javascript">
intArray[2,3] = null             // supprimera le troisième élément du tableau.
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>.</h4>
                        <ul>
                            <li>Opérateur de déréférence</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
GetAsset().GetConfigSoup();
                                </code></pre>
                        <p>La méthode <i>GetConfigSoup()</i> à appeler est la méthode définie pour la classe <i>GetAsset().</i></p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>!</h4>
                        <p>Non Opérateur</p>
                        <pre><code class="language-javascript" data-language="javascript">
!true = false
!vehicle = null
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="2" class="section scrollspy">
                <h5 class="header">Opérateurs incrémentaux et unitaires</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>++</h4>
                        <ul>
                            <li>Incrémente une valeur numérique de 1.</li>
                            <li>Souvent utilisé pour les variables de contrôle dans les boucles:</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
for(n = 0; n < list.size(); n++) {
   DoStuffWith(n);
}
                                </code></pre>
                        <p>Incrémente n de 1 à chaque passage dans la boucle.</p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>--</h4>
                        <ul>
                            <li>Décrémente une valeur numérique de 1.</li>
                            <li>Souvent utilisé pour les variables de contrôle dans les boucles:</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
n = list.size() - 1;
while(n >= 0) {
   DoStuffWith(n);
   n--;
}
                                </code></pre>
                        <p>Décrémente n de 1 à chaque passage dans la boucle.</p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>+</h4>
                        <ul>
                            <li>Plus Unitaire</li>
                            <li>Utilisé pour indiquer qu'une valeur est positive.</li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>-</h4>
                        <ul>
                            <li>Moins Unitaire</li>
                            <li>Utilisé pour indiquer qu'une valeur est négative.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="3" class="section scrollspy">
                <h5 class="header">Types ou Casts</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>(bool)</h4>
                        <p>Type booléen</p>
                        <pre><code class="language-javascript" data-language="javascript">
(bool) 0 = false
(bool) 0.0 = false
(bool) n = true     // où n est supérieur à zéro
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>(int)</h4>
                        <p>Type Entier</p>
                        <pre><code class="language-javascript" data-language="javascript">
(int) 5.0 = 5
(int) true = 1
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>(float)</h4>
                        <p>Type Flottant ou décimal</p>
                        <pre><code class="language-javascript" data-language="javascript">
(float) 100 = 100.0
(float) false = 0.0
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>(string)</h4>
                        <p>Type Chaine de caractère</p>
                        <pre><code class="language-javascript" data-language="javascript">
(string) 1 = "1"
(string) 0.99 = "0.99"
(string) false = "false";
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>Cast</h4>
                        <p>Permet de faire appel à des classe interne ou externe si elle sont public</p>
                        <ul>
                            <li>En règle générale, <i>cast</i> est dynamique et permet de convertir une classe de manière lisible</li>
                            <li>Renvoie la valeur null si la référence ne peut pas être convertie vers la classe spécifiée par className.</li>
                            <li>Notez que les ancêtres de la classe spécifiée seront retournés.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Vehicle vehicle = cast<'Vehicle>msg.src;

MeshObject meshObject;
GameObject gameObject = cast<'GameObject>meshObject;  // retourne meshObject si l'objet existe, null sinon
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>size()</h4>
                        <ul>
                            <li>Retourne le nombre de membres d'un tableau ou la longueur d'une chaîne.</li>
                            <li>Dans le cas d'un tableau, cela ne signifie pas nécessairement que tous les membres contiennent réellement des valeurs.</li>
                        </ul>

                        <pre><code class="language-javascript" data-language="javascript">
given string s = "abcde";
s.size() = 5;

int[] numbers = new int[5];       // crée un tableau vide de 5 champs
numbers.size() = 5;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>copy()</h4>
                        <ul>
                            <li>Crée une copie d'un tableau ou d'une chaîne.</li>
                            <li>Cela diffère d'une simple affectation qui pointe une variable vers la même adresse mémoire qu'une variable existante.</li>
                            <li><i>Array.copy()</i> ou <i>string.copy()</i> crée une copie supplémentaire des données.</li>
                        </ul>

                        <pre><code class="language-javascript" data-language="javascript">
int[] numbers = new int[5];
int[] moreNumbers;

moreNumbers.copy(numbers);  // duplique les nombres
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>isclass()</h4>
                        <p>Utilisé dans la déclaration d'un nouvel identifiant de classe</p>

                        <pre><code class="language-javascript" data-language="javascript">
class MyScript isclass Vehicle {
  classMethods ();
};
                                </code></pre>

                        <p>Utilisé pour tester si une référence donnée est à une classe spécifique.</p>

                        <pre><code class="language-javascript" data-language="javascript">
TrackLoadInfo info = new TrackLoadInfo();

info.isclass(TrackLoadInfo);               // Retourne Vrai
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>new</h4>
                        <p>Crée une nouvelle instance du type spécifié.</p>

                        <pre><code class="language-javascript" data-language="javascript">
int[] numbers = new int[4];                // crée un tableau entier contenant 4 membres.
TrackLoadInfo info = new TrackLoadInfo();  // crée une nouvelle instance de la classe TrackLoadInfo.
                                </code></pre>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> new n'initialise pas les membres.</li>
                            <li><i class="material-icons">navigate_next</i> Les classes GameObject ne peuvent pas être créées avec new.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="4" class="section scrollspy">
                <h5 class="header">Multiplications & Divisions</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>*</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Multiplication</li>
                            <li><i class="material-icons">navigate_next</i> Si les deux termes sont des entiers, le résultat sera un entier.</li>
                            <li><i class="material-icons">navigate_next</i> Si l'un des termes est un décimal, le résultat sera un décimal.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
2 * 5 = 10;
2 * 5,0 = 10,0;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>/</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Division</li>
                            <li><i class="material-icons">navigate_next</i> Si les deux termes sont des entiers, le résultat sera un entier, éventuellement avec un reste.</li>
                            <li><i class="material-icons">navigate_next</i> Si l'un des termes est un décimal, le résultat sera un décimal.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
10/5 = 2;
10 / 5,0 = 2,0;
10/6 = 1;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>%</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Reste ou opérateur de module.</li>
                            <li><i class="material-icons">navigate_next</i> Renvoie le reste d'une division entière.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
5% 2 = 1;
105% 50 = 5;
105% 100 = 5;
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="5" class="section scrollspy">
                <h5 class="header">Addition & Soustraction</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>+</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Addition</li>
                            <li><i class="material-icons">navigate_next</i> Si les deux termes sont des entiers, le résultat sera un entier.</li>
                            <li><i class="material-icons">navigate_next</i> Si l'un des termes est un décimal, le résultat sera un décimal.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
1 + 1 = 2;
1 + 1,0 = 2,0;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>-</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Soustraction</li>
                            <li><i class="material-icons">navigate_next</i> Si les deux termes sont des entiers, le résultat sera un entier.</li>
                            <li><i class="material-icons">navigate_next</i> Si l'un des termes est un décimal, le résultat sera un décimal.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
1 + 1 = 2;
1 + 1,0 = 2,0;
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="6" class="section scrollspy">
                <h5 class="header">Shift</h5>
                <div class="card">
                    <div class="card-content">
                        <h4><<</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Maj gauche</li>
                            <li><i class="material-icons">navigate_next</i> L'opérateur binaire déplace toutes les valeurs binaires vers la gauche du nombre de places spécifié dans le second terme.</li>
                            <li><i class="material-icons">navigate_next</i> Le zéro binaire est ajouté.</li>
                            <li><i class="material-icons">navigate_next</i> À condition qu'il n'y ait pas de dépassement de capacité, le résultat est un doublement de la valeur pour chaque shift.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
8 << 1 = 16
8 << 2 = 32
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>>></h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Maj droit</li>
                            <li><i class="material-icons">navigate_next</i> L'opérateur au niveau des bits déplace toutes les valeurs binaires d'une place à droite du nombre de places spécifié dans le second terme.</li>
                            <li><i class="material-icons">navigate_next</i> Le zéro binaire est ajouté au début et tous les bits déplacés sont supprimés.</li>
                            <li><i class="material-icons">navigate_next</i> Le résultat est une division intégrale de la valeur pour chaque shift.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
8 >> 1 = 4
9 >> 2 = 2
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="7" class="section scrollspy">
                <h5 class="header">Comparaison</h5>
                <div class="card">
                    <div class="card-content">
                        <h4><</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Est inférieur à</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool.</li>
                            <li><i class="material-icons">navigate_next</i> Retourne vrai si le premier terme est inférieur au second.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
1 < 2 = true;
false < false = false;
"abc" < "def" = true;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4><=</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Est inférieur ou égal</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool.</li>
                            <li><i class="material-icons">navigate_next</i> Retourne vrai si le premier terme est inférieur ou égal au second.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
1 <= 2 = true;
false <= false = true;
"abc" <= "def" = true;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>></h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Est supérieur à.</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool.</li>
                            <li><i class="material-icons">navigate_next</i> Retourne vrai si le premier terme est supérieur au second.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
1 > 2 = false;
true > false = true;
"abc" > "def" = false;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>>=</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Est supérieur ou égal.</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool.</li>
                            <li><i class="material-icons">navigate_next</i> Retourne vrai si le premier terme est supérieur ou égal au second.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
1 >= 2 = false;
true >= true = true;
"abc" >= "def" = false;
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="8" class="section scrollspy">
                <h5 class="header">Egalité</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>==</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Est égal.</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool et sur les références d'objet.</li>
                            <li><i class="material-icons">navigate_next</i> Renvoie vrai si les premier et second termes sont égaux ou s'ils correspondent au même objet.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
int a = 0;
int b = 0;
a == b;  // returns true
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>!=</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> N'est pas égal ou différent.</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool et sur les références d'objet.</li>
                            <li><i class="material-icons">navigate_next</i> Renvoie brai si les premier et second termes ne sont pas égaux et ne sont pas évalués avec le même objet.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
int a = 0;
int b = 0;
a != b;  // returns false
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="9" class="section scrollspy">
                <h5 class="header">Opérateurs bit à bit</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>&</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Effectue une comparaison bit à bit de deux entiers et renvoie une nouvelle valeur.</li>
                            <li><i class="material-icons">navigate_next</i> Chaque bit du résultat est activé si les deux bits des arguments correspondants sont définis</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
  0000 1111
& 0001 0111
= 0000 0111

15 & 23 = 7
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>!=</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> N'est pas égal ou différent.</li>
                            <li><i class="material-icons">navigate_next</i> Opérateur de comparaison utilisable sur les valeurs string, float, integer et bool et sur les références d'objet.</li>
                            <li><i class="material-icons">navigate_next</i> Renvoie brai si les premier et second termes ne sont pas égaux et ne sont pas évalués avec le même objet.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
int a = 0;
int b = 0;
a != b;  // returns false
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="10" class="section scrollspy">
                <h5 class="header">Opérateurs Logique</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>and</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Permet à plusieurs expressions booléennes d'être combinées.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
if (msg.minor == "Enter" and doorsOpen)
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>or</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Permet à plusieurs expressions booléennes d'être combinées.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
if (msg.minor == "Enter" or doorsOpen)
if ((msg.minor == "Enter" and doorsOpen) or (msg.minor == "Leave" and !doorsOpen))
                                </code></pre>
                    </div>
                </div>
            </div>
            <div class="divider"></div>
            <div id="11" class="section scrollspy">
                <h5 class="header">Affectation</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>=</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Opérateur d'assignement</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
a = 1;
Train train = World.GetTrainList()[0];
                                </code></pre>
                        <p>L'opérateur d'affectation définit la variable sur le LHS de l'expression sur la même valeur que la variable sur le RHS.</p>
                        <p>Notez que string et array sont des types d'objet et non des types primitifs. Par conséquent, comme tous les objets, l'affectation d'un objet de type chaîne ou tableau signifie que la variable du LHS de l'expression d'affectation fait référence au même objet (c'est-à-dire, la même chaîne ou le même tableau) que la variable du RHS. Une nouvelle copie de l'objet n'est pas créée. Si vous souhaitez affecter un nouvel objet à une copie de l'original, vous devez utiliser la méthode .copy () de la chaîne ou des objets de tableau, ou la méthode .clone () (si disponible) d'autres objets. .</p>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>,</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Séparateur d'expression ou de paramètre.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br><br><br>
                <ul class="section table-of-contents">
                    <li><a href="#1">Regroupement et indexation</a></li>
                    <li><a href="#2">Opérateurs incrémentaux et unitaires</a></li>
                    <li><a href="#3">Types ou Casts</a></li>
                    <li><a href="#4">Multiplications & Divisions</a></li>
                    <li><a href="#5">Addition & Soustraction</a></li>
                    <li><a href="#6">Shift</a></li>
                    <li><a href="#7">Comparaison</a></li>
                    <li><a href="#8">Egalité</a></li>
                    <li><a href="#9">Opérateurs bit à bit</a></li>
                    <li><a href="#10">Opérateurs Logique</a></li>
                    <li><a href="#11">Affectation</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection