@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Les Tableaux</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Element de programmation</a></li>
                        <li class="active">Les Tableaux</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <h4 class="header">Les Types</h4>
    <div class="row">
        <div class="col s12 m9 l10">
            <div id="declare" class="section scrollspy">
                <h5 class="header">Declaration, Initialisation & Population</h5>
                <ul>
                    <li><i class="material-icons">navigate_next</i> Les tableaux doivent être déclarés et initialisés avant de pouvoir être accédés, et il sera généralement nécessaire de renseigner le tableau avec des données significatives.</li>
                </ul>
                <div class="card">
                    <div class="card-content">
                        <h4>Déclaration</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Une déclaration réserve de la mémoire pour la variable et attribue un nom à ce tableau particulier. C'est comme si un promoteur immobilier achetait un site et le réservait pour une construction future.</li>
                            <li><i class="material-icons">navigate_next</i> Pour déclarer un tableau, vous devez d’abord définir le type d’objet que le tableau contiendra, suivi de l’ouverture et de la fermeture de crochets et du nom que vous utiliserez pour référencer la variable du tableau.</li>
                            <li><i class="material-icons">navigate_next</i> Tout comme les aménagements résidentiels peuvent être constitués de n’importe quel type de bâtiment, les tableaux peuvent être composés de presque toutes les classes, y compris les classes que vous avez définies vous-même.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Vehicle[] vehicles;
int[] numberList;
string[] words;
GameObject[] objects;
MyOwnClass[] myObjects;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>Initialisation</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> De la même manière qu'un développeur doit décider du nombre d'unités qu'il souhaite construire, vous devez déterminer le nombre de membres que votre tableau pourra initialement contenir. Vous pourrez agrandir ou réduire le tableau ultérieurement, car vous ne définissez que la taille initiale.</li>
                            <li><i class="material-icons">navigate_next</i> Pour initialiser le tableau, utilisez le mot-clé new, suivi de la classe d'objet que le tableau doit contenir et du nombre initial de membres entre crochets.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
vehicles = new Vehicle[5];
numberList = new int[10];
words = new string[2];
objects = new GameObject[3];
myObjects = new MyOwnClass[7];
                                </code></pre>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Il est possible, et souvent pratique, de déclarer et d'initialiser le tableau en une seule instruction:</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Vehicle[] vehicles = new Vehicle[5];
int[] numberList = new int[10];
string[] words = new string[2];
GameObject[] objects = new GameObject[3];
MyOwnClass[] myObjects = new MyOwnClass[7];
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>Contenue ou population</h4>
                        <p>A l'heure actuelle, nous avons le terrain et des maisons dessus mais personne pour les habités, il convient d'utiliser les syntax de remplissage suivante:</p>
                        <pre><code class="language-javascript" data-language="javascript">
vehicles[0] = GetMyTrain().GetVehicles[3];
numberList[0] = 0;
words[3] = "sentences"
objects[0] = me;
myObjects[2] = Router.GetGameObject("my sibling");
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>Les Tableaux (Méthode)</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Un autre raccourci consiste à utiliser une méthode qui renvoie un tableau pour initialiser et remplir votre tableau en une seule instruction. Souvent, la même déclaration peut également contenir la déclaration:</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Vehicle[] vehicles = World.GetVehicleList();
int[] numberList = GetNumberList(1,10);
string[] words = Str.Tokens("comma,separated,list",",");
                                </code></pre>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Vous pouvez écrire vos propres méthodes pour renvoyer des tableaux:</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
int[] GetNumberList(int start, int count) {
  int[] result = new int[0];
  int i;
  for (i = start; i < count; i++) {
    result[result.size()] = i;
  }
  return result;
}
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="operator" class="section scrollspy">
                <h5 class="header">Les Opérateurs de tableaux</h5>
                <div class="card">
                    <div class="card-content">
                        <h4>Ajouter à un tableau</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> C'est une syntaxe qui vous permet d'augmenter la taille d'un tableau correctement initialisé en ajoutant un nouveau membre à la fin. Comme l'indexation d'un tableau commence à zéro, le dernier membre sera toujours <i>array[array.size() - 1]</i> .</li>
                            <li><i class="material-icons">navigate_next</i> Si vous essayez de lire à partir de <i>array[array.size()]</i>, vous obtiendrez une erreur de plage car vous tenterez d'accéder à un emplacement inexistant.</li>
                            <li><i class="material-icons">navigate_next</i> Assigner à <i>array[array.size()]</i> créera cependant un nouveau membre et incrémentera le nombre d'éléments dans le tableau.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
vehicles[vehicles.size()] = me;
                                </code></pre>
                    </div>
                </div>
                <div class="card">
                    <div class="card-content">
                        <h4>Indexation</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Un indice entre crochets sert à définir un élément individuel dans un tableau. La notation n'aura de sens que si une valeur a été attribuée à cet élément. En supposant que tel soit le cas, il s’agit d’une erreur courante qui peut être très difficile à trouver.</li>
                            <li><i class="material-icons">navigate_next</i> Compte tenu de ces déclarations:</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
int[] numbers = new int[3];
numbers[0] = 0;
numbers[2] = 2;
                                </code></pre>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <i>numbers[0]</i> retournera 0 sousforme d'entier</li>
                            <li><i class="material-icons">navigate_next</i> <i>numbers[2]</i> retournera la valeur 2</li>
                            <li><i class="material-icons">navigate_next</i> <i>numbers[1]</i> Renverra null car aucune valeur n’a été attribuée à cet élément particulier</li>
                        </ul>
                        <p>L'indexation d'un tableau commence à zéro et se termine à <i>array.size() - 1</i> .</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#declare">Declaration, Initialisation & Population</a></li>
                    <li><a href="#operator">Les Opérateurs de tableaux</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection