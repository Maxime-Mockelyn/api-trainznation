@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Mots-clés</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Element de programmation</a></li>
                        <li class="active">Mots-clés</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
    <div id="card-alert" class="card blue">
        <div class="card-content white-text">
            <span class="card-title white-text darken-1">
              <i class="material-icons">info</i> Information
            </span>
            <p>Cette page est encore en construction.</p>
        </div>
        <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="section">
        <h4 class="header">Mots-clé</h4>
        <div class="row">
            <div class="col s12 m9 l10">
                <div id="break" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Break</h4>
                            <p>Permet de sortir du block actuel dans les instructions de type: <i>for,wait,switch,while</i></p>
                            <pre><code class="language-javascript" data-language="javascript">
// Cette Boucle ce termine lorsque n est supérieur à 5 ou lorsque n == list.size()<br>
for(n=0; n < list.size(); n++){
    if(n > 5) break;
}
                                </code></pre>
                        </div>
                    </div>
                </div>
                <div id="case" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Case</h4>
                            <p>Ce mot clé est déclarer dans l'instruction <i>switch</i>, il propose l'étude d'un cas individuel suivant une déclaration.<br>
                            Si aucune des déclaration d'une <i>case</i> n'est retenue le <i>default</i> est utiliser.
                            </p>
                            <pre><code class="language-javascript" data-language="javascript">
int i;
string dayOfWeek;
for(i = 0; i < 6; i++) {
   switch(i) {
      case 0 :  dayOfWeek = "Sunday";
                break;
      case 1 :  dayOfWeek = "Monday";
                break;
      case 2 :  dayOfWeek = "Tuesday";
                break;
      case 3 :  dayOfWeek = "Wednesday";
                break;
      case 4 :  dayOfWeek = "Thursday";
                break;
      case 5 :  dayOfWeek = "Friday";
                break;
      case 6 :  dayOfWeek = "Saturday";
                break;
      default : dayOfWeek = "ERROR";
   }
   Interface.Print(dayOfWeek);
}
                                </code></pre>
                            <p>Dans notre cas, cette fonction affichera le jour de la semaine en fonction de <i>i</i></p>
                        </div>
                    </div>
                </div>
                <div id="class" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Class</h4>
                            <p>Permet de définir une nouvelle classe apparenté à une classe parente.</p>
                            <pre><code class="language-javascript" data-language="javascript">
class ChildClass isclass ParentClass {

    class members...
    class methods...

};
                                </code></pre>
                            <div id="card-alert" class="card gradient-45deg-light-blue-cyan">
                                <div class="card-content white-text">
                                    <p>
                                        <i class="material-icons">info_outline</i> INFO : Plusieurs classe peuvent être incluse dans le même fichier</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="continue" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Continue</h4>
                            <p>Va jusqu'à la prochaine itération pour une boucle <i>for,while ou wait</i>.</p>
                            <pre><code class="language-javascript" data-language="javascript">
int i;
for(i = 0; i < 10; ++i) {
   if(i == 5)
   continue;
   Interface.Print(i);
}
                                </code></pre>
                            <p>Dans cette exemple, ce code va afficher les valeurs de 0 à 4, passer la 5 et afficher les valeurs de 6 à 9.</p>
                        </div>
                    </div>
                </div>
                <div id="default" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Default</h4>
                            <p>Voir <a href="#case">Case</a></p>
                        </div>
                    </div>
                </div>
                <div id="define" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Define</h4>
                            <p>Permet de déclarer une constante</p>
                            <pre><code class="language-javascript" data-language="javascript">
define string firstday = "Monday";
define public int daysInWeek = 7;
                                </code></pre>
                            <p>
                                La Première constante est de type chaine (string) qui sera disponible n'importe ou dans la classe ou elle est déclarer.<br>
                                la deuxième constante est de type Entier (int) et est disponible dans la classe actuelle et dans des classes extérieur <i>déclaration public</i>
                            </p>
                            <div id="card-alert" class="card gradient-45deg-amber-amber">
                                <div class="card-content white-text">
                                    <p>
                                        <i class="material-icons">warning</i> Attention : Les constantes doivent être initialisées lors de la déclaration et ne peuvent pas être modifiées dans le code.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="final" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Final</h4>
                            <p>Spécificateur de classe ou de méthode qui empêche le script de remplacer cette déclaration.</p>
                            <pre><code class="language-javascript" data-language="javascript">
final class Info {
  string name;
  string address;
}
                                </code></pre>
                            <p>Compte tenu de ce qui précède de la déclaration, la déclaration suivante ne serait pas possible</p>
                            <pre><code class="language-css" data-language="css">
class DetailedInfo isclass Info {
  string telephone;
}
                                </code></pre>
                        </div>
                    </div>
                </div>
                <div id="for" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>For</h4>
                            <p><i>For</i> est une instruction de boucle à flux de controle</p>
                            <pre><code class="language-javascript" data-language="javascript">
int i;
Junction[] junctions = World.GetJunctionList();
for (i = 0; i < junctions.size(); i++) {
   string name = junctions[i].GetName();
   Interface.Print("Junction " + i + " is called " + name);
}
                                </code></pre>
                            <p>Une boucle <i>For</i> comporte 4 partie:</p>
                            <ul>
                                <li>A: Initialise une variable de compteur et est éxecuter avant la boucle. (<i>i = 0</i>)</li>
                                <li>B: Est la condition du test. Tant que B est vrai, la boucle continue (<i>i < junctions.size()</i>)</li>
                                <li>C: Est l'expression de la boucle qui sera exécuter à la fin de chaque boucle (<i>i++</i>)</li>
                                <li>D: Est le code exécuter à chaque boucle (<i>Intérieur du for</i>)</li>
                            </ul>
                            <p>Selon les valeurs des parties A et B, la boucle peut ne pas s'exécuter du tout.</p>
                        </div>
                    </div>
                </div>
                <div id="goto" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Goto</h4>
                            <p>Cette instruction va créer une étiquette.</p>
                            <pre><code class="language-javascript" data-language="javascript">
int i = 5

A: // label statement
Interface.Print("hello");
if(i--) goto A;
                                </code></pre>
                            <p>Ce code va afficher 'Hello' 5 fois. (0 n'est pas compté)</p>
                        </div>
                    </div>
                </div>
                <div id="if" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>If</h4>
                            <p>
                                Cette instruction est une condition qui va vérifier si un test est vrai et exécuter un code à son occurence et exécuter une autre code si elle est fausse <i>(else)</i><br>
                                <i>Else et Else if</i> sont optionnel, c'est à dire que vous pouvez très bien n'afficher que la condition <i>if</i>.
                            </p>
                            <pre><code class="language-javascript" data-language="javascript">
int i = Math.Rand(0, 3);

if (i == 0) {
   Interface.Print("i is 0");
}
else if (i == 1) {
   Interface.Print("i is 1");
}
else {
   Interface.Print("i is 2");
}
                                </code></pre>
                            <div id="card-alert" class="card gradient-45deg-amber-amber">
                                <div class="card-content white-text">
                                    <p>
                                        <i class="material-icons">warning</i> Attention : Si vous avez plusieurs test à effectuer préférer l'utilisation du
                                        <a href="#switch">switch</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="include" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Include</h4>
                            <ul>
                                <li><strong>Syntax:</strong> <i>include "file.gs"</i></li>
                                <li>Cette déclaration doit être indiquer en tout début de fichier avant la déclaration de la classe primaire</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="inherited" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Inherited</h4>
                            <p>Les scripts de trainz étant orienté Objet, il convient d'hérité de certaines classe, il faut donc savoir:</p>
                            <ul>
                                <li>Que l'instruction <i>inherited doit être déclarer dans la classe qui doit faire appel à la classe parent de la méthode.</i></li>
                                <li>Quel renvoie le même type et prend les mêmes paramètres que la méthode parente.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="mandatory" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Mandatory</h4>
                            <p>Il s'agit d'un <i>Spécificateur de méthode</i> qui indique que, si la méthode est remplacé, elle doit être obligatoirement être hérité. <i>(inherited)</i></p>
                        </div>
                    </div>
                </div>
                <div id="me" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Me</h4>
                            <p>Cette instruction fait référence à l'objet actuel, il est similaire à <i>self</i> ou <i>this</i> dans les autres languages.</p>
                            <pre><code class="language-javascript" data-language="javascript">
me.SetMeshAnimationState();
SetMeshAnimationState()
                                </code></pre>
                            <p>Ce code déclanche une animation sur l'objet actuel mais la deuxième ligne déclenche également cette animation, ont peut donc ommettre l'instruction.</p>
                        </div>
                    </div>
                </div>
                <div id="native" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Native</h4>
                            <p>Il s'agit d'un <i>spécificateur de méthode</i> qui indique que la méthode est implémentée par le jeu plutôt que dans le code du script</p>
                        </div>
                    </div>
                </div>
                <div id="null" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Null</h4>
                            <p><i>Null</i> est utiliser dans beaucoup de language, voici c'est spécificité:</p>
                            <ul>
                                <li>C'est une expression non initialisé</li>
                                <li>Toutes références non utilisée ou affectée à un objet à cette valeur</li>
                                <li>L'attribution de null à un objet existant supprimera la référence à cet objet.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="on" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>On</h4>
                            <p>Voir <a href="#wait">Wait</a></p>
                        </div>
                    </div>
                </div>
                <div id="public" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Public</h4>
                            <p>Un <i>Spécificateur de classe ou de méthode</i> qui indique que la classe ou la méthode est accessible à l'extérieur du script actuelle</p>
                        </div>
                    </div>
                </div>
                <div id="return" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Return</h4>
                            <p>Tout comme <a href="#null">null</a>, <i>Return</i> est utiliser dans beacoup de language et à des spécificité:</p>
                            <ul>
                                <li>Il permet de quitter instantanément la méthode actuelle.</li>
                                <li>Lors de la sortie, il doit soit avoir une valeur null ou une valeur faisant référence au <i>type</i> de la méthode utiliser (string, int, boolean, etc...).</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="static" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Static</h4>
                            <p>C'est un <i>spécificateur de classe</i> indiquant qu'une seule instance de la classe peut exister. Certaines spécificitées sont à respecter:</p>
                            <ul>
                                <li>Les classes statiques ne peuvent pas commencer ou être créer avec l'opérateur <i>new</i></li>
                                <li>Pour accéder à une classe statique, il faut utiliser obligatoirement la classe ou la methode directement</li>
                            </ul>
                            <pre><code class="language-javascript" data-language="javascript">
static class owner {

   public string forename;              // public member
   public string surname;               // public member
   public string GetName(void) {        // public method
      return forename + " " + surname;
   }

};

class postbox isclass MapObject {

   public void Init(void) {
      if (!owner.forename) owner.forename = "Fred";
      if (!owner.surname) owner.surname = "Bloggs";
      Interface.Print(owner.GetName());
   }

}
                                </code></pre>
                            <p>Comme une seul instance de <i>owner</i> peut exister, elle sera créer en même temps que la première instance de <i>postbox</i></p>
                        </div>
                    </div>
                </div>
                <div id="switch" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Switch</h4>
                            <p>Voir <a href="#case">case</a></p>
                        </div>
                    </div>
                </div>
                <div id="thread" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Thread</h4>
                            <p>
                                Un <i>Thread</i> est une méthode qui s'exécute indépendament du script principal. (Asynchrone)<br>
                                Les <i>threads</i> peuvent être démarrés et synchronisés à l'aide de méthodes standard ou d'autres threads, mais sont autorisés à attendre des événements ou à effectuer des tâches nécessitant des périodes d'inactivité sans interrompre le flux du programme principal.<br>
                                Les méthodes threadées ne peuvent être déclarées que dans les classes étendant la classe GameObject ou ses enfants.<br>
                                De nombreux threads (jusqu'à 64) peuvent être exécutés à la fois sur la même instance d'objet de jeu.
                            </p>
                            <pre><code class="language-javascript" data-language="javascript">
class Tutorial isclass Buildable {

  bool doorsOpen = false;

  public void Init(void) {
     inherited();
     AddHandler(me,"Object","","ObjectHandler");
  }

  thread void RingTheBell(void) {
     while (doorsOpen) {
        Sleep(0.35 + World.Play2DSound(GetAsset(),"bell.wav"));
     }
  }

  void ObjectHandler(Message msg) {
     if (msg.minor == "Enter") {
        doorsOpen = true;
        RingTheBell();
     }
     else doorsOpen = false;
  }

};
                                </code></pre>
                            <p>Dans ce qui précède, le fil RingTheBell est lancé sur un message Object, Enter et se termine sur tout autre message Object , quel que soit le code de la classe.</p>
                        </div>
                    </div>
                </div>
                <div id="void" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Void</h4>
                            <p><i>Void</i> est de type <i>null</i> et est utilisé dans deux situations:</p>
                            <ul>
                                <li>pour indiquer qu'une méthode ne nécessite aucun paramètre</li>
                                <li>pour indiquer qu'une méthode ne renvoie pas de valeur</li>
                            </ul>
                            <pre><code class="language-javascript" data-language="javascript">
public void DoTheDo(void);
                                </code></pre>
                        </div>
                    </div>
                </div>
                <div id="wait" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>Wait</h4>
                            <p>
                                <i>Wait</i> est une instruction <i>de controle de flux</i> qui suspend l'exécution d'un thread en attendant la réception d'un message d'un type donné.<br>
                                Il peut écouter plusieurs types de messages à l'aide de l'instruction on.<br>
                                Lorsqu'une instruction wait est exécutée, l'exécution s'arrête sur le thread jusqu'à ce que l'une des conditions on soit remplie.
                            </p>
                            <pre><code class="language-javascript" data-language="javascript">
wait() {
   on "hello", "world" : {
      // le code ici sera exécuté pour n’importe quel message hello, world , puis continuera d’attendre les autres messages.
      continue;
   }
   on "hi", "there" : {
      // le code ici sera exécuté pour n’importe quel message hi, there , puis quittera la boucle.
      break;
   }
   on "hey", "" :
   {
      // le code ici sera exécuté pour tout message avec un type majeur de
      // "hey", il sera cassé par défaut, mettant fin à la boucle wait ().
   }
 }
                                </code></pre>
                        </div>
                    </div>
                </div>
                <div id="while" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>While</h4>
                            <p>Il s'agit d'une condition (boucle) qui s'exécutera tant que celle-ci sera évaluée à <i>true</i></p>
                            <pre><code class="language-javascript" data-language="javascript">
int i = 5;
while (i > 0) {
  Interface.Print("i is greater than zero");
}
                                </code></pre>
                            <div id="card-alert" class="card gradient-45deg-blue-grey-blue">
                                <div class="card-content white-text">
                                    <p>
                                        <i class="material-icons">info_outline</i> INFO : Notez que si la condition est initialement fausse, la boucle ne fonctionnera pas du tout.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col hide-on-small-only m3 l2">
                <div class="toc-wrapper">
                    <br><br><br>
                    <ul class="section table-of-contents">
                        <li><a href="#break">Break</a></li>
                        <li><a href="#case">Case</a></li>
                        <li><a href="#class">Class</a></li>
                        <li><a href="#continue">Continue</a></li>
                        <li><a href="#default">Default</a></li>
                        <li><a href="#define">Define</a></li>
                        <li><a href="#final">Final</a></li>
                        <li><a href="#for">For</a></li>
                        <li><a href="#goto">goto</a></li>
                        <li><a href="#if">If</a></li>
                        <li><a href="#include">Include</a></li>
                        <li><a href="#inherited">inherited</a></li>
                        <li><a href="#mandatory">Mandatory</a></li>
                        <li><a href="#me">Me</a></li>
                        <li><a href="#native">Native</a></li>
                        <li><a href="#null">Null</a></li>
                        <li><a href="#on">On</a></li>
                        <li><a href="#public">Public</a></li>
                        <li><a href="#return">Return</a></li>
                        <li><a href="#static">Static</a></li>
                        <li><a href="#switch">Switch</a></li>
                        <li><a href="#thread">Thread</a></li>
                        <li><a href="#void">Void</a></li>
                        <li><a href="#wait">Wait</a></li>
                        <li><a href="#while">While</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection