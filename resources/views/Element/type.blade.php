@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Opérateurs</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Element de programmation</a></li>
                        <li class="active">Opérateur</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <h4 class="header">Les Types</h4>
    <div class="row">
        <div class="col s12 m9 l10">
            <div id="bool" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Bool</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Contient les constantes booléennes true ou false</li>
                            <li><i class="material-icons">navigate_next</i> Transtyper un entier de valeur 0 en un bool donnera false , toute autre valeur donnera true.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
!false = true
(bool) 0 = false
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="int" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Int</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Entier 32 bits</li>
                            <li><i class="material-icons">navigate_next</i> Plage de -2147483647 .. 2147483647</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Formats
100
+100
-100
0x0f (Testé uniquement dans T: ANE)
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="float" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Float</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Nombre flottant 32 bits</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Formats
1.0
1.0f
polish notation?
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="object" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Object</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <i>Object</i> est une classe de base qui peut être utilisée pour stocker des références à des objets de n'importe quelle classe.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="string" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>String</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Les chaînes peuvent être indexées sous forme de tableaux.</li>
                            <li><i class="material-icons">navigate_next</i> Les chaînes supportent les séquences d'échappement suivantes</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
\r   ?
\n   New line
\t   Tab character
\l   ?
\a   ?
\b   ?
\\   Backslash
\"   Double quote mark
\'   Single quote mark
\0   ?
                                </code></pre>
                    </div>
                </div>
            </div>
            <div id="array" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Type de tableau (array)</h4>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Les tableaux sont des listes indexées de tout type de base ou de tout type de classe, y compris GameObjects.</li>
                            <li><i class="material-icons">navigate_next</i> Les tableaux sont déclarés à l'aide de ces instructions</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Train[] trains;
int[] Numbers = new int[n]; // où n est le nombre d'éléments du tableau.
                                </code></pre>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Le mot-clé new crée le tableau mais n'initialise aucun des membres.</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Numbers[3] = 256; // assigne une valeur au tableau. Les indices de tableau commencent à zéro
                                </code></pre>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Les tableaux peuvent également être retournés par des méthodes</li>
                        </ul>
                        <pre><code class="language-javascript" data-language="javascript">
Signal[] signals = World.GetSignalList()  // crée un tableau de signaux à partir d'une méthode intégrée
Numbers[Numbers.size()] = 5;              // ajoute une nouvelle valeur après la fin du tableau, en augmentant sa taille de 1
Numbers[0,1] = null;                      // supprime la première valeur du tableau, diminuant sa taille de 1
                                </code></pre>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#bool">Bool</a></li>
                    <li><a href="#int">Int</a></li>
                    <li><a href="#float">Float</a></li>
                    <li><a href="#object">Object</a></li>
                    <li><a href="#string">String</a></li>
                    <li><a href="#array">Type de Tableau</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection