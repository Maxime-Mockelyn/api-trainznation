@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Message</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Référenciel</a></li>
                        <li class="active">Message</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <div class="row">
        <div class="col s12 m9 l10">
            <div id="what" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Que sont les messages et comment peuvent-ils être utilisés?</h4>
                        <p>Les messages:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> sont des communications internes entre les objets dans Trainz.</li>
                            <li><i class="material-icons">navigate_next</i> sont complètement cachés de l'utilisateur normal, qui ne sera pas au courant de leur existence.</li>
                            <li><i class="material-icons">navigate_next</i> informent des événements dans l'environnement Trainz.</li>
                            <li><i class="material-icons">navigate_next</i> fournissent un moyen d'ouvrir des communications directes entre les objets.</li>
                            <li><i class="material-icons">navigate_next</i> fournissent un moyen d'envoyer des instructions d'un script à un autre.</li>
                            <li><i class="material-icons">navigate_next</i> fournissent un moyen de chronométrer les actions scriptées.</li>
                        </ul>
                        <br>
                        <p>Les messages sont gérés par le routeur , une sorte de bureau de poste interne, et peuvent être échangés entre tous les objets issus de <a href="{{ route('Gso.gameobject') }}">GameObject</a> . Cela inclut les commandes de pilote, les règles et les bibliothèques, ainsi que presque tous les objets scriptables basés sur des maillages.</p>
                        <br>
                        <p>Les Scripts peuvent:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> écoutez les messages et utilisez-les pour prendre les mesures appropriées en réponse aux événements et aux instructions.</li>
                            <li><i class="material-icons">navigate_next</i> utilisez le message pour extraire des informations sur l'objet qui envoie ou reçoit le message.</li>
                            <li><i class="material-icons">navigate_next</i> définir et émettre leurs propres messages.</li>
                            <li><i class="material-icons">navigate_next</i> écouter des messages destinés à d'autres objets.</li>
                            <li><i class="material-icons">navigate_next</i> envoyer des messages à eux-mêmes, pour qu'ils arrivent après un délai spécifié, comme un "rappel" pour faire quelque chose à l'avenir.</li>
                        </ul>
                        <br>
                        <p>Les messages sont de petits objets de données contenant quatre informations:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <i>msg.major :</i> une chaîne identifiant le type de message.</li>
                            <li><i class="material-icons">navigate_next</i> <i>msg.minor :</i> une chaîne contenant le sous-type du message ou, dans certains cas, des informations supplémentaires.</li>
                            <li><i class="material-icons">navigate_next</i> <i>msg.src :</i> l'objet envoyant le message.</li>
                            <li><i class="material-icons">navigate_next</i> <i>msg.dst :</i> destinataire prévu, certains messages sont <i>diffusés (broadcast)</i> ou envoyés au monde entier, auquel cas <i>msg.dst</i> sera <i>null</i>.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="how" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Comment écouter les messages ?</h4>
                        <p>Vous pouvez utiliser trois techniques:</p>
                        <ul>
                            <li>
                                <i class="material-icons">navigate_next</i> <a href="/gso/game_object#AddHandler">AddHandler()</a>: Ceci est le périphérique d'écoute le plus simple et
                                est particulièrement utile lorsque vous savez que ce type de message vous intéressera toujours. <i>AddHandler()</i> est généralement configuré
                                dans la méthode <i>Init()</i> d'un script.<br> <strong>Astuce:</strong> omettez les espaces optionnels dans les instructions qui incluent une fonction AddHandler, car leur inclusion fait que la fonction est ignorée sans notification dans TRS2004 et peut-être aussi dans d'autres versions.
                            </li>
                            <li><i class="material-icons">navigate_next</i> Une boucle <i>wait()</i>: Cela doit être exécuté dans un thread séparé et permet un arrangement temporaire dans lequel vous souhaitez connaître les messages pour une période plus courte ou dans un ensemble de conditions spécifié.</li>
                            <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object#Sniff">Sniff()</a>: Cela vous permet d’écouter les messages envoyés à un autre objet.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="when" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Quand et comment envoyer des messages ?</h4>
                        <p>Vous pourriez envisager d'envoyer vos propres messages dans les cas suivants:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Lorsqu'un événement a eu lieu dans votre script, il peut être nécessaire que d'autres objets le sachent.</li>
                            <li><i class="material-icons">navigate_next</i> Lorsque vous souhaitez rendre votre objet disponible à d'autres scripts pour leur permettre d'appeler directement vos méthodes.</li>
                            <li><i class="material-icons">navigate_next</i> Lorsque vous devez donner des instructions à un autre objet pour exécuter une tâche particulière et que vous savez que l'objet cible sera en mesure de comprendre votre message.</li>
                            <li><i class="material-icons">navigate_next</i> Lorsque vous souhaitez que votre propre script prenne une mesure à un moment ultérieur ou à des intervalles réguliers.</li>
                        </ul>
                        <br>
                        <p>Il existe trois routines que vous pouvez utiliser pour envoyer des messages:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object#SendMessage">SendMessage()</a> distribue le message immédiatement.</li>
                            <li><i class="material-icons">navigate_next</i> <a href="/gso/game_object#PostMessage">PostMessage()</a> envoie le message via le routeur avec un délai spécifié (qui peut être égal à zéro). Le message sera exécuté lors de la prochaine mise à jour du routeur.</li>
                            <li><i class="material-icons">navigate_next</i> <a href="/gso/router#PostMessage">Router.PostMessage()</a> autorise les messages à être envoyés même si l'objet que vous scriptez ne dispose pas de ses propres fonctionnalités de messagerie.</li>
                        </ul>
                        <br>
                        <p>Si la destination du message est entrée comme <i>me</i> (c'est-à-dire l'objet actuel), le message sera envoyé à l'objet propriétaire du script. Si la destination est <i>null</i> ou une chaîne vide, le message sera <i>diffusé (broadcast)</i> et donc disponible pour tout objet de la session.</p>
                        <p>L'envoi du message a les conséquences suivantes:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Tout thread bloquant (en attente) sur le message sera débloqué, bien que l'exécution du code ne commence pas avant que la planification n'atteigne ce thread.</li>
                            <li><i class="material-icons">navigate_next</i> Tous les objets avec des gestionnaires exécuteront leurs gestionnaires immédiatement (les gestionnaires ne sont pas threadés).</li>
                            <li><i class="material-icons">navigate_next</i> Tous les gestionnaires natifs (par exemple, le code pfx) seront exécutés immédiatement.</li>
                        </ul>
                        <br>
                        <p>Si vous avez une référence à la cible du message (ou si vous connaissez son nom ou son identifiant), vous devez prendre en compte les éléments suivants:</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Si vous connaissez les méthodes à utiliser, il serait peut-être plus approprié d'appeler directement l'autre script.</li>
                            <li><i class="material-icons">navigate_next</i> Évitez toujours de diffuser des messages s’il est possible de les envoyer directement au destinataire souhaité.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="never" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Ne faite jamais confiance au message</h4>
                        <p>Le système de messagerie contient de nombreux pièges pour les imprudents. Si vous travaillez avec une jonction, par exemple, vous recevrez un objet, entrez un message lorsqu'un train entre dans la zone de déclenchement de votre objet. Il n'est toutefois pas prudent de supposer qu'un objet, Laisser signifie que le train a traversé la jonction et est parti de l'autre côté. N'importe lequel des éléments suivants produirait également Object, Leave :</p>
                        <ul>
                            <li><i class="material-icons">navigate_next</i> Le train s’est peut-être arrêté et inversé loin de votre objet.</li>
                            <li><i class="material-icons">navigate_next</i> Une jonction intermédiaire a peut-être été basculée de sorte que le train ne soit plus visible par votre objet.</li>
                            <li><i class="material-icons">navigate_next</i> Le train pourrait s’être attelé à un autre (et donc avoir cessé d’exister) avant d’atteindre votre objet.</li>
                        </ul>
                        <br>
                        <p>Il est généralement nécessaire de traiter un message comme une indication que l'événement qui vous intéresse pourrait avoir eu lieu et d'utiliser d'autres tests pour confirmer.</p>
                    </div>
                </div>
            </div>
            <div id="exemple" class="section scrollspy">
                <div class="card">
                    <div class="card-content">
                        <h4>Exemple</h4>
                        <p>Écoute de messages et utilisation de méthodes de gestionnaire de messages</p>
                        <pre><code class="language-javascript" data-language="javascript">
include "MapObject.gs"

// le script peut être associé à n'importe quel objet de scène
// permet à la fenêtre de message d'afficher le résultat en Mode Pilote

class Messenger isclass MapObject {

   public void Init(void) {
      inherited();
      // écoute les messages Junction et World, ModuleInit
      AddHandler(me,"Junction","","ToggleHandler");
      AddHandler(me,"World","ModuleInit","ModuleInitHandler");
   }

   thread void TriggerMonitor(void) {
      // Les messages de déclenchement nous seront transmis à la suite de l appel Sniff () dans ModuleInitHandler
      Message msg;
      wait() {
         // // écoutez Trigger,
         on "Trigger","Enter",msg: {
            Interface.Print("A train has entered a trigger");
            continue;
         }
         // écoute Trigger, Stopped, affiche un message lorsqu'il est entendu, puis quitte le fil
         on "Trigger","Stopped",msg: {
            Interface.Print("A train has stopped on a trigger");
            Interface.Print("Trigger monitoring discontinued");
            break;
         }
      }
   }

   void ToggleHandler(Message msg) {
      // affiche un message à chaque jonction, Toggled est diffusé
      Interface.Print("A junction has been toggled");
   }

   void ModuleInitHandler(Message msg) {
      if (World.GetCurrentModule() == World.DRIVER_MODULE) {
         // attache un gestionnaire au premier train sur la carte en lui demandant de
         // copier tous les messages du déclencheur.
         Sniff(World.GetTrainList()[0],"Trigger","",true);
         // commence à surveiller les messages de déclenchement
         TriggerMonitor();
      }
   }

};
                                </code></pre>
                    </div>
                </div>
            </div>
        </div>
        <div class="col hide-on-small-only m3 l2">
            <div class="toc-wrapper">
                <br>
                <br>
                <br>
                <ul class="section table-of-contents">
                    <li><a href="#what">Que sont les messages et comment peuvent-ils être utilisés?</a></li>
                    <li><a href="#how">Comment écouter les messages ?</a></li>
                    <li><a href="#when">Quand et comment envoyer des messages ?</a></li>
                    <li><a href="#never">Ne faite jamais confiance au message</a></li>
                    <li><a href="#exemple">Exemple</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scrollspy').scrollSpy();
        });
    </script>
@endsection