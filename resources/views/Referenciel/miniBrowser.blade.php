@extends("Layout.app")

@section("css")

@endsection

@section("bread")
    <div id="breadcrumbs-wrapper">
        <!-- Search for small screen -->
        <div class="header-search-wrapper grey lighten-2 hide-on-large-only">
            <form id="formSearch" action="{{ route('search') }}" method="GET">
                <input type="text" name="search" class="header-search-input z-depth-2" placeholder="Tapez une fonction" />
            </form>
        </div>
        <div class="container">
            <div class="row">
                <div class="col s10 m6 l6">
                    <h5 class="breadcrumbs-title">Mini Browser</h5>
                    <ol class="breadcrumbs">
                        <li><a href="{{ route('home') }}">{{ env("APP_NAME") }}</a></li>
                        <li><a href="#">Référenciel</a></li>
                        <li class="active">Mini Browser</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("content")
<div class="section">
    <h2 class="header">MINI BROWSER</h2>
    <p>
        Le <strong>mininavigateur</strong> est un moteur de rendu trivial HTML utilisé pour un contrôle simple de mise en page dans l'environnement de jeu Trainz.
        En particulier, il ne remplace pas un vrai navigateur (voir Trainz Online à la place), n’est pas conforme aux normes à distance et ne prend pas en charge une gamme complète de balises HTML.
        Si une balise n'est pas mentionnée ici, elle n'est PAS prise en charge.
        Le javascript n'est pas pris en charge ( TrainzScript est utilisé à la place.) MiniBrowser est utilisé exclusivement par l' objet de script Trainz Browser .
    </p>
    <p>
        Trainz HTML n'est pas conforme aux normes et doit être considéré comme un HTML similaire. Les utilisateurs et les développeurs sont ainsi familiarisés avec le style de syntaxe utilisé.
    </p>
    <ul class="tabs">
        <li class="tab col s3"><a href="#tags">Tags Supportés</a></li>
        <li class="tab col s3"><a href="#characters">Caractères Supportés</a></li>
        <li class="tab col s3"><a href="#embedded">Objets Incorporés</a></li>
    </ul>
    <div id="tags">
        <div class="row">
            <div class="col s12 m9 l10">
                <h4 class="header">Tag Supporté</h4>
                <div id="html" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>HTML</h4>
                            <p>Cette balise enveloppe le document HTML entier. Aucun paramètre n'est pris en charge. Cette balise doit se situer au niveau supérieur du document et contenir une seule balise
                                <a href="#body">BODY</a>.</p>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;html>
...
&lt;/html>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="body" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>BODY</h4>
                            <p>
                                Cette balise enveloppe le corps entier du document HTML. Le paramètre 'scale' est fourni pour forcer le document à être redimensionné à un nombre spécifié de pixels de large, mais son utilisation est fortement déconseillée.
                                Cette balise doit exister dans la balise <a href="#html">HTML</a> et peut contenir un nombre quelconque d'autres éléments.
                            </p>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;body scale=?>
...
&lt;/body>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="p" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>P</h4>
                            <p>
                                Cette balise entraîne la mise en forme des éléments contenus en tant que paragraphe séparé.
                                Ceci est similaire à préfixer et suffixer les éléments avec un saut de ligne. La balise de paragraphe n'a pas de paramètres.
                            </p>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;p> ... &lt;/p>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="br" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>BR</h4>
                            <p>Un saut de ligne forcé. BR n'a pas de paramètres.</p>
                            <p>BR est une balise non fermée, contrairement à XHTML, il n'y a ni &lt;br/> ni &lt;/br>. En fait, utiliser une telle chose sera probablement interprété comme une erreur par l'analyseur HTML de Trainz.</p>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;br>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="table" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>TABLE</h4>
                            <p>
                                Cette balise forme un élément de table. Un nombre quelconque d'éléments < <a href="#tr">TR</a> > (rangée de tableau) peuvent être présents dans la balise 'TABLE' et c'est tout ce qu'elle doit contenir directement.
                                La hauteur de la table ne peut pas être spécifiée, cela peut être contrôlé en définissant chaque élément < <a href="#tr">TR</a> > dans la table pour qu'il s'additionne à la hauteur de table souhaitée.
                            </p>
                            <br>
                            <p>
                                La table ne supporte PAS les paramètres suivants: rules, summary, align.Il ne prend pas non plus en charge l’élément < TH > (le même effet peut être obtenu en rendant le contenu d’un < <a href="#td">TD</a> > <a href="#bold">BOLD</a> ou plus encore en utilisant <a href="#font">FONT</a> ).
                                Il est préférable de concevoir un tableau comme une grille, comme celle d'un tableur ou d'un tableau inséré dans un document.
                                Les tableaux sont décrits par < <a href="#tr">TR</a> >, une ligne dans une table et < <a href="#td">TD</a> >, une cellule dans une rangée.
                            </p>
                            <br>
                            <p>
                                Les tableaux ne sont pas de travers, ce qui signifie que si une ligne a 3 cellules, toutes les lignes auront au moins 3 cellules,
                                cela ne signifie pas que le codeur doit mettre au moins 3 < <a href="#td">TD</a> > dans chaque < <a href="#tr">TR</a> > cela signifie que lorsque formaté pour l'affichage
                                à l'écran le rendu html organisera la table en tant que telle (car elle est uniforme).<br>
                                De plus, les cellules se conforment aux dimensions de leurs frères et sœurs; ce qui signifie qu'une cellule a la même largeur que la cellule la plus large de sa colonne (contrôlable par la largeur dans < <a href="#td">TD</a> >)
                                et la même hauteur que la cellule la plus haute de sa rangée (contrôlable par la hauteur dans < <a href="#tr">TR</a> >).
                                Il est très important de garder cela à l'esprit lorsque vous utilisez des tableaux à des fins de formatage / mise en page.
                            </p>
                            <br>
                            <p>
                                La documentation sur < <a href="#tr">TR</a> > et < <a href="#td">TD</a> > contient davantage d’informations sur l’utilisation des tableaux dans Trainz HTML.
                                Pour plus d'informations et un endroit pour essayer certaines de ces propriétés de tables, essayez la <a href="https://www.w3schools.com/tags/tag_table.asp">page de balises de table w3schools</a> ,
                                mais n'oubliez pas que Trainz HTML n'est pas la même chose que HTML proprement dit.
                            </p>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;table cellpadding=? cellspacing=? border=? width=? inherit-font>...&lt;/table>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="tr" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>TR</h4>
                            <p>
                                TR est une rangée de tableau. Cette balise forme une seule ligne horizontale d'un tableau. Un nombre quelconque d'éléments '<a href="#td">TD</a> ' (données de table)
                                peuvent être présents dans la balise 'TR' et c'est tout ce qu'elle doit contenir directement. <TR> NE prend PAS en charge les paramètres suivants: align, valign, char, charoff.
                            </p>
                            <br>
                            <p>Tous les paramètres sont facultatifs. La hauteur, si elle est spécifiée, limitera le nombre de pixels de hauteur de cette ligne,
                                ce qui entraînera une largeur des cellules élargie si elles devaient approcher cette hauteur.</p>
                            <br>
                            <p>
                                Lorsque vous utilisez des tableaux pour le formatage, il est important de noter que la taille de la cellule dépend à la fois de la colonne et
                                de la ligne (voir < <a href="#td">TD</a> > et / ou < <a href="#table">TABLE</a> > pour plus d'informations). Il peut donc être utile de créer un &lt;TR> sans données. qui existe uniquement pour
                                définir la taille des colonnes, c.-à-d.
                            </p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;tr>
    &lt;td width='20'>&lt;/td>
    &lt;td width='50'>&lt;/td>
    &lt;td width='200'>&lt;/td>
    &lt;td width='20'>&lt;/td>
&lt;/tr>
                            </code>
                            </pre>
                            <p>
                                maintenant, sauf si un < TR > avec plus de 4 < TD > est créé, le formatage de toutes les colonnes est déjà fait et, comme ce < TR > n'a pas de hauteur spécifiée et aucune donnée dans ses cellules < TD >, il ne prendra que la espace spécifié par la bordure, le cellpacing et le cellpadding de < TABLE > (0 par défaut).
                            </p>
                            <br>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;tr height=? bgcolor=?>...&lt;/tr>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="td" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>TD</h4>
                            <p>Cette balise forme une seule cellule dans une ligne de tableau. < TD > ne supporte PAS les paramètres suivants: abbr, axis, char, charoff, headers, height, scope.</p>
                            <br>
                            <p>La hauteur d'une cellule est contrôlée par la cellule la plus haute de la ligne ou par le paramètre de hauteur < TR > s'il est spécifié.</p>
                            <br>
                            <p>< TD > est le cheval de travail des tables, comme son nom l'indique, tout le contenu d'une table est contenu dans une balise < TD >. Tout peut être placé dans une cellule, même un tableau, par exemple.</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;html>
    &lt;body>
        &lt;table border="1">
            &lt;tr>
                &lt;td>Text&lt;/td>
                &lt;td>Text&lt;/td>
            &lt;/tr>
            &lt;tr>
                &lt;td>Text&lt;/td>
                &lt;td>Text&lt;/td>
            &lt;/tr>
            &lt;tr>
                &lt;td>
                    &lt;table border="1">
                        &lt;tr>
                            &lt;td>Some text in a sub table &lt;/td>
                            &lt;td>Some other text in a sub table &lt;/td>
                        &lt;/tr>
                    &lt;/table>
                &lt;/td>
                &lt;td>Text&lt;/td>
            &lt;/tr>
        &lt;/table>
    &lt;/body>
&lt;/html>
                                </code>
                            </pre>
                            <p>copier et coller ce qui précède dans le volet gauche de <a href="https://www.w3schools.com/HTML/tryit.asp?filename=tryhtml_basic">cette fenêtre Tryit</a>
                                (ouvrez-le dans un nouvel onglet ou une nouvelle fenêtre) et cliquer sur le bouton "Editer et cliquer sur Run" vous affichera un tableau avec un tableau à gauche.</p>
                            <br>
                            <p>Tous les paramètres sont facultatifs.</p>
                            <ul>
                                <li><i class="material-icons">navigate_next</i> rowspan et colspan permettent à cette cellule de consommer l'espace qui serait normalement alloué aux cellules voisines des côtés droit et inférieur. Les valeurs par défaut sont 1, ce qui signifie que le < TD > s'étend sur sa propre cellule et rien de plus. <a href="https://www.w3schools.com/tags/tryit.asp?filename=tryhtml_table_span">Exemple visuel d’étalement</a> , mais rappelez-vous Trainz HTML n’est pas la même chose que HTML proprement dit.</li>
                                <li><i class="material-icons">navigate_next</i> width - spécifie la largeur en pixels de cette cellule, utile pour le formatage.</li>
                                <li><i class="material-icons">navigate_next</i> align - 'left', 'center', 'right'. (par défaut, 'left'.) Contrôle l'alignement horizontal des données de contenu dans la cellule du tableau.</li>
                                <li><i class="material-icons">navigate_next</i> valign - 'top', 'center', 'bottom' ('top' par défaut) Contrôle l'alignement vertical des données de contenu dans la cellule du tableau.</li>
                            </ul>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;td rowspawn=? colspan=? width=? bgcolor=? align=? valign=?>...&lt;/td>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="a" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>A</h4>
                            <p>A est la balise d'ancrage. Cette balise marque tous les éléments contenus en tant que lien hypertexte avec les attributs spécifiés. Les liens hypertextes seront soulignés.</p>
                            <br>
                            <p>Le texte de l'info-bulle s'affiche lorsque la souris survole les éléments liés par hyperlien.</p>
                            <br>
                            <p>Le href est le message mineur dans un <a href="/referenciel/browser_message">message de navigateur URL</a> envoyé par le <a href="/gso/game_object/browser">navigateur</a> à qui est l' écoute. c'est à dire. le message major="Browser-URL" minor="live://button/horn" est généré par un bouton comportant un lien hypertexte (horn) dans l'interface utilisateur du pilote.</p>
                            <br>
                            <p>Un hyperlien est considéré comme cliqué lorsque l’utilisateur a appuyé sur le bouton 1 de la souris et l’a relâché.</p>
                            <br>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;a href=? target=? help-label=? mousedown=? disabled-href=? tooltip=?>...&lt;/a>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="img" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>IMG</h4>
                            <p>IMG est la balise IMaGe. Cette balise affiche une image avec les attributs spécifiés. Ne supporte pas les balises <i>alt</i> ou <i>title</i>.</p>
                            <br>
                            <p>Tous les paramètres sont facultatifs. Toutefois, les paramètres <i>src</i> ou <i>kuid</i> doivent être renseignés pour que quoi que ce soit apparaisse, à l'exception d'un remplissage.</p>
                            <br>
                            <p>
                                Les paramètres de largeur (width) et de hauteur (height) définissent la taille en pixels que l'image occupera à l'écran. Vous pouvez les agrandir ou les réduire par rapport à la taille réelle de l'image pour les agrandir, les étirer ou les écraser à votre guise.
                                N'oubliez pas que tous les utilisateurs n'utilisent pas tous la même résolution. Par conséquent, il est généralement préférable de créer des images de taille fixe dans une position d'écran via l'utilisation de tableaux ou de calculer une taille de pixel par rapport à la résolution actuelle.
                            </p>
                            <br>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;img src=? mouseover=? kuid=? width=? height=? srcwidth=? srcheight=? color=? hflip>
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="trainz-text" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>TRAINZ-TEXT</h4>
                            <p>Une section de texte qui peut être modifiée sans avoir à recalculer ou réinitialiser l'intégralité de la chaîne HTML dans un <a href="/gso/game_object/browser#SetTrainzText">navigateur (Browser)</a> .</p>
                            <br>
                            <p>
                                Dans l'interface du conducteur, le panneau de vitesse affiche la vitesse actuelle du train sélectionné, la limite de vitesse actuelle sur son segment de voie et l'heure mondiale actuelle.
                                Ci-dessous, la vitesse HTML, qui est récupérée via la méthode de localisation décrite dans < <a href="#font">FONT</a> >
                            </p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;font color=#00FF00 locale='english'>
    &lt;b>
        &lt;trainz-text id='speed' text='0 KM/H'>&lt;/trainz-text>
    &lt;/b>
&lt;/font>
                                </code>
                            </pre>
                            <p>
                                Ceci spécifie un texte en gras vert lié à un identifiant de "vitesse". Désormais, dans chaque battement de coeur dans DriverModule.gs,
                                la vitesse du train actuellement sélectionné est calculée et appliquée à ce trainz-text en appelant <i>SetTrainzText("speed", "10km/h")</i> [10km/h utilisé uniquement à des fins d'illustration] dans le navigateur contenant la vitesse html.
                            </p>
                            <br>
                            <p>
                                Le paramètre id est requis pour pouvoir mettre à jour cette section de texte via l'appel du <a href="/gso/game_object/browser#SetTrainzText">navigateur SetTrainzText()</a> . Le paramètre text n'a pas besoin d'être renseigné. Int HTML peut être spécifié à l'aide de la méthode SetTrainzText ultérieurement.
                            </p>
                            <p>Usage:</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;trainz-text id=? text=?>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="b" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>B</h4>
                            <p>La Balise <strong>B</strong> est la mise en gras du texte. La balise en gras n'a pas de paramètre.</p>
                            <p>Usage:</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;b> ... &lt;/b>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="nowrap" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>NOWRAP</h4>
                            <p>Supprime le retour automatique à la ligne dans les éléments contenus. La balise nowrap n'a pas de paramètre.</p>
                            <p>Usage:</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;nowrap> ... &lt;/nowrap>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="i" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>I</h4>
                            <p>I est la balise Italique. Fait que le texte des éléments contenus soit rendu en italique. La balise italique n'a pas de paramètre.</p>
                            <p>Usage:</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;i> ... &lt;/i>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="font" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>FONT</h4>
                            <p>
                                Fait en sorte que les attributs de texte spécifiés soient appliqués à tout texte des éléments contenus.
                                Le texte dans Trainz HTML ne doit pas obligatoirement être placé dans des balises de police; s'il n'y en a pas, il reçoit les paramètres de police par défaut.
                            </p>
                            <br>
                            <p>Note de localisation; l'instruction suivante a une signification particulière pour l'analyseur HTML de Trains;</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;font locale=generic>A_TAG&lt;/font>
                                </code>
                            </pre>
                            <br>
                            <p>
                                Quand une telle balise est analysée, Trainz recherchera dans la <a href="/gso/stringtable">table de chaînes de la langue</a> sélectionnée de l’utilisateur actuel de l’actif actuel une chaîne dont le nom se trouve entre les balises &lt;font> &lt;/font>, dans le cas présent A_TAG.
                                Il remplacera l’intégralité de la déclaration à partir d’ici "&lt;font ... &lt;/font>" pour ce qui est trouvé dans la <a href="/gso/stringtable">table des chaînes</a> .
                                Un exemple de cela au travail est dans l’interface du pilote où les types de caméra sont définis dans les tables de chaînes de l’actif du pilote config.txt dans toutes les langues disponibles, de sorte que chaque bouton de la caméra possède l’info-bulle de langue sélectionnée pour chaque mode de caméra.
                            </p>
                            <br>
                            <p>Usage:</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;font size=? color=? locale=? face=?>...&lt;/font>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="trainz-object" class="section scrollspy">
                    <div class="card">
                        <div class="card-content">
                            <h4>TRAINZ-OBJECT</h4>
                            <p>Documenté ci-dessous sous <a href="#embedded">objets incorporés</a> . Les objets incorporés remplissent le paramètre de style. Les paramètres width et height spécifient la taille en pixels de l'objet donné, par exemple. quelle est la taille d'un bouton.</p>
                            <p>Usage:</p>
                            <pre>
                                <code class="language-html" data-language="html">
&lt;trainz-object width=? height=? style=?>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col hide-on-small-only m3 l2">
                <div class="toc-wrapper">
                    <br>
                    <br>
                    <br>
                    <ul class="section table-of-contents">
                        <li><strong>Tags Supportés</strong></li>
                        <li><a href="#html">HTML</a></li>
                        <li><a href="#body">BODY</a></li>
                        <li><a href="#p">P (Paragraphe)</a></li>
                        <li><a href="#br">BR (Saut de ligne)</a></li>
                        <li><a href="#table">TABLE</a></li>
                        <li><a href="#tr">TR</a></li>
                        <li><a href="#td">TD</a></li>
                        <li><a href="#a">A</a></li>
                        <li><a href="#img">IMG</a></li>
                        <li><a href="#trainz-text">TRAINZ-TEXT</a></li>
                        <li><a href="#b">B (Gras)</a></li>
                        <li><a href="#nowrap">Nowrap</a></li>
                        <li><a href="#i">I (Italique)</a></li>
                        <li><a href="#font">FONT</a></li>
                        <li><a href="#trainz-object">TRAINZ-OBJECT</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="characters">
        <h4 class="header">Caractères supportés</h4>
        <p>
            Le MiniBrowser analyse son entrée à partir du code HTML codé UTF-8.
            Toute la gamme de caractères unicode peut être incluse sans séquences d'échappement spéciales.
            Les séquences d'échappement suivantes sont autorisées et peuvent être nécessaires pour empêcher l'analyse des caractères en tant que balises HTML.
        </p>
        <br>
        <ul>
            <li><i class="material-icons">navigate_next</i> '&nbsp': le caractère d'espace insécable HTML.</li>
            <li><i class="material-icons">navigate_next</i> '&gt': le caractère "plus grand que" ('>')</li>
            <li><i class="material-icons">navigate_next</i> '&lt': le caractère "inférieur à" ('<')</li>
            <li><i class="material-icons">navigate_next</i> '&amp': le caractère "esperluette" ('&')</li>
            <li><i class="material-icons">navigate_next</i> '&quot': le caractère "quote" ('"')</li>
        </ul>
    </div>
    <div id="embedded">
        <h4 class="header">Objets Incorporés</h4>
        <p>Les objets incorporés suivants sont pris en charge par la balise trainz-object:</p>
        <div class="row">
            <div class="col s12 m9 l10">
                <div id="driver-menu-button" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>driver-menu-button</h4>
                            <p>Usage:</p>
                            <pre>
                            <code class="language-html" data-language="html">
style=driver-menu-button name=?
                            </code>
                        </pre>
                        </div>
                    </div>
                </div>
                <div id="dial" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>dial</h4>
                            <p>Fonctionne comme un bouton de volume analogique. Un cadran est utilisé dans l'interface du conducteur, dans le panneau dcc pour contrôler la vitesse / puissance du train. Ce sont ses paramètres:</p>
                            <pre>
                            <code class="language-html" data-language="html">
&lt;trainz-object style=dial width=95 height=95 id='dcc'
               texture='newdriver/dcc/dcc_controller.tga' min=0.1 max=0.9
               valmin=-1.0 valmax=1.0 step=0 clickstep=1 value=0.5>&lt;/trainz-object>
                                </code>
                            </pre>
                            <p>Conducteur enroule le cadran dans un < <a href="#a">A</a> > pour recevoir les messages provenant des interactions d'utilisateur avec lui, il utilise ensuite le <i>GetElementProperty("dcc", "value")</i> sur le <a href="/gso/game_object/browser">navigateur (Browser)</a> du cadran se trouve dans l'extrait sa valeur actuelle.</p>
                            <p>Paramètres expliqués:</p>
                            <ul>
                                <li><i class="material-icons">navigate_next</i> Les paramètres style, width, height et id appartiennent au <a href="#tags">TRAINZ-OBJECT</a> .</li>
                                <li><i class="material-icons">navigate_next</i> Les paramètres min et max spécifient la rotation (en pourcentage entre 0 et 1) que le cadran peut avoir dans l'interface. Pour le bouton de vitesse du conducteur, ses 0,1 à 0,9; en imaginant cela comme un cadran d'horloge, cela signifie que le cadran est autorisé à tourner entre 7 et 5 environ dans le sens des aiguilles d'une montre et inversement entre 5 et 7 dans le sens inverse des aiguilles d'une montre. La rotation du cadran ne peut jamais être comprise entre 5 et 7 dans le sens des aiguilles d'une montre.</li>
                                <li><i class="material-icons">navigate_next</i> valmin et valmax spécifient les valeurs renvoyées par le cadran, c'est-à-dire que ses valeurs correspondent à sa rotation. En utilisant à nouveau l'analogie avec le cadran d'horloge, vers 7 heures environ, le cadran retournera la valeur -1, vers 5 heures environ, il retournera la valeur 1 et en 12 heures, il retournera 0.</li>
                                <li><i class="material-icons">navigate_next</i> step spécifie le changement minimum qui peut avoir lieu dans le cadran. 0 signifie qu'il peut se déplacer complètement. Si step était égal à 0,1 et que l'utilisateur l'avait pivoté de 0,04, aucune rotation ne serait enregistrée et le cadran reviendrait à sa position précédente.</li>
                                <li><i class="material-icons">navigate_next</i> clickstep à déterminer.</li>
                                <li><i class="material-icons">navigate_next</i> valeur définit la position de départ initiale (en pourcentage de 0 à 1) de sa plage. 0,5 étant la moitié entre max et min.</li>
                            </ul>
                            <pre>
                                <code class="language-html" data-language="html">
style=dial texture=? min=? max=? valmin=? valmax=? step=? clickstep=? value=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="graphic-button" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>graphic-button</h4>
                            <pre>
                            <code class="language-html" data-language="html">
style=graphic-button text-color=? texture_off=? texture_on=? name=? toggle=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="thumbnail-downloader" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>thumbnail-downloader</h4>
                            <pre>
                            <code class="language-html" data-language="html">
style=thumbnail-downloader asset=? index=? thumb-size=? crop=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="button" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>button</h4>
                            <p>Un bouton est essentiellement composé de 8 <a href="#img">images</a> , où seulement 1 est affiché à la fois, en fonction de l'activation ou de la désactivation du bouton et de l'emplacement du curseur de l'utilisateur.</p>
                            <br>
                            <p>
                                Les boutons peuvent être activés ou désactivés en appelant <i>SetElementProperty("nom du bouton", "valeur", "1") [pour activer ou "0" pour désactiver]</i> dans le <a href="/gso/game_object/browser">navigateur</a> contenant le bouton. Tous les boutons commencent à l'état éteint.
                            </p>
                            <br>
                            <p>Les états "activé" et "désactivé" comportent 4 états permettant d'afficher une interaction avec le curseur. Off est expliqué ci-dessous, on remplace le même mot "off" par "on";</p>
                            <ul>
                                <li><i class="material-icons">navigate_next</i> "off" - le bouton est à l'état éteint et n'a aucune interaction de curseur.</li>
                                <li><i class="material-icons">navigate_next</i> "off-over" - le bouton est désactivé et le curseur le survole.</li>
                                <li><i class="material-icons">navigate_next</i> "off-down" - le bouton est à l'état éteint et le curseur a été cliqué sur le bouton; il est maintenu enfoncé (le bouton 1 de la souris est enfoncé), mais le curseur ne survole pas (n'est plus) sur le bouton, ce qui signifie que l'utilisateur a fait glisser le curseur du bouton après l'avoir cliqué.</li>
                                <li><i class="material-icons">navigate_next</i> "off-down-over" - le bouton est désactivé et le curseur le survole. Le curseur a été cliqué dessus et maintenu enfoncé (le bouton 1 de la souris est enfoncé).</li>
                            </ul>
                            <br>
                            <p>La taille d'un bouton est déterminée par les paramètres de largeur et de hauteur de la balise <a href="#tags">TRAINZ-OBJECT</a> qui la contient .</p>
                            <br>
                            <p>En général , un bouton trainz-objet sera contenu dans un < <a href="#tags">A</a> > donnant l'impression que les boutons envoie des messages. Comme les boutons n'envoient pas eux-mêmes les messages, ils ne savent pas directement quand ils ont été cliqués ou non. Cela doit être fait manuellement en écoutant le message envoyé par la balise d'ancrage et en modifiant l'état des boutons à l'aide de <i>SetElementProperty("nom du bouton", "valeur", "1")</i> dans le navigateur qui le contient.</p>
                            <br>
                            <p><strong>Usage:</strong> ? représenter des fichiers image dans l'actif.</p>
                            <pre>
                            <code class="language-html" data-language="html">
style=button id=&lt;button name> off=? off-over=? off-down=? off-down-over=? on=? on-over=? on-down=? on-down-over=? link-held=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="minimap" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>minimap</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=minimap
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="preview" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>preview</h4>
                            <p>Un aperçu de la ressource utilisé dans les onglets Surveyor.</p>
                            <ul>
                                <li><i class="material-icons">navigate_next</i> <strong>asset</strong>: le kuid de l'actif à afficher (transmettez le KUID par <i>KUID::GetHTMLString()</i>)</li>
                            </ul>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=preview asset=<&lt;asset-kuid>
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="text-line" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>text-line</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=text-line background=? align=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="asset-picker" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>asset-picker</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=asset-picker background=? align=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="clock" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>clock</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=clock face-texture=? seconds-texture=? minutes-texture=? hours-texture=? seconds-visible=? minutes-visible=? hours-visible=? seconds=? minutes=? hours=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="slider" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>slider</h4>
                            <p>Le curseur (slider) est une barre horizontale semblable à une barre de défilement horizontale avec une plage linéaire de valeurs, similaire à <a href="#dial">dial</a> .</p>
                            <br>
                            <p>Il y en a plusieurs dans le menu / élément de paramètres vidéo et ceux-ci seront utilisés à titre d'exemple ici. Les attributs les plus importants sont:</p>
                            <ul>
                                <li><i class="material-icons">navigate_next</i> <strong>min</strong>: la valeur minimale représentée par le curseur (tout à gauche).</li>
                                <li><i class="material-icons">navigate_next</i> <strong>max</strong>: la valeur maximale représentée par le curseur (tout à droite).</li>
                            </ul>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=slider min=? max=? page-size=? draw-lines=? draw-marks=? thumb=? thumb-over=? less=? less-over=? more=? more-over=? thumb-width=? thumb-height=? theme=? background=? arrow-width=? arrow-height=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="browser" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>browser</h4>
                            <p>Le <a href="/gso/game_object/browser">navigateur</a> est mis à jour via l'appel de SetElementProperty("nom-composant", "html", sub-browser html string) à partir du niveau de navigateur parent. Un exemple de ceci au travail peut être trouvé dans le <i>waybillmanager.gs</i></p>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=browser id='component-name' width=? height=? clip=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="edit-box" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>edit-box</h4>
                            <p>La zone d'édition autorise un champ de saisie de texte tel que celui fourni dans le champ de recherche du menu Routes ou dans la fenêtre iTrainz Chat.</p>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=edit-box multi-line read-only masked link-on-focus-loss
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="static-line" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>static-line</h4>
                            <p>Un graphique de ligne de séparation.</p>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=static-line color=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="progress" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>progress</h4>
                            <p>Une barre de progression.</p>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=progress progress=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="keyinput" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>keyinput</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=keyinput context=? id=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="score-gauge" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>score-gauge</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=score-gauge star-thresholds=? points=? high-score=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
                <div id="date-picker" class="section scrollspy2">
                    <div class="card">
                        <div class="card-content">
                            <h4>date-picker</h4>
                            <p><strong>Usage:</strong></p>
                            <pre>
                            <code class="language-html" data-language="html">
style=date-picker day=? month=? year=?
                                </code>
                            </pre>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col hide-on-small-only m3 l2">
                <div class="toc-wrapper">
                    <br>
                    <br>
                    <br>
                    <ul class="section table-of-contents">
                        <li><strong>Objets Incorporés</strong></li>
                        <li><a href="#driver-menu-button">driver-menu-button</a></li>
                        <li><a href="#dial">dial</a></li>
                        <li><a href="#graphic-button">graphic-button</a></li>
                        <li><a href="#thumbnail-downloader">thumbnail-downloader</a></li>
                        <li><a href="#button">button</a></li>
                        <li><a href="#minimap">minimap</a></li>
                        <li><a href="#preview">preview</a></li>
                        <li><a href="#text-line">text-line</a></li>
                        <li><a href="#asset-picker">asset-picker</a></li>
                        <li><a href="#clock">clock</a></li>
                        <li><a href="#slider">slider</a></li>
                        <li><a href="#browser">browser</a></li>
                        <li><a href="#edit-box">edit-box</a></li>
                        <li><a href="#static-line">static-line</a></li>
                        <li><a href="#progress">progress</a></li>
                        <li><a href="#keyinput">keyinput</a></li>
                        <li><a href="#score-gauge">score-gauge</a></li>
                        <li><a href="#date-picker">date-picker</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    <script type="text/javascript">
        $(document).ready(function(){
            $("ul.tabs").tabs()

            $('.scrollspy').scrollSpy({
                target: "#tags",
                scrollOffset: 800
            });
        });
        $(document).ready(function(){
            $('.scrollspy2').scrollSpy({
                target: "#embedded"
            });
        });
    </script>
@endsection